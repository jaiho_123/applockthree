package com.applockthree;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class iImageTouchLockScreen extends AppCompatActivity implements OnClickListener {
    private static final String TAG = "Application";
    private  RelativeLayout Pinlockll;
    private  ImageView appicon;
    private  TextView appname,bf;
    private  Button[] button;
    private  int cnt = 0;
    private  Date date;
    private DataBase db;
    private  SimpleDateFormat dd;
    private  ImageView[] dot;
    private  Drawable[] dotid;
    private  SimpleDateFormat dy,dm,th,tm,ts;
    private  Drawable emergency;
    private LinearLayout imagetouchrl;
    private String password = BuildConfig.FLAVOR;
    private PackageManager pm;
    int v = 0;
    private Vibrator vb;
    protected void onCreate(Bundle savedInstanceState) {
        Resources resources1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_i_image_touch_lock_screen);
        getWindow().setFlags(1024, 1024);
       emergency = getResources().getDrawable(R.drawable.fpassword);
       th = new SimpleDateFormat("hh");
       tm = new SimpleDateFormat("mm");
       ts = new SimpleDateFormat("ss a");
       dd = new SimpleDateFormat("dd");
       dm = new SimpleDateFormat("MM");
       dy = new SimpleDateFormat("yy");
       db = new DataBase(this);
       dot = new ImageView[5];
       vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
       dotid = new Drawable[5];
       button = new Button[50];
       pm = getBaseContext().getPackageManager();
        Addmob();
        try {
           Pinlockll = (RelativeLayout) findViewById(R.id.Pinlockll);
           imagetouchrl = (LinearLayout) findViewById(R.id.imagetouchrl);
           appicon = (ImageView) findViewById(R.id.appicon);
           appname = (TextView) findViewById(R.id.appname);
           bf = (TextView) findViewById(R.id.bf);
           button[1] = (Button) findViewById(R.id.xy1);
           button[2] = (Button) findViewById(R.id.xy2);
           button[3] = (Button) findViewById(R.id.xy3);
           button[4] = (Button) findViewById(R.id.xy4);
           button[5] = (Button) findViewById(R.id.xy5);
           button[6] = (Button) findViewById(R.id.xy6);
           button[7] = (Button) findViewById(R.id.xy7);
           button[8] = (Button) findViewById(R.id.xy8);
           button[9] = (Button) findViewById(R.id.xy9);
           button[10] = (Button) findViewById(R.id.xy10);
           button[11] = (Button) findViewById(R.id.xy11);
           button[12] = (Button) findViewById(R.id.xy12);
           button[13] = (Button) findViewById(R.id.xy13);
           button[14] = (Button) findViewById(R.id.xy14);
           button[15] = (Button) findViewById(R.id.xy15);
           button[16] = (Button) findViewById(R.id.xy16);
           button[17] = (Button) findViewById(R.id.xy17);
           button[18] = (Button) findViewById(R.id.xy18);
           button[19] = (Button) findViewById(R.id.xy19);
           button[20] = (Button) findViewById(R.id.xy20);
           button[21] = (Button) findViewById(R.id.xy21);
           button[22] = (Button) findViewById(R.id.xy22);
           button[23] = (Button) findViewById(R.id.xy23);
           button[24] = (Button) findViewById(R.id.xy24);
           button[25] = (Button) findViewById(R.id.xy25);
           button[26] = (Button) findViewById(R.id.xy26);
           button[27] = (Button) findViewById(R.id.xy27);
           button[28] = (Button) findViewById(R.id.xy28);
           button[29] = (Button) findViewById(R.id.xy29);
           button[30] = (Button) findViewById(R.id.xy30);
           button[31] = (Button) findViewById(R.id.xy31);
           button[32] = (Button) findViewById(R.id.xy32);
           button[33] = (Button) findViewById(R.id.xy33);
           button[34] = (Button) findViewById(R.id.xy34);
           button[35] = (Button) findViewById(R.id.xy35);
           button[36] = (Button) findViewById(R.id.xy36);
           button[37] = (Button) findViewById(R.id.xy37);
           button[38] = (Button) findViewById(R.id.xy38);
           button[39] = (Button) findViewById(R.id.xy39);
           button[40] = (Button) findViewById(R.id.xy40);
           button[41] = (Button) findViewById(R.id.xy41);
           button[42] = (Button) findViewById(R.id.xy42);
           button[43] = (Button) findViewById(R.id.xy43);
           button[44] = (Button) findViewById(R.id.xy44);
           button[45] = (Button) findViewById(R.id.xy45);
           button[46] = (Button) findViewById(R.id.xy46);
           button[47] = (Button) findViewById(R.id.xy47);
           button[48] = (Button) findViewById(R.id.xy48);
           button[49] = (Button) findViewById(R.id.xy49);
           bf.setOnClickListener(this);
           button[1].setOnClickListener(this);
           button[2].setOnClickListener(this);
           button[3].setOnClickListener(this);
           button[4].setOnClickListener(this);
           button[5].setOnClickListener(this);
           button[6].setOnClickListener(this);
           button[7].setOnClickListener(this);
           button[8].setOnClickListener(this);
           button[9].setOnClickListener(this);
           button[10].setOnClickListener(this);
           button[11].setOnClickListener(this);
           button[12].setOnClickListener(this);
           button[13].setOnClickListener(this);
           button[14].setOnClickListener(this);
           button[15].setOnClickListener(this);
           button[16].setOnClickListener(this);
           button[17].setOnClickListener(this);
           button[18].setOnClickListener(this);
           button[19].setOnClickListener(this);
           button[20].setOnClickListener(this);
           button[21].setOnClickListener(this);
           button[22].setOnClickListener(this);
           button[23].setOnClickListener(this);
           button[24].setOnClickListener(this);
           button[25].setOnClickListener(this);
           button[26].setOnClickListener(this);
           button[27].setOnClickListener(this);
           button[28].setOnClickListener(this);
           button[29].setOnClickListener(this);
           button[30].setOnClickListener(this);
           button[31].setOnClickListener(this);
           button[32].setOnClickListener(this);
           button[33].setOnClickListener(this);
           button[34].setOnClickListener(this);
           button[35].setOnClickListener(this);
           button[36].setOnClickListener(this);
           button[37].setOnClickListener(this);
           button[38].setOnClickListener(this);
           button[39].setOnClickListener(this);
           button[40].setOnClickListener(this);
           button[41].setOnClickListener(this);
           button[42].setOnClickListener(this);
           button[43].setOnClickListener(this);
           button[44].setOnClickListener(this);
           button[45].setOnClickListener(this);
           button[46].setOnClickListener(this);
           button[47].setOnClickListener(this);
           button[48].setOnClickListener(this);
           dot[1] = (ImageView) findViewById(R.id.idot1);
           dot[2] = (ImageView) findViewById(R.id.idot2);
           dot[3] = (ImageView) findViewById(R.id.idot3);
           dot[4] = (ImageView) findViewById(R.id.idot4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Cursor cursor = db.getitheme();
            cursor.moveToNext();
            String itheme = cursor.getString(0);
            String ithemepkg = cursor.getString(1);
            String ithemebg = cursor.getString(2);
            cursor.close();
            Resources resources = pm.getResourcesForApplication(ithemepkg);
            int idm0 = resources.getIdentifier(itheme + "imagetouch", "drawable", ithemepkg);
            dotid[0] = resources.getDrawable(resources.getIdentifier(itheme + "dot", "drawable", ithemepkg));
            dotid[1] = resources.getDrawable(resources.getIdentifier(itheme + "doti", "drawable", ithemepkg));
            emergency = resources.getDrawable(resources.getIdentifier("fpassword", "drawable", ithemepkg));
            imagetouchrl.setBackground(resources.getDrawable(idm0));
            Cursor cursor1 = db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 = db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 = pm.getResourcesForApplication(cursor1.getString(1));
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e2) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                    Pinlockll.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e3) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
            bf.setBackground(emergency);
        } catch (NameNotFoundException e4) {
            e4.printStackTrace();
        }
        dot[1].setBackground(dotid[0]);
        dot[2].setBackground(dotid[0]);
        dot[3].setBackground(dotid[0]);
        dot[4].setBackground(dotid[0]);
    }
    public void Addmob() {
        Log.e("iImageTouchLockscren","Activity");
        AdView adView = (AdView) findViewById(R.id.adView_tscn);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }
    private void vcheck() {
        try {
            Cursor cursor1 = db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
                db.deletebackground(cursor1.getString(1));
                Resources resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onResume() {
        vcheck();
        super.onResume();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() != 4 || event.getAction() != 0) {
            if (db.getfastunlockm1().contains("true")) {
                if (event.getKeyCode() == 24 && event.getAction() == 0) {
                    v = 1;
                    return true;
                } else if (event.getKeyCode() == 24 && event.getAction() == 1) {
                    v = 0;
                    return true;
                }
            }
            if (db.getfastunlockm2().contains("true")) {
                if (event.getKeyCode() == 25 && event.getAction() == 0) {
                    v = 1;
                    return true;
                } else if (event.getKeyCode() == 25 && event.getAction() == 1) {
                    v = 0;
                    return true;
                }
            }
            return super.dispatchKeyEvent(event);
        } else if (v == 1) {
            v = 0;
            finish();
            return true;
        } else {
            Intent startHome = new Intent("android.intent.action.MAIN");
            startHome.addCategory("android.intent.category.HOME");
            startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startHome);
            return true;
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.xy1) {
            check("01");
        }
        if (v.getId() == R.id.xy2) {
            check("02");
        }
        if (v.getId() == R.id.xy3) {
            check("03");
        }
        if (v.getId() == R.id.xy4) {
            check("04");
        }
        if (v.getId() == R.id.xy5) {
            check("05");
        }
        if (v.getId() == R.id.xy6) {
            check("06");
        }
        if (v.getId() == R.id.xy7) {
            check("07");
        }
        if (v.getId() == R.id.xy8) {
            check("08");
        }
        if (v.getId() == R.id.xy9) {
            check("09");
        }
        if (v.getId() == R.id.xy10) {
            check("10");
        }
        if (v.getId() == R.id.xy11) {
            check("11");
        }
        if (v.getId() == R.id.xy12) {
            check("12");
        }
        if (v.getId() == R.id.xy13) {
            check("13");
        }
        if (v.getId() == R.id.xy14) {
            check("14");
        }
        if (v.getId() == R.id.xy15) {
            check("15");
        }
        if (v.getId() == R.id.xy16) {
            check("16");
        }
        if (v.getId() == R.id.xy17) {
            check("17");
        }
        if (v.getId() == R.id.xy18) {
            check("18");
        }
        if (v.getId() == R.id.xy19) {
            check("19");
        }
        if (v.getId() == R.id.xy20) {
            check("20");
        }
        if (v.getId() == R.id.xy21) {
            check("21");
        }
        if (v.getId() == R.id.xy22) {
            check("22");
        }
        if (v.getId() == R.id.xy23) {
            check("23");
        }
        if (v.getId() == R.id.xy24) {
            check("24");
        }
        if (v.getId() == R.id.xy25) {
            check("25");
        }
        if (v.getId() == R.id.xy26) {
            check("26");
        }
        if (v.getId() == R.id.xy27) {
            check("27");
        }
        if (v.getId() == R.id.xy28) {
            check("28");
        }
        if (v.getId() == R.id.xy29) {
            check("29");
        }
        if (v.getId() == R.id.xy30) {
            check("30");
        }
        if (v.getId() == R.id.xy31) {
            check("31");
        }
        if (v.getId() == R.id.xy32) {
            check("32");
        }
        if (v.getId() == R.id.xy33) {
            check("33");
        }
        if (v.getId() == R.id.xy34) {
            check("34");
        }
        if (v.getId() == R.id.xy35) {
            check("35");
        }
        if (v.getId() == R.id.xy36) {
            check("36");
        }
        if (v.getId() == R.id.xy37) {
            check("37");
        }
        if (v.getId() == R.id.xy38) {
            check("38");
        }
        if (v.getId() == R.id.xy39) {
            check("39");
        }
        if (v.getId() == R.id.xy40) {
            check("40");
        }
        if (v.getId() == R.id.xy41) {
            check("41");
        }
        if (v.getId() == R.id.xy42) {
            check("42");
        }
        if (v.getId() == R.id.xy43) {
            check("43");
        }
        if (v.getId() == R.id.xy44) {
            check("44");
        }
        if (v.getId() == R.id.xy45) {
            check("45");
        }
        if (v.getId() == R.id.xy46) {
            check("46");
        }
        if (v.getId() == R.id.xy47) {
            check("47");
        }
        if (v.getId() == R.id.xy48) {
            check("48");
        }
        if (v.getId() == R.id.xy49) {
            check("49");
        }
        if (v.getId() == R.id.bf) {
            PopupMenu popup = new PopupMenu(this, v);
            popup.getMenuInflater().inflate(R.menu.actions, popup.getMenu());
            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    cnt = 0;
                    password = BuildConfig.FLAVOR;
                    setdot();
                    db.setfp("true");
                    Intent startHome = new Intent(getBaseContext(), Forgotpassword.class);
                    startHome.addCategory("android.intent.category.HOME");
                    startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startHome);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                        }
                    }, 150);
                    return false;
                }
            });
            popup.show();
        }
    }

    public void check(String key) {
        if (db.getisettingvibrate().contains("true")) {
            vb.vibrate(50);
        }
        if (db.getsound().contains("true")) {
            MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.click);
            mp.start();
            mp.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        password += key;
        cnt++;
        setdot();
        if (cnt < 4) {
            return;
        }
        if (db.matchpassword(password)) {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    finish();
                    cnt = 0;
                    password = BuildConfig.FLAVOR;
                    setdot();
                }
            }, 100);
        } else {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (db.getisettingvibrate().contains("true")) {
                        vb.vibrate(200);
                    }
                    date = new Date();
                    String d = BuildConfig.FLAVOR;
                    String t = BuildConfig.FLAVOR;
                    String an = BuildConfig.FLAVOR;
                    d = dd.format(date) + "/" + dm.format(date) + "/" + dy.format(date);
                    t = th.format(date) + ":" + tm.format(date) + ":" + ts.format(date);
                    try {
                        an = pm.getApplicationLabel(pm.getApplicationInfo(db.getcurrentapp(), PackageManager.GET_META_DATA)).toString();
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    db.insertapphacklog(db.getcurrentapp(), an, getResources().getString(R.string.not_displayble), d, t);
                    cnt = 0;
                    password = BuildConfig.FLAVOR;
                    setdot();
                }
            }, 100);
        }
    }

    public void setdot() {
        if (cnt == 0) {
            dot[1].setBackground(dotid[0]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 1) {
            dot[1].setBackground(dotid[1]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 2) {
            dot[2].setBackground(dotid[1]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 3) {
            dot[3].setBackground(dotid[1]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 4) {
            dot[4].setBackground(dotid[1]);
        }
    }
    @Override
    public void onBackPressed() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
        super.onBackPressed();
    }
}
