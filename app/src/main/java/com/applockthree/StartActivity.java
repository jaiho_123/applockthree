package com.applockthree;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class StartActivity extends AppCompatActivity {
    DataBase db;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        db = new DataBase(this);
        if (db.getmasterpassword().trim().length() < 1 || db.getisettinganswer().trim().length() < 1) {
            startActivity(new Intent(getBaseContext(), emailquestion.class));
            finish();
            return;
        }
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
