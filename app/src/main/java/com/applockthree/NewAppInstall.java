package com.applockthree;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.IBinder;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.applockthree.R.id.btn3;

public class NewAppInstall extends Service implements OnClickListener {
    private ImageView appicon,appiconback,appiconfor;
    private TextView appname;
    private Button btn1,btn2;
    private DataBase db;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String pkg = BuildConfig.FLAVOR;
    private PackageManager pm;
    private Vibrator vb;
    private LayoutParams wmlp;

    public void onCreate() {
        super.onCreate();
        vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        db = new DataBase(this);
        pm = getBaseContext().getPackageManager();
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
    }

    private void initLayouts() {
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        wmlp = new LayoutParams(-1, -1, 2002, 32, -3);
        wmlp.screenOrientation = 1;
        wmlp.alpha = 1.0f;
        wmlp.flags = 262144;
        wmlp.gravity = 17;
        LayoutParams layoutParams = this.wmlp;
        layoutParams.flags &= -2097297;
        wmlp.flags = 256;
        wmlp.token = null;
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        mLayout = new LinearLayout(this) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() != 4 || event.getAction() != 0) {
                    return super.dispatchKeyEvent(event);
                }
                db.setallappunlock();
               removeLockScreen();
                return true;
            }
        };
        mLayout.setLayoutParams(new LayoutParams(-2, -2));
        mLayout.setGravity(17);
        View view = LayoutInflater.from(this).inflate(R.layout.newapp, mLayout, true);
        appicon = (ImageView) view.findViewById(R.id.appicon);
        appiconback = (ImageView) view.findViewById(R.id.appiconback);
        appiconfor = (ImageView) view.findViewById(R.id.appiconfor);
        appname = (TextView) view.findViewById(R.id.txt1);
        btn1 = (Button) view.findViewById(R.id.btn1);
        btn2 = (Button) view.findViewById(R.id.btn2);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        try {
            appname.setText(getResources().getString(R.string.Protect) + " " + pm.getApplicationLabel(pm.getApplicationInfo(pkg, PackageManager.GET_META_DATA)).toString() + " " + getResources().getString(R.string.by_iAppLock1));
            appicon.setImageDrawable(pm.getApplicationIcon(pkg));
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e2) {
        }
        Cursor cursor = this.db.getitheme();
        cursor.moveToNext();
        String itheme = cursor.getString(0);
        String ithemepkg = cursor.getString(1);
        String ithemebg = cursor.getString(2);
        cursor.close();
        try {
            Resources resources = this.pm.getResourcesForApplication(ithemepkg);
            int idm0 = resources.getIdentifier(itheme + "appiconback", "drawable", ithemepkg);
            int idm1 = resources.getIdentifier(itheme + "appiconfor", "drawable", ithemepkg);
            appiconback.setBackground(resources.getDrawable(idm0));
            appiconfor.setBackground(resources.getDrawable(idm1));
        } catch (Exception e3) {
        }
        try {
            this.mWindowManager.addView(this.mLayout, this.wmlp);
        } catch (RuntimeException e4) {
            stopSelf();
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            this.pkg = intent.getStringExtra("pkg");
        } catch (Exception e) {
        }
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (this.mLayout == null) {
            initLayouts();
        }
        return START_STICKY;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn1) {
            try {
                db.insertapp(pm.getApplicationLabel(pm.getApplicationInfo(pkg, PackageManager.GET_META_DATA)).toString(), pkg, BuildConfig.FLAVOR, "true", "true");
                Toast.makeText(getBaseContext(), pkg + getString(R.string.sucelock), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            stopSelf();
        }
        if (v.getId() == R.id.btn2) {
            try {
                db.insertapp(pm.getApplicationLabel(pm.getApplicationInfo(pkg, PackageManager.GET_META_DATA)).toString(), pkg, BuildConfig.FLAVOR, "false", "true");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            stopSelf();
        }
        if (v.getId() == btn3) {
            try {
                db.insertapp(pm.getApplicationLabel(pm.getApplicationInfo(pkg, PackageManager.GET_META_DATA)).toString(), pkg, BuildConfig.FLAVOR, "false", "true");
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            stopSelf();
        }

    }

    public void onDestroy() {
        super.onDestroy();
        removeLockScreen();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void removeLockScreen() {
        if (mLayout != null) {
            try {
                mWindowManager.removeView(mLayout);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mLayout = null;
        }
    }
}
