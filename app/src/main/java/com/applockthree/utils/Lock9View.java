package com.applockthree.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.applockthree.R;

import java.util.ArrayList;
import java.util.List;

public class Lock9View extends ViewGroup {
    private Bitmap bitmap;
    private CallBack callBack;
    private Canvas canvas;
    private NodeView currentNode;
    private List<Pair<NodeView, NodeView>> lineList;
    private Drawable nodeOnSrc;
    private Drawable nodeSrc;
    private Paint paint;
    private StringBuilder pwdSb;

    public interface CallBack {
        void onFinish(String str);
    }

    public class NodeView extends View {
        private boolean highLighted;
        private int num;

        private NodeView(Context context) {
            super(context);
        }

        public NodeView(Lock9View this$0, Context context, int num) {
            this(context);
            this.num = num;
            this.highLighted = false;
            if (this$0.nodeSrc == null) {
                setBackgroundResource(0);
            } else {
                setBackground(this$0.nodeSrc);
            }
        }

        public boolean isHighLighted() {
            return this.highLighted;
        }

        public void setHighLighted(boolean highLighted) {
            this.highLighted = highLighted;
            if (highLighted) {
                if (Lock9View.this.nodeOnSrc == null) {
                    setBackgroundResource(0);
                } else {
                    setBackground(nodeOnSrc);
                }
            } else if (nodeSrc == null) {
                setBackgroundResource(0);
            } else {
                setBackground(nodeSrc);
            }
        }

        public int getCenterX() {
            return (getLeft() + getRight()) / 2;
        }

        public int getCenterY() {
            return (getTop() + getBottom()) / 2;
        }

        public int getNum() {
            return this.num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }

    public Lock9View(Context context) {
        this(context, null);
    }

    public Lock9View(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Lock9View(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public Lock9View(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        initFromAttributes(attrs, defStyleAttr);
    }

    private void initFromAttributes(AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.Lock9View, defStyleAttr, 0);
        nodeSrc = a.getDrawable(0);
        a.recycle();
        paint = new Paint(4);
        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setColor(Color.parseColor("#bbee00"));
        paint.setAntiAlias(true);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        bitmap = Bitmap.createBitmap(dm.widthPixels, dm.widthPixels, Config.ARGB_8888);
        canvas = new Canvas();
        canvas.setBitmap(this.bitmap);
        for (int n = 0; n < 9; n++) {
            addView(new NodeView(this, getContext(), n + 1));
        }
        this.lineList = new ArrayList();
        this.pwdSb = new StringBuilder();
        setWillNotDraw(false);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, widthMeasureSpec);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (changed) {
            int nodeWidth = (right - left) / 3;
            int nodePadding = nodeWidth / 6;
            for (int n = 0; n < 9; n++) {
                int row = n / 3;
                int col = n % 3;
                (getChildAt(n)).layout((col * nodeWidth) + nodePadding, (row * nodeWidth) + nodePadding, ((col * nodeWidth) + nodeWidth) - nodePadding, ((row * nodeWidth) + nodeWidth) - nodePadding);
            }
        }
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, null);
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0 /*0*/:
            case 2 /*2*/:
                NodeView nodeAt = getNodeAt(event.getX(), event.getY());
                if (nodeAt == null && this.currentNode == null) {
                    return true;
                }
                clearScreenAndDrawList();
                if (currentNode == null) {
                    currentNode = nodeAt;
                    currentNode.setHighLighted(true);
                    pwdSb.append(currentNode.getNum());
                } else if (nodeAt == null || nodeAt.isHighLighted()) {
                    canvas.drawLine((float) currentNode.getCenterX(), (float) currentNode.getCenterY(), event.getX(), event.getY(), paint);
                } else {
                    canvas.drawLine((float) currentNode.getCenterX(), (float) currentNode.getCenterY(), (float) nodeAt.getCenterX(), (float) nodeAt.getCenterY(), paint);
                    nodeAt.setHighLighted(true);
                    lineList.add(new Pair(currentNode, nodeAt));
                    currentNode = nodeAt;
                    pwdSb.append(currentNode.getNum());
                }
                invalidate();
                return true;
            case 1 /*1*/:
                if (pwdSb.length() <= 0) {
                    return super.onTouchEvent(event);
                }
                if (callBack != null) {
                    callBack.onFinish(pwdSb.toString());
                    pwdSb.setLength(0);
                }
                currentNode = null;
                lineList.clear();
                clearScreenAndDrawList();
                for (int n = 0; n < getChildCount(); n++) {
                    ((NodeView) getChildAt(n)).setHighLighted(false);
                }
                invalidate();
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }

    private void clearScreenAndDrawList() {
        this.canvas.drawColor(0, Mode.CLEAR);
        for (Pair<NodeView, NodeView> pair : this.lineList) {
            this.canvas.drawLine((float) ((NodeView) pair.first).getCenterX(), (float) ((NodeView) pair.first).getCenterY(), (float) ((NodeView) pair.second).getCenterX(), (float) ((NodeView) pair.second).getCenterY(), this.paint);
        }
    }

    private NodeView getNodeAt(float x, float y) {
        for (int n = 0; n < getChildCount(); n++) {
            NodeView node = (NodeView) getChildAt(n);
            if (x >= ((float) node.getLeft()) && x < ((float) node.getRight()) && y >= ((float) node.getTop()) && y < ((float) node.getBottom())) {
                return node;
            }
        }
        return null;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    public void set(Drawable on, Drawable off, Integer color, Integer color1, Integer color2, Integer color3) {
        int n;
        nodeSrc = off;
        nodeOnSrc = on;
        int lineColor = Color.argb(color.intValue(), color1.intValue(), color2.intValue(), color3.intValue());
        paint = new Paint(4);
        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth(15.0f);
        paint.setColor(lineColor);
        paint.setAntiAlias(true);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        bitmap = Bitmap.createBitmap(dm.widthPixels, dm.widthPixels, Config.ARGB_8888);
        canvas = new Canvas();
        canvas.setBitmap(bitmap);
        for (n = 0; n < 9; n++) {
            addView(new NodeView(this, getContext(), n + 1));
        }
        lineList = new ArrayList();
        pwdSb = new StringBuilder();
        setWillNotDraw(false);
        currentNode = null;
        lineList.clear();
        clearScreenAndDrawList();
        for (n = 0; n < getChildCount(); n++) {
            ((NodeView) getChildAt(n)).setHighLighted(false);
        }
    }
}
