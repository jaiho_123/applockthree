package com.applockthree;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

public class installunistall_reciver extends BroadcastReceiver {
    DataBase db;

    public void onReceive(Context context, Intent intent) {
        String ithemepkg;
        db = new DataBase(context);
        String toastMessage = "";
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            toastMessage = (" " + intent.getData().toString()).substring(8);
            if (toastMessage.contains("com.applockthree.theme")) {
                db.inserttheme("itheme1", toastMessage, "bg1");
                db.insertbackground("background", toastMessage.trim());
                Cursor cursor = db.getitheme();
                cursor.moveToNext();
                ithemepkg = cursor.getString(1);
                cursor.close();
                if (ithemepkg.trim().contentEquals(toastMessage.trim())) {
                    intent = new Intent("android.intent.action.MAIN");
                    intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype()));
                    intent.putExtra("type", "3");
                    context.stopService(intent);
                    context.startService(intent);
                } else {
                    intent = new Intent("android.intent.action.MAIN");
                    intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype()));
                    intent.putExtra("type", "3");
                    context.stopService(intent);
                    context.startService(intent);
                }
            } else {
                Intent intent1 = new Intent(context, NewAppInstall.class);
                intent1.putExtra("pkg", toastMessage);
                context.startService(intent1);
            }
        }
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            Log.e("ssc screen restart", "theme unistal =" + toastMessage);
            toastMessage = (BuildConfig.FLAVOR + intent.getData().toString()).substring(8);
            Log.e("ssc screen restart", "theme unistall =" + toastMessage);
            if (toastMessage.contains("com.applockthree.theme")) {
                Log.e( "ssc screen restart", "theme unistall 1 =" + toastMessage);
                this.db.deletetheme(toastMessage);
               Cursor cursor = this.db.getitheme();
                cursor.moveToNext();
                ithemepkg = cursor.getString(1);
                cursor.close();
                boolean flag = false;
                if (ithemepkg.trim().toString().contains(toastMessage.trim().toString())) {
                    this.db.setitheme("itheme1", BuildConfig.APPLICATION_ID, "bg1");
                    intent = new Intent("android.intent.action.MAIN");
                    intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype()));
                    intent.putExtra("type", "3");
                    context.stopService(intent);
                    context.startService(intent);
                    flag = true;
                }
                Log.e( "ssc screen restart", "check =" + db.checkbackground(toastMessage.trim()));
                if (db.checkbackground(toastMessage.trim()) && !flag) {
                    Log.e("ssc screen restart", "flag=" + flag);
                    intent = new Intent("android.intent.action.MAIN");
                    intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype()));
                    intent.putExtra("type", "3");
                    context.stopService(intent);
                    context.startService(intent);
                }
                db.deletebackground(toastMessage);
            }
            db.deleteapp(toastMessage);
        }
    }
}
