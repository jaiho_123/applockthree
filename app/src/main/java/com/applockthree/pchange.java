package com.applockthree;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;

public class pchange extends Service implements OnClickListener {
    private Button btn1;
    private Button btn2;
    private DataBase db;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String password;
    private Vibrator vb;
    private LayoutParams wmlp;

    public void onCreate() {
        super.onCreate();
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        db = new DataBase(this);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
    }

    private void initLayouts() {
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        wmlp = new LayoutParams(-1, -1, 2002, 32, -3);
        wmlp.screenOrientation = 1;
        wmlp.alpha = 1.0f;
        wmlp.flags = 262144;
        wmlp.gravity = 17;
        LayoutParams layoutParams = this.wmlp;
        layoutParams.flags &= -2097297;
        wmlp.flags = 256;
        wmlp.token = null;
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        mLayout = new LinearLayout(this) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() != 4 || event.getAction() != 0) {
                    return super.dispatchKeyEvent(event);
                }
                db.setallappunlock();
                removeLockScreen();
                return true;
            }
        };
        mLayout.setLayoutParams(new LayoutParams(-2, -2));
        mLayout.setGravity(17);
        View view = LayoutInflater.from(this).inflate(R.layout.pchange, mLayout, true);
        btn1 = (Button) view.findViewById(R.id.btn1);
        btn2 = (Button) view.findViewById(R.id.btn2);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        try {
            mWindowManager.addView(mLayout, wmlp);
        } catch (RuntimeException e) {
            stopSelf();
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (this.mLayout == null) {
            initLayouts();
        }
        return START_STICKY;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn1) {
            this.db.setallapplockbymasterpassword();
            stopSelf();
        }
        if (v.getId() == R.id.btn2) {
            this.db.setallappunlock();
            stopSelf();
        }

    }

    public void onDestroy() {
        super.onDestroy();
        removeLockScreen();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void removeLockScreen() {
        if (this.mLayout != null) {
            try {
                mWindowManager.removeView(mLayout);
            } catch (Exception e) {
            }
            mLayout = null;
        }
    }
}
