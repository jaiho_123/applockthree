package com.applockthree;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataBase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "iapplock";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "apps";
    private static final String TABLE_NAME1 = "setting";
    private static final String TABLE_NAME2 = "theme";
    private static final String TABLE_NAME3 = "apphacklog";
    private static final String TABLE_NAME4 = "feedback";
    private static final String TABLE_NAME5 = "fastunlock";
    private static final String TABLE_NAME6 = "background";
    SQLiteDatabase sqldb;

    public DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.sqldb = getWritableDatabase();
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table apps(name text,package text,icon text,lock text,flag text,password text,lodd text,lodm text,lody text,loth text,lotm text,lots text,lcdd text,lcdm text,lcdy text,lcth text,lctm text,lcts text,totaltime text)");
        db.execSQL("create table setting(currentapp text,iapplock text,sound text,vibrate text,notification,asar text,question text,answer text,language text,hideiapplock text,email text,masterpassword text,installer text,itheme text,ithemepkg text,ithemebg text,locktype text,position text,hiddencamera text,fp text,lchange text,fp1 text)");
        db.execSQL("create table theme(id INTEGER PRIMARY KEY AUTOINCREMENT,itheme text,ithemepkg text,ithemebg text)");
        db.execSQL("create table apphacklog(id INTEGER PRIMARY KEY AUTOINCREMENT,package text,name text,password text,date text,time text)");
        db.execSQL("create table feedback(email text,subject text,feedback text)");
        db.execSQL("create table fastunlock(fastunlock text,method1 text,method2 text)");
        db.execSQL("create table background(id INTEGER PRIMARY KEY AUTOINCREMENT,bgtype text,bg text,seta text)");
        db.execSQL("insert into background (bgtype,bg,seta) values('background','com.applockthree','true')");
        db.execSQL("insert into setting (currentapp,iapplock,sound,vibrate,notification,asar,question,answer,language,hideiapplock,email,masterpassword,installer,itheme,ithemepkg,ithemebg,locktype,position,hiddencamera,fp,lchange,fp1) values('','true','true','false','true','true','','','English','false','','','false','itheme1','com.applockthree','bg1','PINLockScreen','iapplock','false','false','false','false')");
        db.execSQL("insert into theme(itheme,ithemepkg,ithemebg) values('itheme1','com.applockthree','bg1')");
        db.execSQL("insert into fastunlock(fastunlock,method1,method2) values('false','false','false')");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists apps");
        onCreate(db);
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    public void insertapp(String name, String packag, String icon, String lock, String flag) {
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("package", packag);
        cv.put("icon", icon);
        cv.put("lock", lock);
        cv.put("flag", flag);
        cv.put("password", BuildConfig.FLAVOR);
        cv.put("lodd", "00");
        cv.put("lodm", "00");
        cv.put("lody", "00");
        cv.put("loth", "00");
        cv.put("lotm", "00");
        cv.put("lots", "00");
        cv.put("lcdd", "00");
        cv.put("lcdm", "00");
        cv.put("lcdy", "00");
        cv.put("lcth", "00");
        cv.put("lctm", "00");
        cv.put("lcts", "00");
        cv.put("totaltime", "0");
        this.sqldb.insert(TABLE_NAME, null, cv);
    }

    public void deleteapp(String pkg) {
        this.sqldb.delete(TABLE_NAME, "package='" + pkg + "'", null);
    }

    public void insertbackground(String bgtype, String bg) {
        ContentValues cv = new ContentValues();
        cv.put("bgtype", bgtype);
        cv.put("bg", bg);
        cv.put("seta", "false");
        this.sqldb.insert(TABLE_NAME6, null, cv);
    }

    public void setbackground(String bgtype, String bg) {
        ContentValues cv = new ContentValues();
        cv.put("seta", "false");
        this.sqldb.update(TABLE_NAME6, cv, null, null);
        cv.clear();
        cv.put("seta", "true");
        this.sqldb.update(TABLE_NAME6, cv, "bgtype='" + bgtype + "' and bg='" + bg + "'", null);
    }

    public Cursor getbackgroundcursor() {
        return this.sqldb.rawQuery("select bgtype,bg,seta from background order by id desc", null);
    }

    public boolean checkbackground(String bg) {
        String str = "false";
        try {
            Cursor cursor = this.sqldb.rawQuery("select * from background where bg='" + bg + "'", null);
            if (cursor.getCount() >= DATABASE_VERSION) {
                str = "true";
            }
            cursor.close();
        } catch (Exception e) {
        }
        if (str.contains("true")) {
            return true;
        }
        return false;
    }

    public void deletebackground(String bg) {
        String str = "false";
        try {
            Cursor cursor = this.sqldb.rawQuery("select seta from background where bg='" + bg + "'", null);
            cursor.moveToNext();
            str = cursor.getString(0);
            cursor.close();
        } catch (Exception e) {
        }
        if (str.contains("true")) {
            this.sqldb.delete(TABLE_NAME6, "bg='" + bg + "'", null);
            ContentValues cv = new ContentValues();
            cv.put("seta", "true");
            this.sqldb.update(TABLE_NAME6, cv, "id='1'", null);
            return;
        }
        this.sqldb.delete(TABLE_NAME6, "bg='" + bg + "'", null);
    }

    public Cursor getbackground() {
        return this.sqldb.rawQuery("select bgtype,bg from background where seta='true' ", null);
    }

    public Cursor getbackground1() {
        ContentValues cv = new ContentValues();
        cv.put("seta", "true");
        this.sqldb.update(TABLE_NAME6, cv, "id='1'", null);
        return this.sqldb.rawQuery("select bgtype,bg from background where bg='com.applockthree' ", null);
    }

    public void insertapphacklog(String pkg, String name, String password, String date, String time) {
        ContentValues cv = new ContentValues();
        cv.put("package", pkg);
        cv.put("name", name);
        cv.put("password", password);
        cv.put("date", date);
        cv.put("time", time);
        this.sqldb.insert(TABLE_NAME3, null, cv);
    }

    public Cursor getapphacklog() {
        return this.sqldb.rawQuery("select package,name,password,date,time from apphacklog order by id desc", null);
    }

    public Cursor getfastunlock() {
        return this.sqldb.rawQuery("select * from fastunlock", null);
    }

    public String getfastunlockm1() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select method1 from fastunlock", null);
            cursor.moveToNext();
            str = cursor.getString(0);
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public String getfastunlockm2() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select method2 from fastunlock", null);
            cursor.moveToNext();
            str = cursor.getString(0);
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setfastunlock(String str, String str1, String str2) {
        ContentValues cv = new ContentValues();
        cv.put(TABLE_NAME5, str);
        cv.put("method1", str1);
        cv.put("method2", str2);
        this.sqldb.update(TABLE_NAME5, cv, null, null);
    }

    public String gethiddencamera() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select hiddencamera from theme", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim().toString();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setlchange(String str) {
        ContentValues cv = new ContentValues();
        cv.put("lchange", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getlchange() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select lchange from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim().toString();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void sethiddencamera(String str) {
        ContentValues cv = new ContentValues();
        cv.put("hiddencamera", str);
        this.sqldb.insert(TABLE_NAME2, null, cv);
    }

    public String getsound() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select sound from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0);
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public String getfp() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select fp from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim().toString();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setfp(String str) {
        ContentValues cv = new ContentValues();
        cv.put("fp", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public void inserttheme(String itheme, String ithemepkg, String ithemebg) {
        String v = "0";
        try {
            Cursor cursor = this.sqldb.rawQuery("select * from theme where ithemepkg='" + ithemepkg + "'", null);
            if (cursor.getCount() >= DATABASE_VERSION) {
                v = "1";
            }
            cursor.close();
        } catch (Exception e) {
        }
        if (v.contains("0")) {
            ContentValues cv = new ContentValues();
            cv.put("itheme", itheme);
            cv.put("ithemepkg", ithemepkg);
            cv.put("ithemebg", ithemebg);
            this.sqldb.insert(TABLE_NAME2, null, cv);
        }
    }

    public void deletetheme(String pkg) {
        this.sqldb.delete(TABLE_NAME2, "ithemepkg='" + pkg + "'", null);
    }

    public Cursor getalltheme() {
        return this.sqldb.rawQuery("select itheme,ithemepkg,ithemebg from theme order by id desc", null);
    }

    public String getlocktype() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select locktype from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0);
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setlocktype(String str) {
        ContentValues cv = new ContentValues();
        cv.put("locktype", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getposition() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select position from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0);
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setposition(String str) {
        ContentValues cv = new ContentValues();
        cv.put("position", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public Cursor getallapp() {
        return this.sqldb.rawQuery("select * from apps order by name", null);
    }

    public int gettotalapp() {
        Integer str = Integer.valueOf(0);
        try {
            Cursor cursor = this.sqldb.rawQuery("select * from apps order by name", null);
            if (cursor.getCount() <= 0) {
                str = Integer.valueOf(0);
            } else {
                str = Integer.valueOf(cursor.getCount());
            }
            cursor.close();
        } catch (Exception e) {
        }
        return str.intValue();
    }

    public boolean getpackageavilable(String packag) {
        boolean str = false;
        try {
            Cursor cursor = this.sqldb.rawQuery("select * from apps where package='" + packag + "'", null);
            if (cursor.getCount() >= DATABASE_VERSION) {
                str = true;
            } else {
                str = false;
            }
            cursor.close();
        } catch (Exception e) {
        }
        return str;
    }

    public void insertlockbypackage(String packag, String lock) {
        ContentValues cv = new ContentValues();
        cv.put("lock", lock);
        this.sqldb.update(TABLE_NAME, cv, "package='" + packag.trim() + "'", null);
    }

    public void setallappunlock() {
        ContentValues cv = new ContentValues();
        cv.put("lock", "false");
        cv.put("password", BuildConfig.FLAVOR);
        this.sqldb.update(TABLE_NAME, cv, "package!='com.android.packageinstaller' || package!='com.google.android.packageinstaller'", null);
    }

    public void insertcurrentapp(String ca) {
        ContentValues cv = new ContentValues();
        cv.put("currentapp", ca);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getapplockbypackage(String pakage) {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select lock from apps where package='" + pakage + "'", null);
            if (cursor.getCount() >= DATABASE_VERSION) {
                cursor.moveToNext();
                if (cursor.getString(0).contains("true")) {
                    str = "true";
                } else {
                    str = "false";
                }
            } else {
                str = "false";
            }
            cursor.close();
        } catch (Exception e) {
        }
        return str;
    }

    public String getcurrentapp() {
        String str = " ";
        try {
            Cursor cursor = this.sqldb.rawQuery("select currentapp from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).toString();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public String getapppassword(String pkg) {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select password from apps where package='" + pkg + "'", null);
            if (cursor.getCount() >= DATABASE_VERSION) {
                cursor.moveToNext();
                str = cursor.getString(0).toString();
            } else {
                str = "spp";
            }
            cursor.close();
        } catch (Exception e) {
        }
        return str;
    }

    public void setapppassword(String pkg, String password) {
        ContentValues cv = new ContentValues();
        cv.put("password", password);
        this.sqldb.update(TABLE_NAME, cv, "package='" + pkg + "'", null);
    }

    public String getisettingsound() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select sound from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setitheme(String itheme, String ithemepkg, String ithemebg) {
        ContentValues cv = new ContentValues();
        cv.put("itheme", itheme);
        cv.put("ithemepkg", ithemepkg);
        cv.put("ithemebg", ithemebg);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public Cursor getitheme() {
        return this.sqldb.rawQuery("select itheme,ithemepkg,ithemebg from setting", null);
    }

    public void setisettingsound(String str) {
        ContentValues cv = new ContentValues();
        cv.put("sound", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getisettingvibrate() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select vibrate from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettingvibrate(String str) {
        ContentValues cv = new ContentValues();
        cv.put("vibrate", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getisettingnotification() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select notification from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettingnotification(String str) {
        ContentValues cv = new ContentValues();
        cv.put("notification", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getisettingasar() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select asar from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettingasar(String str) {
        ContentValues cv = new ContentValues();
        cv.put("asar", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getisettingiapplock() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select iapplock from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettingiapplock(String str) {
        ContentValues cv = new ContentValues();
        cv.put(DATABASE_NAME, str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public  String getisettingquestion() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select question from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettingquestion(String str) {
        ContentValues cv = new ContentValues();
        cv.put("question", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getisettinganswer() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select answer from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettinganswer(String str) {
        ContentValues cv = new ContentValues();
        cv.put("answer", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getisettinglanguage() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select language from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettinglanguage(String str) {
        ContentValues cv = new ContentValues();
        cv.put("language", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getisettinghideiapplock() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select hideiapplock from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setisettinghideiapplock(String str) {
        ContentValues cv = new ContentValues();
        cv.put("hideiapplock", str);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public Cursor getisetting() {
        return this.sqldb.rawQuery("select iapplock,sound,vibrate,notification,asar,language,hideiapplock from setting", null);
    }
    public void setmasterpassword(String masterpassword) {
        ContentValues cv = new ContentValues();
        cv.put("masterpassword", masterpassword);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getmasterpassword() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select masterpassword from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).toString();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public boolean matchpassword(String password) {
        try {
            String str = getapppassword(getcurrentapp());
            str.trim();
            String str1 = getmasterpassword();
            str1.trim();
            if (str.contentEquals(password) || str1.contentEquals(password)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void setinstaller(String setinstaller) {
        ContentValues cv = new ContentValues();
        cv.put("installer", setinstaller);
        this.sqldb.update(TABLE_NAME1, cv, null, null);
    }

    public String getinstaller() {
        String str = BuildConfig.FLAVOR;
        try {
            Cursor cursor = this.sqldb.rawQuery("select installer from setting", null);
            cursor.moveToNext();
            str = cursor.getString(0).trim();
            cursor.close();
            return str;
        } catch (Exception e) {
            return str;
        }
    }

    public void setallapplockbymasterpassword() {
        ContentValues cv = new ContentValues();
        cv.put("password", getmasterpassword());
        this.sqldb.update(TABLE_NAME, cv, "lock='true'", null);
    }

    public void setallapplockbymasterpassword1() {
        ContentValues cv = new ContentValues();
        cv.put("password", getmasterpassword());
        cv.put("lock", "true");
        this.sqldb.update(TABLE_NAME, cv, "package!='com.android.packageinstaller' || package!='com.google.android.packageinstaller'", null);
    }

    public void setloapp(String pkg, String day, String month, String year, String hour, String minute, String second) {
        ContentValues cv = new ContentValues();
        cv.put("lodd", day);
        cv.put("lodm", month);
        cv.put("lody", year);
        cv.put("loth", hour);
        cv.put("lotm", minute);
        cv.put("lots", second);
        this.sqldb.update(TABLE_NAME, cv, "package='" + pkg + "'", null);
    }

    public void setlcapp(String pkg, String day, String month, String year, String hour, String minute, String second) {
        Integer ohour = Integer.valueOf(0);
        Integer ominute = Integer.valueOf(0);
        Integer osecond = Integer.valueOf(0);
        Integer oday = Integer.valueOf(0);
        Integer omonth = Integer.valueOf(0);
        Integer oyear = Integer.valueOf(0);
        Integer total = Integer.valueOf(0);
        Integer temp = Integer.valueOf(0);
        try {
            Cursor cursor = this.sqldb.rawQuery("select loth,lotm,lodd,totaltime from apps where package='" + pkg + "'", null);
            cursor.moveToNext();
            ohour = Integer.valueOf(cursor.getString(0));
            ominute = Integer.valueOf(cursor.getString(DATABASE_VERSION));
            oday = Integer.valueOf(cursor.getString(2));
            total = Integer.valueOf(cursor.getString(3));
            cursor.close();
        } catch (Exception e) {
        }
        if (Integer.parseInt(minute) > ominute.intValue()) {
            temp = Integer.valueOf(temp.intValue() + (Integer.parseInt(minute) - ominute.intValue()));
        }
        if (Integer.parseInt(hour) > ohour.intValue()) {
            temp = Integer.valueOf(temp.intValue() + ((Integer.parseInt(hour) - ohour.intValue()) * 60));
        }
        if (Integer.parseInt(day) > oday.intValue()) {
            temp = Integer.valueOf(temp.intValue() + (((Integer.parseInt(day) - oday.intValue()) * 24) * 60));
        }
        ContentValues cv = new ContentValues();
        cv.put("lcdd", day);
        cv.put("lcdm", month);
        cv.put("lcdy", year);
        cv.put("lcth", hour);
        cv.put("lctm", minute);
        cv.put("lcts", second);
        cv.put("totaltime", Integer.valueOf(total.intValue() + temp.intValue()));
        this.sqldb.update(TABLE_NAME, cv, "package='" + pkg + "'", null);
    }

    public Cursor gethiddenapplog() {
        return this.sqldb.rawQuery("select package,lodd,lodm,lody,loth,lotm,lots,lcdd,lcdm,lcdy,lcth,lctm,lcts,totaltime,name from apps order by name", null);
    }
}
