package com.applockthree;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class emailquestion extends AppCompatActivity {
    private TextView answer;
    private DataBase db;
    private String[] items;
    private  Spinner question;
    private Button save;
    private Typeface tf;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emailquestion);
        db = new DataBase(this);
        question = (Spinner) findViewById(R.id.spinnerquestion);
        String[] s = new String[16];
        s[1] = getResources().getString(R.string.q1);
        s[2] = getResources().getString(R.string.q2);
        s[3] = getResources().getString(R.string.q3);
        s[4] = getResources().getString(R.string.q4);
        s[5] = getResources().getString(R.string.q5);
        s[6] = getResources().getString(R.string.q6);
        s[7] = getResources().getString(R.string.q7);
        s[8] = getResources().getString(R.string.q8);
        s[9] = getResources().getString(R.string.q9);
        s[10] = getResources().getString(R.string.q10);
        s[11] = getResources().getString(R.string.q11);
        s[12] = getResources().getString(R.string.q12);
        s[13] = getResources().getString(R.string.q13);
        s[14] = getResources().getString(R.string.q14);
        s[15] = getResources().getString(R.string.q15);
        items = new String[]{s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11], s[12], s[13], s[14], s[15]};
        answer = (TextView) findViewById(R.id.txtanswerset);
        save = (Button) findViewById(R.id.btnstart);
        set();
        save.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String val = "^[a-zA-Z ]{1,1}[a-zA-Z0-9 _@*.,]{2,}";
                if (emailquestion.this.answer.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getBaseContext(), emailquestion.this.getResources().getString(R.string.enter_answer), Toast.LENGTH_LONG).show();
                    return;
                }
              db.setisettingquestion(String.valueOf(question.getSelectedItemPosition()));
               db.setisettinganswer(answer.getText().toString().trim());
                Intent intent1 = new Intent(getBaseContext(), MainActivity.class);
                intent1.putExtra("ft", "true");
              startActivity(intent1);
                finish();
            }
        });
    }

    private void set() {
        setQuestion();
        if (!db.getisettingquestion().trim().isEmpty()) {
            question.setSelection(Integer.parseInt(db.getisettingquestion()));
        }
        if (!db.getisettinganswer().trim().isEmpty()) {
            answer.setText(db.getisettinganswer());
        }
    }

   public <ViewGroup> void setQuestion() {
        Spinner mySpinner = (Spinner) findViewById(R.id.spinnerquestion);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, this.items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) getView(position, convertView, parent);
                v.setTextSize(13.0f);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) getView(position, convertView, parent);
                v.setTextSize(13.0f);
                return v;
            }
        };
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(adapter1);
    }

    public void onBackPressed() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
    }

    protected void onResume() {
        if (db.getmasterpassword().trim().toString().length() < 1) {
            startActivity(new Intent(getBaseContext(), firsttime.class));
        } else if (db.getisettinganswer().trim().length() > 0) {
            Intent intent1 = new Intent(getBaseContext(), MainActivity.class);
            intent1.putExtra("ft", "true");
            startActivity(intent1);
            finish();
        }
        super.onResume();
    }
}






