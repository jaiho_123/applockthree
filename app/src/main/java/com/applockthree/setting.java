package com.applockthree;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils.SimpleStringSplitter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class setting extends Fragment {
    private String TAG = "applockthree";
    private SwitchCompat autostartafterrestart;
    private Cursor cursor;
    private DataBase db;
    private Handler handler;
    private TextView language, language1;
    private RelativeLayout language3;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private SwitchCompat notification, sound, taskprevent, vibrate, applock;
    private TextView questionanswer, questionanswer1;
    private RelativeLayout questionanswer3;
    private Vibrator vb;
    private View view;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = new DataBase(getContext());
        vb = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        cursor = db.getisetting();
        cursor.moveToNext();
        handler = new Handler();
        view = inflater.inflate(R.layout.setting_layout, container, false);

        applock = (SwitchCompat) view.findViewById(R.id.isettingiapplock);
        sound = (SwitchCompat) view.findViewById(R.id.isettingsound);
        vibrate = (SwitchCompat) view.findViewById(R.id.isettingvibrate);
        notification = (SwitchCompat) view.findViewById(R.id.isettingnotification);
        autostartafterrestart = (SwitchCompat) view.findViewById(R.id.isettingautostartafterrestart);
        taskprevent = (SwitchCompat) view.findViewById(R.id.isettingtaskprevent);
        language1 = (TextView) view.findViewById(R.id.txtlanguage1);
        language = (TextView) view.findViewById(R.id.txtlanguage);
        language3 = (RelativeLayout) view.findViewById(R.id.txtlanguage3);
        questionanswer = (TextView) view.findViewById(R.id.txtquestionanswer);
        questionanswer1 = (TextView) view.findViewById(R.id.txtquestionanswer1);
        questionanswer3 = (RelativeLayout) view.findViewById(R.id.txtquestionanswer3);
        mFragmentManager = getActivity().getSupportFragmentManager();
        set();
        taskprevent.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isAccessibilitySettingsOn(getContext())) {
                    taskprevent.setChecked(true);
                    return;
                }
                Toast.makeText(getContext(), R.string.enableaccessiblity, Toast.LENGTH_LONG).show();
                startActivity(new Intent("android.settings.ACCESSIBILITY_SETTINGS"));
                taskprevent.setChecked(false);
            }
        });
        applock.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MediaPlayer mp;
                if (applock.isChecked()) {
                    db.setisettingiapplock("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingiapplock("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
            }
        });
        sound.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MediaPlayer mp;
                if (sound.isChecked()) {
                    db.setisettingsound("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingsound("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
            }
        });
        vibrate.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MediaPlayer mp;
                if (vibrate.isChecked()) {
                    db.setisettingvibrate("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingvibrate("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
            }
        });
        notification.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MediaPlayer mp;
                if (notification.isChecked()) {
                    db.setisettingnotification("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingnotification("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
            }
        });
        autostartafterrestart.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MediaPlayer mp;
                if (autostartafterrestart.isChecked()) {
                    db.setisettingasar("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingasar("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
            }
        });

        language1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                language();
            }
        });
        language.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                language();
            }
        });
        language3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                language();
            }
        });
        questionanswer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                questionanswer();
            }
        });
        questionanswer1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                questionanswer();
            }
        });
        questionanswer3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                questionanswer();
            }
        });
        return view;
    }

    private void set() {
        if (cursor.getString(0).contains("true")) {
            applock.setChecked(true);
        } else {
            applock.setChecked(false);
        }
        if (cursor.getString(1).contains("true")) {
            sound.setChecked(true);
        } else {
            sound.setChecked(false);
        }
        if (cursor.getString(2).contains("true")) {
            vibrate.setChecked(true);
        } else {
            vibrate.setChecked(false);
        }
        if (cursor.getString(3).contains("true")) {
            notification.setChecked(true);
        } else {
            notification.setChecked(false);
        }
        if (cursor.getString(4).contains("true")) {
            autostartafterrestart.setChecked(true);
        } else {
            autostartafterrestart.setChecked(false);
        }
        language1.setText(cursor.getString(5));
        cursor.close();
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        String service = getActivity().getPackageName() + "/" + MyService.class.getCanonicalName();
        try {
            accessibilityEnabled = Secure.getInt(mContext.getApplicationContext().getContentResolver(), "accessibility_enabled");
        } catch (SettingNotFoundException e) {
        }
        SimpleStringSplitter mStringColonSplitter = new SimpleStringSplitter(':');
        if (accessibilityEnabled == 1) {
            String settingValue = Secure.getString(mContext.getApplicationContext().getContentResolver(), "enabled_accessibility_services");
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    if (mStringColonSplitter.next().equalsIgnoreCase(service)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void language() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.language);
        Button btnfr = (Button) dialog.findViewById(R.id.btnfr);
        Button btnhi = (Button) dialog.findViewById(R.id.btnhi);
        (dialog.findViewById(R.id.btnen)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                db.setisettinglanguage("English");
                language1.setText(getResources().getString(R.string.English));
                lchange("en");
                dialog.dismiss();
            }
        });
        btnfr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                db.setisettinglanguage("Franch");
                language1.setText(getResources().getString(R.string.Franch));
                lchange("fr");
                dialog.dismiss();
            }
        });
        btnhi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                db.setisettinglanguage("Hindi");
                language1.setText(getResources().getString(R.string.Hindi));
                lchange("hi");
                dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void lchange(String code) {
        db.setlchange("true");
        Locale locale = new Locale(code);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getContext().getResources().updateConfiguration(config, null);
        mFragmentManager.beginTransaction().replace(R.id.containerView, new setting()).commit();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype()));
        intent.putExtra("type", "3");
        getContext().startService(intent);
        db.setposition("setting");
    }


    public void questionanswer() {
        mFragmentManager.beginTransaction().replace(R.id.containerView, new questionansweremail()).commit();
        db.setposition(getString(R.string.quest));
    }

    public void onResume() {
        if (taskprevent != null) {
            if (isAccessibilitySettingsOn(getContext())) {
                taskprevent.setChecked(true);
                Log.e("resume m acessiblity-","true");
            } else {
                taskprevent.setChecked(false);
                Log.e("resume m acessiblity-","false");
            }
        }
        super.onResume();
    }
}
