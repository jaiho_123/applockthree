package com.applockthree;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applockthree.utils.Lock9View;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;

public class PatternLockScreencustom extends Service implements OnClickListener {
    private RelativeLayout Pinlockll;
    private int cnt1 = 0;
    private RelativeLayout confirmPatternContainer;
    private DataBase db;
    private RelativeLayout enterPatternContainer;
    private Lock9View lockViewConfirm, lockViewFirstTry;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String pkg, pwd, string1, string2;
    private PackageManager pm;
    private TextView txtname;
    private Vibrator vb;

    public void onCreate() {
        super.onCreate();
        db = new DataBase(this);
        pwd = BuildConfig.FLAVOR;
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        pkg = BuildConfig.FLAVOR;
        string1 = getResources().getString(R.string.Enter_new_custom_password);
        string2 = getResources().getString(R.string.Comfirm_custom_password);
        pm = getBaseContext().getPackageManager();
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        cnt1 = 1;
    }

    private void initLayouts() {
        Resources resources1;
        LayoutParams wmlp = new LayoutParams(-1, -1, 2003, 256, -3);
        wmlp.gravity = 17;
        wmlp.screenOrientation = 1;
        wmlp.alpha = 1.0f;
        wmlp.flags = 262144;
        wmlp.flags &= -2097297;
        wmlp.flags = 256;
        wmlp.format = -1;
        wmlp.token = null;
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        mLayout = new LinearLayout(this) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() != 4 || event.getAction() != 0) {
                    return super.dispatchKeyEvent(event);
                }
                Intent startHome = new Intent(getBaseContext(), MainActivity.class);
                startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startHome);
                stopSelf();
                return true;
            }
        };
        mLayout.setLayoutParams(new LayoutParams(-1, -1));
        View view = LayoutInflater.from(this).inflate(R.layout.pattern_lock_screen_change_password, mLayout, true);
        Pinlockll = (RelativeLayout) view.findViewById(R.id.Pinlockll);
        txtname = (TextView) view.findViewById(R.id.appname);
        txtname.setText(string1);
        enterPatternContainer = (RelativeLayout) view.findViewById(R.id.enterPattern);
        confirmPatternContainer = (RelativeLayout) view.findViewById(R.id.confirmPattern);
        lockViewFirstTry = (Lock9View) view.findViewById(R.id.lock_viewFirstTry);
        lockViewConfirm = (Lock9View) view.findViewById(R.id.lock_viewConfirm);
        Addmob(view);
        lockViewFirstTry.setCallBack(new Lock9View.CallBack() {
            public void onFinish(String password) {
                enterPatternContainer.setVisibility(View.INVISIBLE);
                cnt1 = 2;
                setname();
                confirmPatternContainer.setVisibility(View.VISIBLE);
                pwd = password;
            }
        });
        lockViewConfirm.setCallBack(new Lock9View.CallBack() {
            public void onFinish(String password) {
                if (password.contentEquals(pwd)) {
                    db.insertlockbypackage(pkg, "true");
                    db.setapppassword(pkg, password);
                    stopSelf();
                } else if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(200);
                }
            }
        });
        Cursor cursor = db.getitheme();
        cursor.moveToNext();
        String itheme = cursor.getString(0);
        String ithemepkg = cursor.getString(1);
        String ithemebg = cursor.getString(2);
        cursor.close();
        try {
            Resources resources = pm.getResourcesForApplication(ithemepkg);
            int pon = resources.getIdentifier(itheme + "pon", "drawable", ithemepkg);
            int poff = resources.getIdentifier(itheme + "poff", "drawable", ithemepkg);
            int c1 = resources.getIdentifier(ithemepkg + ":string/itheme1color1", null, null);
            int c2 = resources.getIdentifier(ithemepkg + ":string/itheme1color2", null, null);
            int c3 = resources.getIdentifier(ithemepkg + ":string/itheme1color3", null, null);
            int c4 = resources.getIdentifier(ithemepkg + ":string/itheme1color4", null, null);
            Integer color1 = Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c1, null)));
            Integer color2 = Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c2, null)));
            Integer color3 = Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c3, null)));
            Integer color4 = Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c4, null)));
            lockViewFirstTry.set(resources.getDrawable(pon), resources.getDrawable(poff), color1, color2, color3, color4);
            lockViewConfirm.set(resources.getDrawable(pon), resources.getDrawable(poff), color1, color2, color3, color4);
            Cursor cursor1 = db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 = db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 = pm.getResourcesForApplication(cursor1.getString(1));
                    Resources resources2 = resources1;
                    Pinlockll.setBackground(resources1.getDrawable(resources2.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                    Pinlockll.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e2) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
        } catch (NameNotFoundException e3) {
            e3.printStackTrace();
        }
        txtname.setText(string1);
        try {
            mWindowManager.addView(mLayout, wmlp);
        } catch (RuntimeException e4) {
            stopSelf();
        }
    }
    public void Addmob(View view) {
        Log.e("FourSquareLockScreen","Activity");
        AdView adView = (AdView) view.findViewById(R.id.adView_patnlockchnpswd);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }

    private void vcheck() {
        try {
            Cursor cursor1 = db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
                db.deletebackground(cursor1.getString(1));
                Resources resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                Pinlockll.setBackgroundDrawable(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        vcheck();
        try {
            pkg = intent.getStringExtra("pkg");
        } catch (Exception e) {
        }
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (mLayout == null) {
            initLayouts();
        }
        return START_STICKY;
    }

    public void onClick(View v) {
    }

    public void setname() {
        if (cnt1 == 1) {
            txtname.setText(string1);
        } else {
            txtname.setText(string2);
        }
    }

    public void onDestroy() {
        removeLockScreen();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void removeLockScreen() {
        pkg = BuildConfig.FLAVOR;
        if (mLayout != null) {
            try {
                mWindowManager.removeView(mLayout);
            } catch (IllegalStateException e) {
            }
            mLayout = null;
        }
    }
}
