package com.applockthree.icropimage;

import android.app.Activity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;

public class iMonitoredActivity extends Activity {
    private final ArrayList<LifeCycleListener> mListeners = new ArrayList();

    public interface LifeCycleListener {
        void onActivityCreated(iMonitoredActivity iMonitoredActivity);

        void onActivityDestroyed(iMonitoredActivity iMonitoredActivity);

        void onActivityStarted(iMonitoredActivity iMonitoredActivity);

        void onActivityStopped(iMonitoredActivity iMonitoredActivity);
    }

    public static class LifeCycleAdapter implements LifeCycleListener {
        public void onActivityCreated(iMonitoredActivity activity) {
        }

        public void onActivityDestroyed(iMonitoredActivity activity) {
        }

        public void onActivityStarted(iMonitoredActivity activity) {
        }

        public void onActivityStopped(iMonitoredActivity activity) {
        }
    }

    public void addLifeCycleListener(LifeCycleListener listener) {
        if (!this.mListeners.contains(listener)) {
            this.mListeners.add(listener);
        }
    }

    public void removeLifeCycleListener(LifeCycleListener listener) {
        this.mListeners.remove(listener);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Iterator it = this.mListeners.iterator();
        while (it.hasNext()) {
            ((LifeCycleListener) it.next()).onActivityCreated(this);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        Iterator it = this.mListeners.iterator();
        while (it.hasNext()) {
            ((LifeCycleListener) it.next()).onActivityDestroyed(this);
        }
    }

    protected void onStart() {
        super.onStart();
        Iterator it = this.mListeners.iterator();
        while (it.hasNext()) {
            ((LifeCycleListener) it.next()).onActivityStarted(this);
        }
    }

    protected void onStop() {
        super.onStop();
        Iterator it = this.mListeners.iterator();
        while (it.hasNext()) {
            ((LifeCycleListener) it.next()).onActivityStopped(this);
        }
    }
}
