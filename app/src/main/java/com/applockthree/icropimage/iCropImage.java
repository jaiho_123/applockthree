package com.applockthree.icropimage;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PointF;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.applockthree.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;

public class iCropImage extends iMonitoredActivity {
    public static final String ACTION_INLINE_DATA = "inline-data";
    public static final String ASPECT_X = "aspectX";
    public static final String ASPECT_Y = "aspectY";
    public static final int CANNOT_STAT_ERROR = -2;
    public static final String CIRCLE_CROP = "circleCrop";
    public static final String IMAGE_PATH = "image-path";
    public static final int NO_STORAGE_ERROR = -1;
    public static final String OUTPUT_X = "outputX";
    public static final String OUTPUT_Y = "outputY";
    public static final String RETURN_DATA = "return-data";
    public static final String RETURN_DATA_AS_BITMAP = "data";
    public static final String SCALE = "scale";
    public static final String SCALE_UP_IF_NEEDED = "scaleUpIfNeeded";
    private static final String iTAG = "iCropImage";
    private int imAspectX;
    private int imAspectY;
    private Bitmap imBitmap;
    private boolean imCircleCrop = false;
    private ContentResolver imContentResolver;
    private final iBitmapManager.ThreadSet imDecodingThreads = new iBitmapManager.ThreadSet();
    private boolean imDoFaceDetection = true;
    private final Handler imHandler = new Handler();
    private String imImagePath;
    private iCropIImageView imImageView;
    private int imOutputX;
    private int imOutputY;
    private Uri imSaveUri = null;
    private boolean imScale;
    private boolean imScaleUp = true;
    iHighlightView mCrop;
    Runnable mRunFaceDetection = new Runnable() {
        float imScale = 1.0f;
        Face[] mFaces = new Face[3];
        Matrix mImageMatrix;
        int mNumFaces;

        private void handleFace(Face f) {
            PointF midPoint = new PointF();
            int r = ((int) (f.eyesDistance() * this.imScale)) * 2;
            f.getMidPoint(midPoint);
            midPoint.x *= this.imScale;
            midPoint.y *= this.imScale;
            int midX = (int) midPoint.x;
            int midY = (int) midPoint.y;
            iHighlightView hv = new iHighlightView(imImageView);
            Rect imageRect = new Rect(0, 0, imBitmap.getWidth(), imBitmap.getHeight());
            RectF faceRect = new RectF((float) midX, (float) midY, (float) midX, (float) midY);
            faceRect.inset((float) (-r), (float) (-r));
            if (faceRect.left < 0.0f) {
                faceRect.inset(-faceRect.left, -faceRect.left);
            }
            if (faceRect.top < 0.0f) {
                faceRect.inset(-faceRect.top, -faceRect.top);
            }
            if (faceRect.right > ((float) imageRect.right)) {
                faceRect.inset(faceRect.right - ((float) imageRect.right), faceRect.right - ((float) imageRect.right));
            }
            if (faceRect.bottom > ((float) imageRect.bottom)) {
                faceRect.inset(faceRect.bottom - ((float) imageRect.bottom), faceRect.bottom - ((float) imageRect.bottom));
            }
            Matrix matrix = this.mImageMatrix;
            boolean access$500 = imCircleCrop;
            boolean z = (imAspectX == 0 || imAspectY == 0) ? false : true;
            hv.setup(matrix, imageRect, faceRect, access$500, z);
            imImageView.add(hv);
        }

        private void makeDefault() {
            boolean z = false;
            iHighlightView hv = new iHighlightView(imImageView);
            int width = imBitmap.getWidth();
            int height = imBitmap.getHeight();
            Rect imageRect = new Rect(0, 0, width, height);
            int cropWidth = (Math.min(width, height) * 4) / 5;
            int cropHeight = cropWidth;
            if (!(imAspectX == 0 || imAspectY == 0)) {
                if (imAspectX > imAspectY) {
                    cropHeight = (imAspectY * cropWidth) / imAspectX;
                } else {
                    cropWidth = (imAspectX * cropHeight) / imAspectY;
                }
            }
            int x = (width - cropWidth) / 2;
            int y = (height - cropHeight) / 2;
            RectF cropRect = new RectF((float) x, (float) y, (float) (x + cropWidth), (float) (y + cropHeight));
            Matrix matrix = this.mImageMatrix;
            boolean access$500 = imCircleCrop;
            if (!(imAspectX == 0 || imAspectY == 0)) {
                z = true;
            }
            hv.setup(matrix, imageRect, cropRect, access$500, z);
            imImageView.mIHighlightViews.clear();
            imImageView.add(hv);
        }

        private Bitmap prepareBitmap() {
            if (imBitmap == null) {
                return null;
            }
            if (imBitmap.getWidth() > 256) {
                this.imScale = 256.0f / ((float) imBitmap.getWidth());
            }
            Matrix matrix = new Matrix();
            matrix.setScale(this.imScale, this.imScale);
            return Bitmap.createBitmap(imBitmap, 0, 0, imBitmap.getWidth(), imBitmap.getHeight(), matrix, true);
        }

        public void run() {
            this.mImageMatrix = imImageView.getImageMatrix();
            Bitmap faceBitmap = prepareBitmap();
            this.imScale = 1.0f / imScale;
            if (faceBitmap != null && imDoFaceDetection) {
                mNumFaces = new FaceDetector(faceBitmap.getWidth(), faceBitmap.getHeight(), mFaces.length).findFaces(faceBitmap, mFaces);
            }
            if (!(faceBitmap == null || faceBitmap == imBitmap)) {
                faceBitmap.recycle();
            }
            imHandler.post(new Runnable() {
                public void run() {
                    boolean z;
                    iCropImage iCropImage = iCropImage.this;
                    if (mNumFaces > 1) {
                        z = true;
                    } else {
                        z = false;
                    }
                    iCropImage.mWaitingToPick = z;
                    if (mNumFaces > 0) {
                        for (int i = 0; i < mNumFaces; i++) {
                            handleFace(mFaces[i]);
                        }
                    } else {
                        makeDefault();
                    }
                    imImageView.invalidate();
                    if (imImageView.mIHighlightViews.size() == 1) {
                        mCrop = imImageView.mIHighlightViews.get(0);
                        mCrop.setFocus(true);
                    }
                    if (mNumFaces > 1) {
                        Toast.makeText(iCropImage.this, "Multi face crop help", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    };
    boolean mSaving;
    boolean mWaitingToPick;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.imContentResolver = getContentResolver();
        requestWindowFeature(1);
        setContentView(R.layout.cropimage);
        imImageView = (iCropIImageView) findViewById(R.id.image);
        showStorageToast(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getString(CIRCLE_CROP) != null) {
                if (VERSION.SDK_INT > 11) {
                    imImageView.setLayerType(1, null);
                }
                imCircleCrop = true;
                imAspectX = 1;
                imAspectY = 1;
            }
            imImagePath = extras.getString(IMAGE_PATH);
            imSaveUri = getImageUri(imImagePath);
            imBitmap = getBitmap(imImagePath);
            if (extras.containsKey(ASPECT_X) && (extras.get(ASPECT_X) instanceof Integer)) {
                imAspectX = extras.getInt(ASPECT_X);
                if (extras.containsKey(ASPECT_Y) && (extras.get(ASPECT_Y) instanceof Integer)) {
                    imAspectY = extras.getInt(ASPECT_Y);
                    imOutputX = extras.getInt(OUTPUT_X);
                    imOutputY = extras.getInt(OUTPUT_Y);
                    imScale = extras.getBoolean(SCALE, true);
                    imScaleUp = extras.getBoolean(SCALE_UP_IF_NEEDED, true);
                } else {
                    throw new IllegalArgumentException("aspect_y must be integer");
                }
            }
            throw new IllegalArgumentException("aspect_x must be integer");
        }
        if (imBitmap == null) {
            Log.d(iTAG, "finish!!!");
            finish();
            return;
        }
        getWindow().addFlags(1024);
        findViewById(R.id.discard).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                setResult(0);
                finish();
            }
        });
        findViewById(R.id.save).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    onSaveClicked();
                } catch (Exception e) {
                    finish();
                }
            }
        });
        findViewById(R.id.rotateLeft).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                imBitmap = iUtil.rotateImage(imBitmap, -90.0f);
                imImageView.setImageRotateBitmapResetBase(new iRotateBitmap(imBitmap), true);
                mRunFaceDetection.run();
            }
        });
        findViewById(R.id.rotateRight).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                imBitmap = iUtil.rotateImage(imBitmap, 90.0f);
                imImageView.setImageRotateBitmapResetBase(new iRotateBitmap(imBitmap), true);
                mRunFaceDetection.run();
            }
        });
        startFaceDetection();
    }

    private Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }

    private Bitmap getBitmap(String path) {
        Uri uri = getImageUri(path);
        try {
            InputStream in = this.imContentResolver.openInputStream(uri);
            Options o = new Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight > 1024 || o.outWidth > 1024) {
                scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(1024.0d / ((double) Math.max(o.outHeight, o.outWidth))) / Math.log(0.5d))));
            }
            Options o2 = new Options();
            o2.inSampleSize = scale;
            in = this.imContentResolver.openInputStream(uri);
            Bitmap b = BitmapFactory.decodeStream(in, null, o2);
            in.close();
            return b;
        } catch (FileNotFoundException e) {
            Log.e(iTAG, "file " + path + " not found");
            return null;
        } catch (IOException e2) {
            Log.e(iTAG, "file " + path + " not found");
            return null;
        }
    }

    private void startFaceDetection() {
        if (!isFinishing()) {
            imImageView.setImageBitmapResetBase(imBitmap, true);
            iUtil.startBackgroundJob(this, null, "Please wait\u2026", new Runnable() {
                public void run() {
                    final CountDownLatch latch = new CountDownLatch(1);
                    final Bitmap b = imBitmap;
                    imHandler.post(new Runnable() {
                        public void run() {
                            if (!(b == imBitmap || b == null)) {
                                imImageView.setImageBitmapResetBase(b, true);
                                imBitmap.recycle();
                                imBitmap = b;
                            }
                            if (imImageView.getScale() == 1.0f) {
                                imImageView.center(true, true);
                            }
                            latch.countDown();
                        }
                    });
                    try {
                        latch.await();
                        mRunFaceDetection.run();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }, this.imHandler);
        }
    }

    private void onSaveClicked() throws Exception {
        if (!this.mSaving && this.mCrop != null) {
            this.mSaving = true;
            Rect r = this.mCrop.getCropRect();
            int width = r.width();
            int height = r.height();
            try {
                Bitmap croppedImage = Bitmap.createBitmap(width, height, this.imCircleCrop ? Config.ARGB_8888 : Config.RGB_565);
                if (croppedImage != null) {
                   final Bitmap b;
                    new Canvas(croppedImage).drawBitmap(this.imBitmap, r, new Rect(0, 0, width, height), null);
                    if (this.imCircleCrop) {
                        Canvas c = new Canvas(croppedImage);
                        Path p = new Path();
                        p.addCircle(((float) width) / 2.0f, ((float) height) / 2.0f, ((float) width) / 2.0f, Direction.CW);
                        c.clipPath(p, Op.DIFFERENCE);
                        c.drawColor(0, Mode.CLEAR);
                    }
                    if (!(this.imOutputX == 0 || this.imOutputY == 0)) {
                        if (this.imScale) {
                            Bitmap old = croppedImage;
                            croppedImage = iUtil.transform(new Matrix(), croppedImage, this.imOutputX, this.imOutputY, this.imScaleUp);
                            if (old != croppedImage) {
                                old.recycle();
                            }
                        } else {
                            b = Bitmap.createBitmap(this.imOutputX, this.imOutputY, Config.RGB_565);
                            Canvas canvas = new Canvas(b);
                            Rect srcRect = this.mCrop.getCropRect();
                            Rect dstRect = new Rect(0, 0, this.imOutputX, this.imOutputY);
                            int dx = (srcRect.width() - dstRect.width()) / 2;
                            int dy = (srcRect.height() - dstRect.height()) / 2;
                            srcRect.inset(Math.max(0, dx), Math.max(0, dy));
                            dstRect.inset(Math.max(0, -dx), Math.max(0, -dy));
                            canvas.drawBitmap(this.imBitmap, srcRect, dstRect, null);
                            croppedImage.recycle();
                            croppedImage = b;
                        }
                    }
                    Bundle myExtras = getIntent().getExtras();
                    if (myExtras == null || (myExtras.getParcelable(RETURN_DATA_AS_BITMAP) == null && !myExtras.getBoolean(RETURN_DATA))) {
                      final Bitmap bb = croppedImage;
                        iUtil.startBackgroundJob(this, null, getString(R.string.saving_image), new Runnable() {
                            public void run() {
                                saveOutput(bb);
                            }
                        }, imHandler);
                        return;
                    }
                    Bundle extras = new Bundle();
                    extras.putParcelable(RETURN_DATA_AS_BITMAP, croppedImage);
                    setResult(NO_STORAGE_ERROR, new Intent().setAction(ACTION_INLINE_DATA).putExtras(extras));
                    finish();
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }
    private void saveOutput(Bitmap r8) {
        throw new UnsupportedOperationException("Method not decompiled: com.applockthree.icropimage.iCropImage.saveOutput(android.graphics.Bitmap):void");
    }

    protected void onPause() {
        super.onPause();
        iBitmapManager.instance().cancelThreadDecoding(imDecodingThreads);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (imBitmap != null) {
            imBitmap.recycle();
        }
    }

    public static void showStorageToast(Activity activity) {
        showStorageToast(activity, calculatePicturesRemaining(activity));
    }

    public static void showStorageToast(Activity activity, int remaining) {
        String noStorageText = null;
        if (remaining == NO_STORAGE_ERROR) {
            if (Environment.getExternalStorageState().equals("checking")) {
                noStorageText = activity.getString(R.string.preparing_card);
            } else {
                noStorageText = activity.getString(R.string.no_storage_card);
            }
        } else if (remaining < 1) {
            noStorageText = activity.getString(R.string.not_enough_space);
        }
        if (noStorageText != null) {
            Toast.makeText(activity, noStorageText, Toast.LENGTH_LONG).show();
        }
    }

    public static int calculatePicturesRemaining(Activity activity) {
        try {
            String storageDirectory ;
            if ("mounted".equals(Environment.getExternalStorageState())) {
                storageDirectory = Environment.getExternalStorageDirectory().toString();
            } else {
                storageDirectory = activity.getFilesDir().toString();
            }
            StatFs stat = new StatFs(storageDirectory);
            return (int) ((((float) stat.getAvailableBlocks()) * ((float) stat.getBlockSize())) / 400000.0f);
        } catch (Exception e) {
            return CANNOT_STAT_ERROR;
        }
    }
}
