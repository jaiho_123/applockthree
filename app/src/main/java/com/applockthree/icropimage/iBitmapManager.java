package com.applockthree.icropimage;

import android.graphics.BitmapFactory.Options;

import java.util.Iterator;
import java.util.WeakHashMap;

public class iBitmapManager {
    private static final String iTAG = "iBitmapManager";
    private static iBitmapManager sManager = null;
    private final WeakHashMap<Thread, ThreadStatus> mThreadStatus = new WeakHashMap();

    private enum State {
        CANCEL,
        ALLOW
    }

    public static class ThreadSet implements Iterable<Thread> {
        private final WeakHashMap<Thread, Object> mWeakCollection = new WeakHashMap();

        public void add(Thread t) {
            this.mWeakCollection.put(t, null);
        }


        public Iterator<Thread> iterator() {
            return this.mWeakCollection.keySet().iterator();
        }
    }

    private static class ThreadStatus {
        public Options mOptions;
        public State mState;

        private ThreadStatus() {
            this.mState = State.ALLOW;
        }

        public String toString() {
            String s;
            if (this.mState == State.CANCEL) {
                s = "Cancel";
            } else if (this.mState == State.ALLOW) {
                s = "Allow";
            } else {
                s = "?";
            }
            return "thread state = " + s + ", options = " + this.mOptions;
        }
    }

    private iBitmapManager() {
    }

    private synchronized ThreadStatus getOrCreateThreadStatus(Thread t) {
        ThreadStatus status;
        status = (ThreadStatus) this.mThreadStatus.get(t);
        if (status == null) {
            status = new ThreadStatus();
            this.mThreadStatus.put(t, status);
        }
        return status;
    }




    public synchronized void cancelThreadDecoding(ThreadSet threads) {
        Iterator it = threads.iterator();
        while (it.hasNext()) {

        }
    }



    public synchronized void allowThreadDecoding(Thread t) {
        getOrCreateThreadStatus(t).mState = State.ALLOW;
    }




    public static synchronized iBitmapManager instance() {
        iBitmapManager iBitmapManager;
        synchronized (iBitmapManager.class) {
            if (sManager == null) {
                sManager = new iBitmapManager();
            }
            iBitmapManager = sManager;
        }
        return iBitmapManager;
    }


}
