package com.applockthree.icropimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.applockthree.ifab.iFloatingActionsMenu;

import java.util.ArrayList;
import java.util.Iterator;

public class iCropIImageView extends iImageViewTouchBase {
    private Context imContext;
    ArrayList<iHighlightView> mIHighlightViews = new ArrayList();
    float mLastX;
    float mLastY;
    int mMotionEdge;
    iHighlightView mMotionIHighlightView = null;

    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    public /* bridge */ /* synthetic */ boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    public /* bridge */ /* synthetic */ void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
    }

    public /* bridge */ /* synthetic */ void setImageBitmapResetBase(Bitmap bitmap, boolean z) {
        super.setImageBitmapResetBase(bitmap, z);
    }

    public /* bridge */ /* synthetic */ void setImageRotateBitmapResetBase(iRotateBitmap iRotateBitmap, boolean z) {
        super.setImageRotateBitmapResetBase(iRotateBitmap, z);
    }

    public /* bridge */ /* synthetic */ void setRecycler(Recycler recycler) {
        super.setRecycler(recycler);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (this.mBitmapDisplayed.getBitmap() != null) {
            Iterator it = this.mIHighlightViews.iterator();
            while (it.hasNext()) {
                iHighlightView hv = (iHighlightView) it.next();
                hv.mMatrix.set(getImageMatrix());
                hv.invalidate();
                if (hv.mIsFocused) {
                    centerBasedOnHighlightView(hv);
                }
            }
        }
    }

    public iCropIImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.imContext = context;
    }

    protected void zoomTo(float scale, float centerX, float centerY) {
        super.zoomTo(scale, centerX, centerY);
        Iterator it = this.mIHighlightViews.iterator();
        while (it.hasNext()) {
            iHighlightView hv = (iHighlightView) it.next();
            hv.mMatrix.set(getImageMatrix());
            hv.invalidate();
        }
    }

    protected void zoomIn() {
        super.zoomIn();
        Iterator it = this.mIHighlightViews.iterator();
        while (it.hasNext()) {
            iHighlightView hv = (iHighlightView) it.next();
            hv.mMatrix.set(getImageMatrix());
            hv.invalidate();
        }
    }

    protected void zoomOut() {
        super.zoomOut();
        Iterator it = this.mIHighlightViews.iterator();
        while (it.hasNext()) {
            iHighlightView hv = (iHighlightView) it.next();
            hv.mMatrix.set(getImageMatrix());
            hv.invalidate();
        }
    }

    protected void postTranslate(float deltaX, float deltaY) {
        super.postTranslate(deltaX, deltaY);
        for (int i = 0; i < this.mIHighlightViews.size(); i++) {
            iHighlightView hv = (iHighlightView) this.mIHighlightViews.get(i);
            hv.mMatrix.postTranslate(deltaX, deltaY);
            hv.invalidate();
        }
    }

    private void recomputeFocus(MotionEvent event) {
        int i;
        for (i = 0; i < this.mIHighlightViews.size(); i++) {
            iHighlightView hv = (iHighlightView) this.mIHighlightViews.get(i);
            hv.setFocus(false);
            hv.invalidate();
        }
        for (i = 0; i < this.mIHighlightViews.size(); i++) {
            iHighlightView   hv = (iHighlightView) this.mIHighlightViews.get(i);
            if (hv.getHit(event.getX(), event.getY()) != 1) {
                if (!hv.hasFocus()) {
                    hv.setFocus(true);
                    hv.invalidate();
                }
                invalidate();
            }
        }
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        iCropImage iCropImage = (com.applockthree.icropimage.iCropImage) imContext;
        if (iCropImage.mSaving) {
            return false;
        }
        int i;
        iHighlightView hv;
        switch (event.getAction()) {
            case iFloatingActionsMenu.LABELS_ON_LEFT_SIDE /*0*/:
                if (!iCropImage.mWaitingToPick) {
                    for (i = 0; i < this.mIHighlightViews.size(); i++) {
                        hv = (iHighlightView) this.mIHighlightViews.get(i);
                        int edge = hv.getHit(event.getX(), event.getY());
                        if (edge != 1) {
                            this.mMotionEdge = edge;
                            this.mMotionIHighlightView = hv;
                            this.mLastX = event.getX();
                            this.mLastY = event.getY();
                            this.mMotionIHighlightView.setMode(edge == 32 ? iHighlightView.ModifyMode.Move : iHighlightView.ModifyMode.Grow);
                            break;
                        }
                    }
                    break;
                }
                recomputeFocus(event);
                break;
            case 1 /*1*/:
                if (iCropImage.mWaitingToPick) {
                    for (i = 0; i < this.mIHighlightViews.size(); i++) {
                        hv = (iHighlightView) this.mIHighlightViews.get(i);
                        if (hv.hasFocus()) {
                            iCropImage.mCrop = hv;
                            for (int j = 0; j < this.mIHighlightViews.size(); j++) {
                                if (j != i) {
                                    mIHighlightViews.get(j).setHidden(true);
                                }
                            }
                            centerBasedOnHighlightView(hv);
                            ((iCropImage) this.imContext).mWaitingToPick = false;
                            return true;
                        }
                    }
                } else if (this.mMotionIHighlightView != null) {
                    centerBasedOnHighlightView(this.mMotionIHighlightView);
                    this.mMotionIHighlightView.setMode(iHighlightView.ModifyMode.None);
                }
                this.mMotionIHighlightView = null;
                break;
            case iFloatingActionsMenu.EXPAND_LEFT /*2*/:
                if (!iCropImage.mWaitingToPick) {
                    if (this.mMotionIHighlightView != null) {
                        this.mMotionIHighlightView.handleMotion(this.mMotionEdge, event.getX() - this.mLastX, event.getY() - this.mLastY);
                        this.mLastX = event.getX();
                        this.mLastY = event.getY();
                        ensureVisible(this.mMotionIHighlightView);
                        break;
                    }
                }
                recomputeFocus(event);
                break;
        }
        switch (event.getAction()) {
            case iFloatingActionsMenu.LABELS_ON_RIGHT_SIDE /*1*/:
                center(true, true);
                break;
            case iFloatingActionsMenu.EXPAND_LEFT /*2*/:
                if (getScale() == 1.0f) {
                    center(true, true);
                    break;
                }
                break;
        }
        return true;
    }

    private void ensureVisible(iHighlightView hv) {
        int panDeltaX;
        int panDeltaY;
        Rect r = hv.mDrawRect;
        int panDeltaX1 = Math.max(0, this.mLeft - r.left);
        int panDeltaX2 = Math.min(0, this.mRight - r.right);
        int panDeltaY1 = Math.max(0, this.mTop - r.top);
        int panDeltaY2 = Math.min(0, this.mBottom - r.bottom);
        if (panDeltaX1 != 0) {
            panDeltaX = panDeltaX1;
        } else {
            panDeltaX = panDeltaX2;
        }
        if (panDeltaY1 != 0) {
            panDeltaY = panDeltaY1;
        } else {
            panDeltaY = panDeltaY2;
        }
        if (panDeltaX != 0 || panDeltaY != 0) {
            panBy((float) panDeltaX, (float) panDeltaY);
        }
    }

    private void centerBasedOnHighlightView(iHighlightView hv) {
        Rect drawRect = hv.mDrawRect;
        float thisWidth = (float) getWidth();
        float thisHeight = (float) getHeight();
        float zoom = Math.max(1.0f, Math.min((thisWidth / ((float) drawRect.width())) * 0.6f, (thisHeight / ((float) drawRect.height())) * 0.6f) * getScale());
        if (((double) (Math.abs(zoom - getScale()) / zoom)) > 0.1d) {
            float[] coordinates = new float[]{hv.mCropRect.centerX(), hv.mCropRect.centerY()};
            getImageMatrix().mapPoints(coordinates);
            zoomTo(zoom, coordinates[0], coordinates[1], 300.0f);
        }
        ensureVisible(hv);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < this.mIHighlightViews.size(); i++) {
            ((iHighlightView) this.mIHighlightViews.get(i)).draw(canvas);
        }
    }

    public void add(iHighlightView hv) {
        this.mIHighlightViews.add(hv);
        invalidate();
    }
}
