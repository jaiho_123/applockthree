package com.applockthree.icropimage;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class iRotateBitmap {
    public static final String iTAG = "iRotateBitmap";
    private Bitmap imBitmap;
    private int imRotation;

    public iRotateBitmap(Bitmap bitmap) {
        this.imBitmap = bitmap;
        this.imRotation = 0;
    }

    public iRotateBitmap(Bitmap bitmap, int rotation) {
        this.imBitmap = bitmap;
        this.imRotation = rotation % 360;
    }

    public void setRotation(int rotation) {
        this.imRotation = rotation;
    }

    public int getRotation() {
        return this.imRotation;
    }

    public Bitmap getBitmap() {
        return this.imBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.imBitmap = bitmap;
    }

    public Matrix getRotateMatrix() {
        Matrix matrix = new Matrix();
        if (this.imRotation != 0) {
            matrix.preTranslate((float) (-(this.imBitmap.getWidth() / 2)), (float) (-(this.imBitmap.getHeight() / 2)));
            matrix.postRotate((float) this.imRotation);
            matrix.postTranslate((float) (getWidth() / 2), (float) (getHeight() / 2));
        }
        return matrix;
    }

    public boolean isOrientationChanged() {
        return (this.imRotation / 90) % 2 != 0;
    }

    public int getHeight() {
        if (isOrientationChanged()) {
            return this.imBitmap.getWidth();
        }
        return this.imBitmap.getHeight();
    }

    public int getWidth() {
        if (isOrientationChanged()) {
            return this.imBitmap.getHeight();
        }
        return this.imBitmap.getWidth();
    }

    public void recycle() {
        if (this.imBitmap != null) {
            this.imBitmap.recycle();
            this.imBitmap = null;
        }
    }
}
