package com.applockthree;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
public class MyService extends AccessibilityService {
    private static Thread vmThread;
    private Date date;
    private DataBase db;
    private SimpleDateFormat dd;
    private  SimpleDateFormat dm;
    private SimpleDateFormat dy;
    private Intent intent;
    private String opkg = BuildConfig.FLAVOR;
    private String pkg = BuildConfig.FLAVOR;
    private SimpleDateFormat th;
    private SimpleDateFormat tm;
    private SimpleDateFormat ts;

    private class vThread extends Thread {
        private vThread() {
        }
        public void run() {
            while (!isInterrupted()) {
                try {
                    Thread.sleep(100);
                    if (VERSION.SDK_INT >= 21) {
                        try {
                            UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(USAGE_STATS_SERVICE);
                            long time = System.currentTimeMillis();
                            List<UsageStats> stats = mUsageStatsManager.queryUsageStats(0, time - 10000, time);
                            if (stats != null) {
                                SortedMap<Long, UsageStats> mySortedMap = new TreeMap();
                                for (UsageStats usageStats : stats) {
                                    mySortedMap.put(Long.valueOf(usageStats.getLastTimeUsed()), usageStats);
                                }
                                if (!mySortedMap.isEmpty()) {
                                    pkg = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        RunningTaskInfo foregroundTaskInfo = ((ActivityManager) getBaseContext().getSystemService(ACTIVITY_SERVICE)).getRunningTasks(1).get(0);
                        pkg = foregroundTaskInfo.topActivity.getPackageName();
                    }
                    if (db.getcurrentapp().contains(pkg)) {
                        opkg = pkg;
                    } else {
                        date = new Date();
                        db.setlcapp(opkg, dd.format(date), dm.format(date), dy.format(date), th.format(date), tm.format(date), ts.format(date));
                        db.setloapp(pkg, dd.format(date), dm.format(date), dy.format(date), th.format(date), tm.format(date), ts.format(date));
                        db.insertcurrentapp(pkg);
                        if (pkg.contains("com.android.packageinstaller") || pkg.contains("com.google.android.packageinstaller")) {
                            db.setinstaller("true");
                        }
                        Log.e("PKG AYA OPEN-",pkg);
                        if (pkg.contains("launcher") || pkg.contains("com.google.android.googlequicksearchbox")) {
                            try {
                                intent.putExtra("type", "0");
                                startService(intent);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }

                        } else if (((db.getapplockbypackage(pkg).contains("true") && db.getisettingiapplock().contains("true")) || pkg.contentEquals(BuildConfig.APPLICATION_ID)) && db.getmasterpassword().length() > 0){
                            Log.e("PKG AYA OPEN DB WALA-",db.getapplockbypackage(pkg));
                            try {
                                if (pkg.contains(BuildConfig.APPLICATION_ID) && db.getisettinganswer().trim().length() <= 3 && db.getmasterpassword().toString().length() < 1) {
                                    Intent intent = new Intent(getBaseContext(), emailquestion.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else if (db.getfp().contains("false") && !pkg.contains(BuildConfig.APPLICATION_ID)) {
                                    intent.putExtra("type", "2");
                                    startService(intent);
                                }
                            } catch (Exception e4) {
                                e4.printStackTrace();
                            }
                        }
                    }
                } catch (InterruptedException e5) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        }
    }
 /*   @Override
    public IBinder onBind(Intent intent) {
        // Implement your logic here.
        return null;
    }*/
   public void onAccessibilityEvent(AccessibilityEvent event) {
    }

    public void onInterrupt() {
    }

    public void onServiceConnected() {
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = 64;
        info.notificationTimeout = 100;
        info.feedbackType = -1;
        setServiceInfo(info);
    }

    public void onCreate() {
        th = new SimpleDateFormat("kk");
       tm = new SimpleDateFormat("mm");
       ts = new SimpleDateFormat("ss a");
       dd = new SimpleDateFormat("dd");
       dm = new SimpleDateFormat("MM");
       dy = new SimpleDateFormat("yy");
       db = new DataBase(getBaseContext());
        vcall();
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        vcall();
        if (vmThread != null) {
            vmThread.interrupt();
        }
        vmThread = new vThread();
        vmThread.start();
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    public void vcall() {
        Log.e("LOCKY TYPE-",db.getlocktype());
        intent = new Intent("android.intent.action.MAIN");
        intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype()));
        intent.putExtra("type", "1");
        startService(intent);
    }

    public void onDestroy() {
        vmThread.interrupt();
        super.onDestroy();
    }
}
