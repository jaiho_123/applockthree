package com.applockthree;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applockthree.BuildConfig;
import com.applockthree.DataBase;
import com.applockthree.R;

public class firsttime extends AppCompatActivity implements OnClickListener {
    private static final String TAG = "Application";
    private  TextView[] button;
    private  int cnt = 0;
    private  int cnt1 = 0;
    private  DataBase db;
    private   ImageView[] dot;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private  String password;
    private   String password1;
    private  String string1;
    private  String string2;
    private  TextView txtname;
    private  Vibrator vb;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DataBase(this);
        button = new TextView[12];
        dot = new ImageView[5];
        password = BuildConfig.FLAVOR;
        password1 = BuildConfig.FLAVOR;
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        string1 = getResources().getString(R.string.Enter_new_Master_password);
        string2 = getResources().getString(R.string.Comfirm_Master_password);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        cnt1 = 1;
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.activity_firsttime);
        initLayouts();
    }

    private void initLayouts() {
        txtname = (TextView) findViewById(R.id.appname);
        dot[1] = (ImageView) findViewById(R.id.idot1);
        dot[2] = (ImageView) findViewById(R.id.idot2);
        dot[3] = (ImageView) findViewById(R.id.idot3);
        dot[4] = (ImageView) findViewById(R.id.idot4);
        button[0] = (TextView) findViewById(R.id.btn0);
        button[1] = (TextView) findViewById(R.id.btn1);
        button[2] = (TextView) findViewById(R.id.btn2);
        button[3] = (TextView) findViewById(R.id.btn3);
        button[4] = (TextView) findViewById(R.id.btn4);
        button[5] = (TextView) findViewById(R.id.btn5);
        button[6] = (TextView) findViewById(R.id.btn6);
        button[7] = (TextView) findViewById(R.id.btn7);
        button[8] = (TextView) findViewById(R.id.btn8);
        button[9] = (TextView) findViewById(R.id.btn9);
        button[10] = (TextView) findViewById(R.id.txtcancel);
        button[11] = (TextView) findViewById(R.id.btnback);
        button[0].setOnClickListener(this);
        button[1].setOnClickListener(this);
        button[2].setOnClickListener(this);
        button[3].setOnClickListener(this);
        button[4].setOnClickListener(this);
        button[5].setOnClickListener(this);
        button[6].setOnClickListener(this);
        button[7].setOnClickListener(this);
        button[8].setOnClickListener(this);
        button[9].setOnClickListener(this);
        button[10].setOnClickListener(this);
        button[11].setOnClickListener(this);
        txtname.setText(string1);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn0) {
            check(Integer.valueOf(0));
        }
        if (v.getId() == R.id.btn1) {
            check(Integer.valueOf(1));
        }
        if (v.getId() == R.id.btn2) {
            check(Integer.valueOf(2));
        }
        if (v.getId() == R.id.btn3) {
            check(Integer.valueOf(3));
        }
        if (v.getId() == R.id.btn4) {
            check(Integer.valueOf(4));
        }
        if (v.getId() == R.id.btn5) {
            check(Integer.valueOf(5));
        }
        if (v.getId() == R.id.btn6) {
            check(Integer.valueOf(6));
        }
        if (v.getId() == R.id.btn7) {
            check(Integer.valueOf(7));
        }
        if (v.getId() == R.id.btn8) {
            check(Integer.valueOf(8));
        }
        if (v.getId() == R.id.btn9) {
            check(Integer.valueOf(9));
        }
        if (v.getId() == R.id.txtcancel) {
            cnt = 0;
            password = BuildConfig.FLAVOR;
            setname();
            setdot();
        }
        if (v.getId() == R.id.btnback) {
            cnt = 0;
            password = BuildConfig.FLAVOR;
            password1 = BuildConfig.FLAVOR;
            cnt1 = 1;
            setdot();
            setname();
            button[11].setVisibility(View.GONE);
        }
    }

    public void check(Integer key) {
        if (db.getsound().contains("true")) {
            MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.click);
            mp.start();
            mp.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        if (db.getisettingvibrate().contains("true")) {
            vb.vibrate(50);
        }
        password += key;
        cnt++;
        setdot();
        if (cnt >= 4) {
            if (cnt1 < 2) {
                password1 = password;
                password = BuildConfig.FLAVOR;
                cnt1++;
                cnt = 0;
                setdot();
            } else if (!password.contains(password1)) {
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(200);
                }
                Log.println(7, "Password", "Password=" + password + " Password=" + password1);
                Toast.makeText(getBaseContext(), getResources().getString(R.string.password_not_match), 0).show();
                cnt = 0;
                password = BuildConfig.FLAVOR;
                setdot();
            } else if (db.getmasterpassword().trim().toString().length() < 2) {
                db.setmasterpassword(password1);
                finish();
            } else {
                db.setmasterpassword(password1);
                finish();
            }
        }
        if (cnt1 == 2) {
            button[11].setVisibility(0);
        } else {
            button[11].setVisibility(8);
        }
        setname();
    }

    public void setdot() {
        if (cnt == 0) {
            dot[1].setBackgroundResource(R.drawable.itheme1dot);
            dot[2].setBackgroundResource(R.drawable.itheme1dot);
            dot[3].setBackgroundResource(R.drawable.itheme1dot);
            dot[4].setBackgroundResource(R.drawable.itheme1dot);
        }
        if (cnt == 1) {
            dot[1].setBackgroundResource(R.drawable.itheme1doti);
        }
        if (cnt == 2) {
            dot[2].setBackgroundResource(R.drawable.itheme1doti);
        }
        if (cnt == 3) {
            dot[3].setBackgroundResource(R.drawable.itheme1doti);
        }
        if (cnt == 4) {
            dot[4].setBackgroundResource(R.drawable.itheme1doti);
        }
    }

    public void setname() {
        if (cnt1 == 1) {
            txtname.setText(string1);
        } else {
            txtname.setText(string2);
        }
    }
    @Override
    public void onBackPressed() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
        finish();
        super.onBackPressed();
    }
}
