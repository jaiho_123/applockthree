package com.applockthree;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PINLockScreen extends Service implements OnClickListener {
    private LinearLayout Pinlockll;
    private ImageView appicon;
    private TextView appname;
    private Button[] button;
    private Drawable cancel, d, delete, emergency;
    private int cnt = 0;
    private Date date;
    private DataBase db;
    private SimpleDateFormat dd, dm, dy;
    private ImageView[] dot;
    private Drawable[] dotid;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String password = BuildConfig.FLAVOR;
    private PackageManager pm;
    private SimpleDateFormat th, tm, ts;
    private int v = 0;
    private Vibrator vb;
    private LayoutParams wmlp;

    public void onCreate() {
        super.onCreate();
        th = new SimpleDateFormat("hh");
        tm = new SimpleDateFormat("mm");
        ts = new SimpleDateFormat("ss a");
        dd = new SimpleDateFormat("dd");
        dm = new SimpleDateFormat("MM");
        dy = new SimpleDateFormat("yy");
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        db = new DataBase(this);
        button = new Button[12];
        dot = new ImageView[5];
        dotid = new Drawable[5];
        pm = getBaseContext().getPackageManager();
        cancel = getResources().getDrawable(R.drawable.itheme1keycancel);
        delete = getResources().getDrawable(R.drawable.itheme1keydelete);
        emergency = getResources().getDrawable(R.drawable.fpassword);
    }

    private void initLayouts() {
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        wmlp = new LayoutParams(-1, -1, 2003, 256, -3);
        wmlp.gravity = 17;
        wmlp.alpha = 1.0f;
        wmlp.flags = 262144;
        LayoutParams layoutParams = wmlp;
        layoutParams.flags &= -2097297;
        wmlp.flags = 256;
        wmlp.format = -1;
        wmlp.token = null;
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        mLayout = new LinearLayout(this) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() != 4 || event.getAction() != 0) {
                    if (db.getfastunlockm1().contains("true")) {
                        if (event.getKeyCode() == 24 && event.getAction() == 0) {
                            v = 1;
                            return true;
                        } else if (event.getKeyCode() == 24 && event.getAction() == 1) {
                            v = 0;
                            return true;
                        }
                    }
                    if (db.getfastunlockm2().contains("true")) {
                        if (event.getKeyCode() == 25 && event.getAction() == 0) {
                            v = 1;
                            return true;
                        } else if (event.getKeyCode() == 25 && event.getAction() == 1) {
                            v = 0;
                            return true;
                        }
                    }
                    return super.dispatchKeyEvent(event);
                } else if (v == 1) {
                    v = 0;
                    removeLockScreen();
                    return true;
                } else {
                    cancelAndGoHome();
                    return true;
                }
            }
        };
        mLayout.setLayoutParams(new LayoutParams(-1, -1));
        View view = LayoutInflater.from(this).inflate(R.layout.pin_lock_screen, mLayout, true);
        Pinlockll = (LinearLayout) view.findViewById(R.id.locks);
        appicon = (ImageView) view.findViewById(R.id.appicon);
        appname = (TextView) view.findViewById(R.id.appname);
        button[0] = (Button) view.findViewById(R.id.btn0);
        button[1] = (Button) view.findViewById(R.id.btn1);
        button[2] = (Button) view.findViewById(R.id.btn2);
        button[3] = (Button) view.findViewById(R.id.btn3);
        button[4] = (Button) view.findViewById(R.id.btn4);
        button[5] = (Button) view.findViewById(R.id.btn5);
        button[6] = (Button) view.findViewById(R.id.btn6);
        button[7] = (Button) view.findViewById(R.id.btn7);
        button[8] = (Button) view.findViewById(R.id.btn8);
        button[9] = (Button) view.findViewById(R.id.btn9);
        button[10] = (Button) view.findViewById(R.id.txtcancel);
        button[11] = (Button) view.findViewById(R.id.btnfp);
        dot[1] = (ImageView) view.findViewById(R.id.idot1);
        dot[2] = (ImageView) view.findViewById(R.id.idot2);
        dot[3] = (ImageView) view.findViewById(R.id.idot3);
        dot[4] = (ImageView) view.findViewById(R.id.idot4);
        button[0].setOnClickListener(this);
        button[1].setOnClickListener(this);
        button[2].setOnClickListener(this);
        button[3].setOnClickListener(this);
        button[4].setOnClickListener(this);
        button[5].setOnClickListener(this);
        button[6].setOnClickListener(this);
        button[7].setOnClickListener(this);
        button[8].setOnClickListener(this);
        button[9].setOnClickListener(this);
        button[10].setOnClickListener(this);
        button[11].setOnClickListener(this);
        Cursor cursor = this.db.getitheme();
        cursor.moveToNext();
        String itheme = cursor.getString(0);
        String ithemepkg = cursor.getString(1);
        String ithemebg = cursor.getString(2);
        cursor.close();
      //  Addmob(view);
        try {
            Resources resources1;
            Resources resources = this.pm.getResourcesForApplication(ithemepkg);
            int idm0 = resources.getIdentifier(itheme + "key0d", "drawable", ithemepkg);
            int idm1 = resources.getIdentifier(itheme + "key1d", "drawable", ithemepkg);
            int idm2 = resources.getIdentifier(itheme + "key2d", "drawable", ithemepkg);
            int idm3 = resources.getIdentifier(itheme + "key3d", "drawable", ithemepkg);
            int idm4 = resources.getIdentifier(itheme + "key4d", "drawable", ithemepkg);
            int idm5 = resources.getIdentifier(itheme + "key5d", "drawable", ithemepkg);
            int idm6 = resources.getIdentifier(itheme + "key6d", "drawable", ithemepkg);
            int idm7 = resources.getIdentifier(itheme + "key7d", "drawable", ithemepkg);
            int idm8 = resources.getIdentifier(itheme + "key8d", "drawable", ithemepkg);
            int idm9 = resources.getIdentifier(itheme + "key9d", "drawable", ithemepkg);
            dotid[0] = resources.getDrawable(resources.getIdentifier(itheme + "dot", "drawable", ithemepkg));
            dotid[1] = resources.getDrawable(resources.getIdentifier(itheme + "doti", "drawable", ithemepkg));
            delete = resources.getDrawable(resources.getIdentifier(itheme + "keydelete", "drawable", ithemepkg));
            cancel = resources.getDrawable(resources.getIdentifier(itheme + "keycancel", "drawable", ithemepkg));
            emergency = resources.getDrawable(resources.getIdentifier("fpassword", "drawable", ithemepkg));
            button[0].setBackground(resources.getDrawable(idm0));
            button[1].setBackground(resources.getDrawable(idm1));
            button[2].setBackground(resources.getDrawable(idm2));
            button[3].setBackground(resources.getDrawable(idm3));
            button[4].setBackground(resources.getDrawable(idm4));
            button[5].setBackground(resources.getDrawable(idm5));
            button[6].setBackground(resources.getDrawable(idm6));
            button[7].setBackground(resources.getDrawable(idm7));
            button[8].setBackground(resources.getDrawable(idm8));
            button[9].setBackground(resources.getDrawable(idm9));
            button[10].setBackground(cancel);
            button[11].setBackground(emergency);
            Cursor cursor1 = db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 = db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 = pm.getResourcesForApplication(cursor1.getString(1));
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                    Pinlockll.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e2) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
        } catch (NameNotFoundException e3) {
            e3.printStackTrace();
        }
        dot[1].setBackground(dotid[0]);
        dot[2].setBackground(dotid[0]);
        dot[3].setBackground(dotid[0]);
        dot[4].setBackground(dotid[0]);
    }
/*    public void Addmob(View view) {
        Log.e("4SqureLckScrenChangPaswor","Act");
        AdView adView = (AdView) view.findViewById(R.id.adView_pinlockscreen);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }*/
    private void vcheck() {
        try {
            Cursor cursor1 = db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
                db.deletebackground(cursor1.getString(1));
                Resources resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                Pinlockll.setBackgroundDrawable(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        vcheck();
        String str = BuildConfig.FLAVOR;
        try {
            str = intent.getStringExtra("type");
        } catch (Exception e) {
        }
        if ((str == BuildConfig.FLAVOR || str == null) && mLayout == null) {
            initLayouts();
        }
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (str.contains("1") && mLayout == null) {
            initLayouts();
        }
        if (str.contains("3")) {
            try {
                initLayouts();
            } catch (Exception e2) {
            }
        }
        if (str.contains("2")) {
            if (mLayout == null) {
                initLayouts();
            }
            cnt = 0;
            password ="";
            try {
                appname.setText(pm.getApplicationLabel(pm.getApplicationInfo(db.getcurrentapp(), PackageManager.GET_META_DATA)).toString());
                appicon.setImageDrawable(pm.getApplicationIcon(db.getcurrentapp()));
            } catch (NameNotFoundException e3) {
                e3.printStackTrace();
            }
            setdot();
            try {
                mWindowManager.addView(mLayout, wmlp);
                mWindowManager.addView(mLayout, wmlp);
            } catch (Exception e4) {
            }
        }
        if (str.contains("0")) {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    try {
                        removeLockScreen();
                    } catch (Exception e) {
                    }
                }
            }, 100);
        }
        return START_STICKY;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn0) {
            check(Integer.valueOf(0));
        }
        if (v.getId() == R.id.btn1) {
            check(Integer.valueOf(1));
        }
        if (v.getId() == R.id.btn2) {
            check(Integer.valueOf(2));
        }
        if (v.getId() == R.id.btn3) {
            check(Integer.valueOf(3));
        }
        if (v.getId() == R.id.btn4) {
            check(Integer.valueOf(4));
        }
        if (v.getId() == R.id.btn5) {
            check(Integer.valueOf(5));
        }
        if (v.getId() == R.id.btn6) {
            check(Integer.valueOf(6));
        }
        if (v.getId() == R.id.btn7) {
            check(Integer.valueOf(7));
        }
        if (v.getId() == R.id.btn8) {
            check(Integer.valueOf(8));
        }
        if (v.getId() == R.id.btn9) {
            check(Integer.valueOf(9));
        }
        if (v.getId() == R.id.txtcancel) {
            if (cnt != 0) {
                password = password.substring(0, password.length() - 1);
                cnt--;
                setdot();
            } else {
                Intent startHome = new Intent("android.intent.action.MAIN");
                startHome.addCategory("android.intent.category.HOME");
                startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startHome);
            }
        }
        if (v.getId() == R.id.btnfp) {
            PopupMenu popup = new PopupMenu(this, v);
            popup.getMenuInflater().inflate(R.menu.actions, popup.getMenu());
            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    cnt = 0;
                    password = BuildConfig.FLAVOR;
                    setdot();
                    db.setfp("true");
                    Intent startHome = new Intent(getBaseContext(), Forgotpassword.class);
                    startHome.addCategory("android.intent.category.HOME");
                    startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startHome);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            removeLockScreen();
                        }
                    }, 150);
                    return false;
                }
            });
            popup.show();
        }
    }

    public void check(Integer key) {
        if (db.getsound().contains("true")) {
            MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.click);
            mp.start();
            mp.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    Log.e("mediapalyer", "finish");
                    mp.release();
                }
            });
        }
        if (db.getisettingvibrate().contains("true")) {
            vb.vibrate(50);
        }
        password += key;
        cnt++;
        setdot();
        if (cnt < 4) {
            return;
        }
        if (db.matchpassword(password)) {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    removeLockScreen();
                    cnt = 0;
                    password = BuildConfig.FLAVOR;
                    setdot();
                }
            }, 100);
        } else {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (db.getisettingvibrate().contains("true")) {
                        vb.vibrate(200);
                    }
                    date = new Date();
                    String d ;
                    String t ;
                    String an = BuildConfig.FLAVOR;
                    d = dd.format(date) + "/" + dm.format(date) + "/" + dy.format(date);
                    t = th.format(date) + ":" + tm.format(date) + ":" + ts.format(date);
                    try {
                        an = pm.getApplicationLabel(pm.getApplicationInfo(db.getcurrentapp(), PackageManager.GET_META_DATA)).toString();
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    db.insertapphacklog(db.getcurrentapp(), an, password, d, t);
                    cnt = 0;
                    password = BuildConfig.FLAVOR;
                    setdot();
                }
            }, 100);
        }
    }

    public void setdot() {
        if (cnt == 0) {
            dot[1].setBackground(dotid[0]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
            button[10].setBackground(cancel);
        }
        if (cnt == 1) {
            dot[1].setBackground(dotid[1]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
            button[10].setBackground(delete);
        }
        if (cnt == 2) {
            dot[2].setBackground(dotid[1]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
            button[10].setBackground(delete);
        }
        if (cnt == 3) {
            dot[3].setBackground(dotid[1]);
            dot[4].setBackground(dotid[0]);
            button[10].setBackground(delete);
        }
        if (cnt == 4) {
            dot[4].setBackground(dotid[1]);
            button[10].setBackground(delete);
        }
    }

    public void onDestroy() {
        removeLockScreen();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void cancelAndGoHome() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            public void run() {
                removeLockScreen();
            }
        }, 100);
    }

    private void removeLockScreen() {
        try {
            mWindowManager.removeView(mLayout);
        } catch (Exception e) {
        }
        try {
            appicon.setImageResource(R.drawable.blank);
        } catch (Exception e2) {
        }
    }
}
