package com.applockthree;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class CustomGridAdapteritheme extends BaseAdapter {
    private String citheme;
    private String cithemepkg;
    private Context context;
    private DataBase db;
    private Intent intent;
    private final String[] itheme;
    private final String[] ithemebg;
    private final String[] ithemepkg;
    private  PackageManager pm;
    private  Resources resources;
    private  View view;

    public CustomGridAdapteritheme(Context context, String[] itheme, String[] ithemepkg, String[] ithemebg, String citheme, String cithemepkg, View view) {
        this.context = context;
        this.itheme = itheme;
        this.ithemepkg = ithemepkg;
        this.citheme = citheme;
        this.cithemepkg = cithemepkg;
        this.pm = context.getPackageManager();
        this.ithemebg = ithemebg;
        db = new DataBase(context);
        this.view = view;
    }

    public int getCount() {
        return itheme.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View gridView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.grid_item_itheme, null);
        RelativeLayout rl1 = (RelativeLayout) gridView.findViewById(R.id.ithemerl);
        ImageView imageView1 = (ImageView) gridView.findViewById(R.id.itheme);
        ImageView imageView = (ImageView) gridView.findViewById(R.id.ithemcheck);
        try {
            resources = pm.getResourcesForApplication(ithemepkg[position]);
            imageView1.setBackground(resources.getDrawable(resources.getIdentifier(itheme[position], "drawable", ithemepkg[position])));
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        if (itheme[position].trim().contentEquals(citheme) && ithemepkg[position].trim().contentEquals(cithemepkg)) {
            imageView.setBackgroundResource(R.drawable.check);
        }
        rl1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (!itheme[position].trim().contentEquals(citheme) || !ithemepkg[position].trim().contentEquals(cithemepkg)) {
                    db.setitheme(itheme[position],ithemepkg[position], ithemebg[position]);
                    citheme =itheme[position];
                    cithemepkg = ithemepkg[position];
                    intent = new Intent("android.intent.action.MAIN");
                    intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype()));
                    intent.putExtra("type", "3");
                    context.startService(intent);
                    reset();
                }
            }
        });
        return gridView;
    }

    private void reset() {
        ((GridView) view.findViewById(R.id.gridViewitheme)).setAdapter(new CustomGridAdapteritheme(context, itheme, ithemepkg, ithemebg, citheme, cithemepkg, view));
    }
}
