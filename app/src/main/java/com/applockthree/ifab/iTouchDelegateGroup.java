package com.applockthree.ifab;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import java.util.ArrayList;

public class iTouchDelegateGroup extends TouchDelegate {
    private static final Rect iUSELESS_HACKY_RECT = new Rect();
    private TouchDelegate imCurrentTouchDelegate;
    private boolean imEnabled;
    private final ArrayList<TouchDelegate> imTouchDelegates = new ArrayList();

    public iTouchDelegateGroup(View uselessHackyView) {
        super(iUSELESS_HACKY_RECT, uselessHackyView);
    }

    public void addTouchDelegate(@NonNull TouchDelegate touchDelegate) {
        this.imTouchDelegates.add(touchDelegate);
    }

    public void removeTouchDelegate(TouchDelegate touchDelegate) {
        this.imTouchDelegates.remove(touchDelegate);
        if (this.imCurrentTouchDelegate == touchDelegate) {
            this.imCurrentTouchDelegate = null;
        }
    }

    public void clearTouchDelegates() {
        this.imTouchDelegates.clear();
        this.imCurrentTouchDelegate = null;
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (!this.imEnabled) {
            return false;
        }
        TouchDelegate delegate = null;
        switch (event.getAction()) {
            case iFloatingActionsMenu.LABELS_ON_LEFT_SIDE /*0*/:
                for (int i = 0; i < this.imTouchDelegates.size(); i++) {
                    TouchDelegate touchDelegate = (TouchDelegate) this.imTouchDelegates.get(i);
                    if (touchDelegate.onTouchEvent(event)) {
                        this.imCurrentTouchDelegate = touchDelegate;
                        return true;
                    }
                }
                break;
            case iFloatingActionsMenu.LABELS_ON_RIGHT_SIDE /*1*/:
            case iFloatingActionsMenu.EXPAND_RIGHT /*3*/:
                delegate = this.imCurrentTouchDelegate;
                this.imCurrentTouchDelegate = null;
                break;
            case iFloatingActionsMenu.EXPAND_LEFT /*2*/:
                delegate = this.imCurrentTouchDelegate;
                break;
        }
        if (delegate == null || !delegate.onTouchEvent(event)) {
            return false;
        }
        return true;
    }

    public void setEnabled(boolean enabled) {
        this.imEnabled = enabled;
    }
}
