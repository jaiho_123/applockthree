package com.applockthree.ifab;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build.VERSION;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

import com.applockthree.R;

public class iFloatingActionButton extends AppCompatImageButton {
    public static final int SIZE_MINI = 1;
    private float imCircleSize;
    private int imDrawableSize;
    @DrawableRes
    private int imIcon;
    private Drawable imIconDrawable;
    private float imShadowOffset;
    private float imShadowRadius;
    private int imSize;
    private int mColorDisabled;
    public int mColorNormal;
    public int mColorPressed;
    public boolean mStrokeVisible;
    private String mTitle;

    private static class TranslucentLayerDrawable extends LayerDrawable {
        private final int mAlpha;

        public TranslucentLayerDrawable(int alpha, Drawable... layers) {
            super(layers);
            this.mAlpha = alpha;
        }

        public void draw(Canvas canvas) {
            Rect bounds = getBounds();
            canvas.saveLayerAlpha((float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom, this.mAlpha, Canvas.ALL_SAVE_FLAG);
            super.draw(canvas);
            canvas.restore();
        }
    }

    public iFloatingActionButton(Context context) {
        this(context, null);
    }

    public iFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public iFloatingActionButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    void init(Context context, AttributeSet attributeSet) {
        TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.ActionBar, 0, 0);
        this.mColorNormal = 16776961;
        this.mColorPressed = 16711681;
        this.mColorDisabled = 12303292;
        this.imSize = 10;
        this.imIcon = R.drawable.allapp;
        this.mTitle = "jaiho";
        this.mStrokeVisible = true;
        attr.recycle();
        updateCircleSize();
        this.imShadowRadius = getDimension(R.dimen.fab_shadow_radius);
        this.imShadowOffset = getDimension(R.dimen.fab_shadow_offset);
        updateDrawableSize();
        updateBackground();
    }

    private void updateDrawableSize() {
        this.imDrawableSize = (int) (this.imCircleSize + (2.0f * this.imShadowRadius));
    }

    private void updateCircleSize() {
        this.imCircleSize = getDimension(this.imSize == 0 ? R.dimen.fab_size_normal : R.dimen.fab_size_mini);
    }

    public void setSize(int size) {
        if (size != SIZE_MINI && size != 0) {
            throw new IllegalArgumentException("Use @FAB_SIZE constants only!");
        } else if (this.imSize != size) {
            this.imSize = size;
            updateCircleSize();
            updateDrawableSize();
            updateBackground();
        }
    }


    public void setIcon(@DrawableRes int icon) {
        if (imIcon != icon) {
            this.imIcon = icon;
            imIconDrawable = null;
            updateBackground();
        }
    }


    int getColor(@ColorRes int id) {
        return getResources().getColor(id);
    }

    float getDimension(@DimenRes int id) {
        return getResources().getDimension(id);
    }


    public String getTitle() {
        return this.mTitle;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(this.imDrawableSize, this.imDrawableSize);
    }

    void updateBackground() {
        float strokeWidth = getDimension(R.dimen.fab_stroke_width);
        float halfStrokeWidth = strokeWidth / 2.0f;
        Drawable[] drawableArr = new Drawable[4];
        drawableArr[0] = getResources().getDrawable(imSize == 0 ? R.drawable.fab_bg_normal : R.drawable.fab_bg_mini);
        drawableArr[SIZE_MINI] = createFillDrawable(strokeWidth);
        drawableArr[2] = createOuterStrokeDrawable(strokeWidth);
        drawableArr[3] = getIconDrawable();
        LayerDrawable layerDrawable = new LayerDrawable(drawableArr);
        int iconOffset = ((int) (this.imCircleSize - getDimension(R.dimen.fab_icon_size))) / 2;
        int circleInsetHorizontal = (int) imShadowRadius;
        int circleInsetTop = (int) (imShadowRadius - imShadowOffset);
        int circleInsetBottom = (int) (imShadowRadius + imShadowOffset);
        layerDrawable.setLayerInset(SIZE_MINI, circleInsetHorizontal, circleInsetTop, circleInsetHorizontal, circleInsetBottom);
        layerDrawable.setLayerInset(2, (int) (((float) circleInsetHorizontal) - halfStrokeWidth), (int) (((float) circleInsetTop) - halfStrokeWidth), (int) (((float) circleInsetHorizontal) - halfStrokeWidth), (int) (((float) circleInsetBottom) - halfStrokeWidth));
        layerDrawable.setLayerInset(3, circleInsetHorizontal + iconOffset, circleInsetTop + iconOffset, circleInsetHorizontal + iconOffset, circleInsetBottom + iconOffset);
        setBackgroundCompat(layerDrawable);
    }

    Drawable getIconDrawable() {
        if (imIconDrawable != null) {
            return imIconDrawable;
        }
        if (this.imIcon != 0) {
            return getResources().getDrawable(imIcon);
        }
        return new ColorDrawable(0);
    }

    private StateListDrawable createFillDrawable(float strokeWidth) {
        StateListDrawable drawable = new StateListDrawable();
        int[] iArr = new int[SIZE_MINI];
        iArr[0] = -16842910;
        drawable.addState(iArr, createCircleDrawable(mColorDisabled, strokeWidth));
        iArr = new int[SIZE_MINI];
        iArr[0] = 16842919;
        drawable.addState(iArr, createCircleDrawable(mColorPressed, strokeWidth));
        drawable.addState(new int[0], createCircleDrawable(mColorNormal, strokeWidth));
        return drawable;
    }

    private Drawable createCircleDrawable(int color, float strokeWidth) {
        int alpha = Color.alpha(color);
        int opaqueColor = opaque(color);
        Paint paint = new ShapeDrawable(new OvalShape()).getPaint();
        paint.setAntiAlias(true);
        paint.setColor(opaqueColor);
        Drawable[] layers = new Drawable[]{createInnerStrokesDrawable(opaqueColor, strokeWidth)};
        LayerDrawable drawable = (alpha == MotionEventCompat.ACTION_MASK || !mStrokeVisible) ? new LayerDrawable(layers) : new TranslucentLayerDrawable(alpha, layers);
        int halfStrokeWidth = (int) (strokeWidth / 2.0f);
        drawable.setLayerInset(SIZE_MINI, halfStrokeWidth, halfStrokeWidth, halfStrokeWidth, halfStrokeWidth);
        return drawable;
    }

    private Drawable createOuterStrokeDrawable(float strokeWidth) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        Paint paint = shapeDrawable.getPaint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(Style.STROKE);
        paint.setColor(ViewCompat.MEASURED_STATE_MASK);
        paint.setAlpha(opacityToAlpha(0.02f));
        return shapeDrawable;
    }

    private int opacityToAlpha(float opacity) {
        return (int) (255.0f * opacity);
    }

    private int darkenColor(int argb) {
        return adjustColorBrightness(argb, 0.9f);
    }

    private int lightenColor(int argb) {
        return adjustColorBrightness(argb, 1.1f);
    }

    private int adjustColorBrightness(int argb, float factor) {
        float[] hsv = new float[3];
        Color.colorToHSV(argb, hsv);
        hsv[2] = Math.min(hsv[2] * factor, 1.0f);
        return Color.HSVToColor(Color.alpha(argb), hsv);
    }

    private int halfTransparent(int argb) {
        return Color.argb(Color.alpha(argb) / 2, Color.red(argb), Color.green(argb), Color.blue(argb));
    }

    private int opaque(int argb) {
        return Color.rgb(Color.red(argb), Color.green(argb), Color.blue(argb));
    }

    public Drawable createInnerStrokesDrawable(int color, float strokeWidth) {
        if (!this.mStrokeVisible) {
            return new ColorDrawable(0);
        }
        Drawable shapeDrawable = new ShapeDrawable(new OvalShape());
        int bottomStrokeColor = darkenColor(color);
        int bottomStrokeColorHalfTransparent = halfTransparent(bottomStrokeColor);
        int topStrokeColor = lightenColor(color);
        int topStrokeColorHalfTransparent = halfTransparent(topStrokeColor);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(Style.STROKE);
        // shapeDrawable.setShaderFactory(new C06361(topStrokeColor, topStrokeColorHalfTransparent, color, bottomStrokeColorHalfTransparent, bottomStrokeColor));
        return shapeDrawable;
    }

    @SuppressLint({"NewApi"})
    private void setBackgroundCompat(Drawable drawable) {
        if (VERSION.SDK_INT >= 16) {
            setBackground(drawable);
        } else {
            setBackgroundDrawable(drawable);
        }
    }

    public void setVisibility(int visibility) {

        super.setVisibility(visibility);
    }
}
