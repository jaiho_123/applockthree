package com.applockthree.ifab;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;

import com.applockthree.R;

public class iAddIFloatingActionButton extends iFloatingActionButton {
    int mPlusColor;

    public iAddIFloatingActionButton(Context context) {
        this(context, null);
    }

    public iAddIFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public iAddIFloatingActionButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    void init(Context context, AttributeSet attributeSet) {
        TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.ActionBar, 0, 0);
        this.mPlusColor = attr.getColor(0, Color.parseColor("#bbee00"));
        attr.recycle();
        super.init(context, attributeSet);
    }

    public int getPlusColor() {
        return this.mPlusColor;
    }

    public void setPlusColorResId(@ColorRes int plusColor) {
        setPlusColor(getColor(plusColor));
    }

    public void setPlusColor(int color) {
        if (this.mPlusColor != color) {
            this.mPlusColor = color;
            updateBackground();
        }
    }

    public void setIcon(@DrawableRes int icon) {
        throw new UnsupportedOperationException("Use FloatingActionButton if you want to use custom icon");
    }

    Drawable getIconDrawable() {
        final float iconSize = getDimension(R.dimen.fab_icon_size);
        final float iconHalfSize = iconSize / 2.0f;
        final float plusHalfStroke = getDimension(R.dimen.fab_plus_icon_stroke) / 2.0f;
        final float plusOffset = (iconSize - getDimension(R.dimen.fab_plus_icon_size)) / 2.0f;
        ShapeDrawable drawable = new ShapeDrawable(new Shape() {
            public void draw(Canvas canvas, Paint paint) {
                canvas.drawRect(plusOffset, iconHalfSize - plusHalfStroke, iconSize - plusOffset, plusHalfStroke + iconHalfSize, paint);
                canvas.drawRect(iconHalfSize - plusHalfStroke, plusOffset, plusHalfStroke + iconHalfSize, iconSize - plusOffset, paint);
            }
        });
        Paint paint = drawable.getPaint();
        paint.setColor(this.mPlusColor);
        paint.setStyle(Style.FILL);
        paint.setAntiAlias(true);
        return drawable;
    }
}
