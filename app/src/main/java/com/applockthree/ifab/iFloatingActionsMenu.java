package com.applockthree.ifab;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import com.applockthree.R;


public class iFloatingActionsMenu extends ViewGroup {
    private static final int ANIMATION_DURATION = 300;
    private static final float COLLAPSED_PLUS_ROTATION = 0.0f;
    private static final float EXPANDED_PLUS_ROTATION = 135.0f;
    public static final int EXPAND_DOWN = 1;
    public static final int EXPAND_LEFT = 2;
    public static final int EXPAND_RIGHT = 3;
    public static final int EXPAND_UP = 0;
    public static final int LABELS_ON_LEFT_SIDE = 0;
    public static final int LABELS_ON_RIGHT_SIDE = 1;
    private static Interpolator sAlphaExpandInterpolator = new DecelerateInterpolator();
    private static Interpolator sCollapseInterpolator = new DecelerateInterpolator(3.0f);
    private static Interpolator sExpandInterpolator = new OvershootInterpolator();
    private iAddIFloatingActionButton imAddButton;
    private int imAddButtonColorNormal;
    private int imAddButtonColorPressed;
    private int imAddButtonPlusColor;
    private int imAddButtonSize;
    private boolean imAddButtonStrokeVisible;
    private int imButtonSpacing;
    private int imButtonsCount;
    private AnimatorSet imCollapseAnimation;
    private AnimatorSet imExpandAnimation;
    private int imExpandDirection;
    private boolean imExpanded;
    private iTouchDelegateGroup imITouchDelegateGroup;
    private int imLabelsMargin;
    private int imLabelsPosition;
    private int imLabelsStyle;
    private int imLabelsVerticalOffset;
    private int imMaxButtonHeight;
    private int imMaxButtonWidth;
    private RotatingDrawable imRotatingDrawable;
    private OnFloatingActionsMenuUpdateListener mListener;

    public interface OnFloatingActionsMenuUpdateListener {
        void onMenuCollapsed();

        void onMenuExpanded();
    }

    private class LayoutParams extends ViewGroup.LayoutParams {
        private boolean animationsSetToPlay;
        private ObjectAnimator mCollapseAlpha = new ObjectAnimator();
        private ObjectAnimator mCollapseDir = new ObjectAnimator();
        private ObjectAnimator mExpandAlpha = new ObjectAnimator();
        private ObjectAnimator mExpandDir = new ObjectAnimator();

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
            this.mExpandDir.setInterpolator(iFloatingActionsMenu.sExpandInterpolator);
            this.mExpandAlpha.setInterpolator(iFloatingActionsMenu.sAlphaExpandInterpolator);
            this.mCollapseDir.setInterpolator(iFloatingActionsMenu.sCollapseInterpolator);
            this.mCollapseAlpha.setInterpolator(iFloatingActionsMenu.sCollapseInterpolator);
            this.mCollapseAlpha.setProperty(View.ALPHA);
            this.mCollapseAlpha.setFloatValues(new float[]{1.0f, iFloatingActionsMenu.COLLAPSED_PLUS_ROTATION});
            this.mExpandAlpha.setProperty(View.ALPHA);
            this.mExpandAlpha.setFloatValues(new float[]{iFloatingActionsMenu.COLLAPSED_PLUS_ROTATION, 1.0f});
            switch (iFloatingActionsMenu.this.imExpandDirection) {
                case iFloatingActionsMenu.LABELS_ON_LEFT_SIDE /*0*/:
                case iFloatingActionsMenu.LABELS_ON_RIGHT_SIDE /*1*/:
                    this.mCollapseDir.setProperty(View.TRANSLATION_Y);
                    this.mExpandDir.setProperty(View.TRANSLATION_Y);
                    return;
                case iFloatingActionsMenu.EXPAND_LEFT /*2*/:
                case iFloatingActionsMenu.EXPAND_RIGHT /*3*/:
                    this.mCollapseDir.setProperty(View.TRANSLATION_X);
                    this.mExpandDir.setProperty(View.TRANSLATION_X);
                    return;
                default:
                    return;
            }
        }

        public void setAnimationsTarget(View view) {
            this.mCollapseAlpha.setTarget(view);
            this.mCollapseDir.setTarget(view);
            this.mExpandAlpha.setTarget(view);
            this.mExpandDir.setTarget(view);
            if (!this.animationsSetToPlay) {
                addLayerTypeListener(this.mExpandDir, view);
                addLayerTypeListener(this.mCollapseDir, view);
                iFloatingActionsMenu.this.imCollapseAnimation.play(this.mCollapseAlpha);
                iFloatingActionsMenu.this.imCollapseAnimation.play(this.mCollapseDir);
                iFloatingActionsMenu.this.imExpandAnimation.play(this.mExpandAlpha);
                iFloatingActionsMenu.this.imExpandAnimation.play(this.mExpandDir);
                this.animationsSetToPlay = true;
            }
        }

        private void addLayerTypeListener(Animator animator, final View view) {
            animator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    view.setLayerType(iFloatingActionsMenu.LABELS_ON_LEFT_SIDE, null);
                }

                public void onAnimationStart(Animator animation) {
                    view.setLayerType(iFloatingActionsMenu.EXPAND_LEFT, null);
                }
            });
        }
    }

    private static class RotatingDrawable extends LayerDrawable {
        private float mRotation;
        Drawable[] drawableArr;

        public RotatingDrawable( Drawable[] layers) {

            super(layers);
            drawableArr = new Drawable[iFloatingActionsMenu.LABELS_ON_RIGHT_SIDE];
            drawableArr[iFloatingActionsMenu.LABELS_ON_LEFT_SIDE] = layers[0];
        }

        public float getRotation() {
            return this.mRotation;
        }

        public void setRotation(float rotation) {
            this.mRotation = rotation;
            invalidateSelf();
        }

        public void draw(Canvas canvas) {
            canvas.save();
            canvas.rotate(this.mRotation, (float) getBounds().centerX(), (float) getBounds().centerY());
            super.draw(canvas);
            canvas.restore();
        }
    }

    public static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        public boolean imExpanded;

        public SavedState(Parcelable parcel) {
            super(parcel);
        }

        private SavedState(Parcel in) {
            super(in);
            boolean z = true;

            if (in.readInt() != iFloatingActionsMenu.LABELS_ON_RIGHT_SIDE) {
                z = false;
            }
            this.imExpanded = z;
        }

        public void writeToParcel(@NonNull Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.imExpanded ? iFloatingActionsMenu.LABELS_ON_RIGHT_SIDE : iFloatingActionsMenu.LABELS_ON_LEFT_SIDE);
        }
    }

    public iFloatingActionsMenu(Context context) {
        this(context, null);
    }

    public iFloatingActionsMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.imExpandAnimation = new AnimatorSet().setDuration(300);
        this.imCollapseAnimation = new AnimatorSet().setDuration(300);
        init(context, attrs);
    }

    public iFloatingActionsMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.imExpandAnimation = new AnimatorSet().setDuration(300);
        this.imCollapseAnimation = new AnimatorSet().setDuration(300);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {
        this.imButtonSpacing = (int) ((getResources().getDimension(R.dimen.fab_actions_spacing) - getResources().getDimension(R.dimen.fab_shadow_radius)) - getResources().getDimension(R.dimen.fab_shadow_offset));
        this.imLabelsMargin = getResources().getDimensionPixelSize(R.dimen.fab_labels_margin);
        this.imLabelsVerticalOffset = getResources().getDimensionPixelSize(R.dimen.fab_shadow_offset);
        this.imITouchDelegateGroup = new iTouchDelegateGroup(this);
        setTouchDelegate(this.imITouchDelegateGroup);
        TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.ActionBar, LABELS_ON_LEFT_SIDE, LABELS_ON_LEFT_SIDE);
     //   this.imAddButtonPlusColor = attr.getColor(EXPAND_RIGHT, getColor(17170443));
       // this.imAddButtonColorNormal = attr.getColor(LABELS_ON_RIGHT_SIDE, getColor(17170451));
       // this.imAddButtonColorPressed = attr.getColor(LABELS_ON_LEFT_SIDE, getColor(17170450));
        this.imAddButtonSize = attr.getInt(LABELS_ON_LEFT_SIDE, LABELS_ON_LEFT_SIDE);
        this.imAddButtonStrokeVisible = attr.getBoolean(LABELS_ON_LEFT_SIDE, true);
        this.imExpandDirection = attr.getInt(EXPAND_UP, LABELS_ON_LEFT_SIDE);
        this.imLabelsStyle = attr.getResourceId(LABELS_ON_LEFT_SIDE, EXPAND_RIGHT);
        this.imLabelsPosition = attr.getInt(LABELS_ON_LEFT_SIDE, LABELS_ON_LEFT_SIDE);
        attr.recycle();
        if (this.imLabelsStyle == 0 || !expandsHorizontally()) {
            createAddButton(context);
            return;
        }
        throw new IllegalStateException("Action labels in horizontal expand orientation is not supported.");
    }

    public void setOnFloatingActionsMenuUpdateListener(OnFloatingActionsMenuUpdateListener listener) {
        this.mListener = listener;
    }

    private boolean expandsHorizontally() {
        return this.imExpandDirection == EXPAND_LEFT || this.imExpandDirection == EXPAND_RIGHT;
    }

    private void createAddButton(Context context) {
        this.imAddButton = new iAddIFloatingActionButton(context) {
            void updateBackground() {
                this.mPlusColor = iFloatingActionsMenu.this.imAddButtonPlusColor;
                this.mColorNormal = iFloatingActionsMenu.this.imAddButtonColorNormal;
                this.mColorPressed = iFloatingActionsMenu.this.imAddButtonColorPressed;
                this.mStrokeVisible = iFloatingActionsMenu.this.imAddButtonStrokeVisible;
                super.updateBackground();
            }

            Drawable getIconDrawable() {
             /* //  RotatingDrawable rotatingDrawable = new RotatingDrawable(getIconDrawable());
                iFloatingActionsMenu.this.imRotatingDrawable = rotatingDrawable;
                OvershootInterpolator interpolator = new OvershootInterpolator();
                ObjectAnimator collapseAnimator = ObjectAnimator.ofFloat(rotatingDrawable, "rotation", new float[]{iFloatingActionsMenu.EXPANDED_PLUS_ROTATION, iFloatingActionsMenu.COLLAPSED_PLUS_ROTATION});
                ObjectAnimator expandAnimator = ObjectAnimator.ofFloat(rotatingDrawable, "rotation", new float[]{iFloatingActionsMenu.COLLAPSED_PLUS_ROTATION, iFloatingActionsMenu.EXPANDED_PLUS_ROTATION});
                collapseAnimator.setInterpolator(interpolator);
                expandAnimator.setInterpolator(interpolator);
                iFloatingActionsMenu.this.imExpandAnimation.play(expandAnimator);
                iFloatingActionsMenu.this.imCollapseAnimation.play(collapseAnimator);
                return rotatingDrawable;*/
             return null;
            }
        };
      //  this.imAddButton.setId(R.id.fab_expand_menu_button);
        this.imAddButton.setSize(this.imAddButtonSize);
        this.imAddButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                iFloatingActionsMenu.this.toggle();
            }
        });
        addView(this.imAddButton, super.generateDefaultLayoutParams());
        this.imButtonsCount += LABELS_ON_RIGHT_SIDE;
    }

    public void addButton(iFloatingActionButton button) {
        addView(button, this.imButtonsCount - 1);
        this.imButtonsCount += LABELS_ON_RIGHT_SIDE;
        if (this.imLabelsStyle != 0) {
            createLabels();
        }
    }

    public void removeButton(iFloatingActionButton button) {
        removeView(button);
       // button.setTag(R.id.fab_label, null);
        this.imButtonsCount--;
    }

    private int getColor(@ColorRes int id) {
        return getResources().getColor(id);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int i = LABELS_ON_LEFT_SIDE;
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        int width = LABELS_ON_LEFT_SIDE;
        int height = LABELS_ON_LEFT_SIDE;
        this.imMaxButtonWidth = LABELS_ON_LEFT_SIDE;
        this.imMaxButtonHeight = LABELS_ON_LEFT_SIDE;
        int maxLabelWidth = LABELS_ON_LEFT_SIDE;
        for (int i2 = LABELS_ON_LEFT_SIDE; i2 < this.imButtonsCount; i2 += LABELS_ON_RIGHT_SIDE) {
            View child = getChildAt(i2);
            if (child.getVisibility() != GONE) {
                switch (this.imExpandDirection) {
                    case LABELS_ON_LEFT_SIDE /*0*/:
                    case LABELS_ON_RIGHT_SIDE /*1*/:
                        this.imMaxButtonWidth = Math.max(this.imMaxButtonWidth, child.getMeasuredWidth());
                        height += child.getMeasuredHeight();
                        break;
                    case EXPAND_LEFT /*2*/:
                    case EXPAND_RIGHT /*3*/:
                        width += child.getMeasuredWidth();
                        this.imMaxButtonHeight = Math.max(this.imMaxButtonHeight, child.getMeasuredHeight());
                        break;
                }
                if (!expandsHorizontally()) {

                }
            }
        }
        if (expandsHorizontally()) {
            height = this.imMaxButtonHeight;
        } else {
            int i3 = this.imMaxButtonWidth;
            if (maxLabelWidth > 0) {
                i = this.imLabelsMargin + maxLabelWidth;
            }
            width = i3 + i;
        }
        switch (this.imExpandDirection) {
            case LABELS_ON_LEFT_SIDE /*0*/:
            case LABELS_ON_RIGHT_SIDE /*1*/:
                height = adjustForOvershoot(height + (this.imButtonSpacing * (this.imButtonsCount - 1)));
                break;
            case EXPAND_LEFT /*2*/:
            case EXPAND_RIGHT /*3*/:
                width = adjustForOvershoot(width + (this.imButtonSpacing * (this.imButtonsCount - 1)));
                break;
        }
        setMeasuredDimension(width, height);
    }

    private int adjustForOvershoot(int dimension) {
        return (dimension * 12) / 10;
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int i;
        View child;
        int childX;
        int childY=0;
        float collapsedTranslation;
        LayoutParams params;
        ObjectAnimator access$700;
        float[] fArr;
        switch (this.imExpandDirection) {
            case LABELS_ON_LEFT_SIDE /*0*/:
            case LABELS_ON_RIGHT_SIDE /*1*/:
                int nextY;
                boolean expandUp = this.imExpandDirection == 0;
                if (changed) {
                    this.imITouchDelegateGroup.clearTouchDelegates();
                }
                int addButtonY = expandUp ? (b - t) - this.imAddButton.getMeasuredHeight() : LABELS_ON_LEFT_SIDE;
                int buttonsHorizontalCenter = this.imLabelsPosition == 0 ? (r - l) - (this.imMaxButtonWidth / EXPAND_LEFT) : this.imMaxButtonWidth / EXPAND_LEFT;
                int addButtonLeft = buttonsHorizontalCenter - (this.imAddButton.getMeasuredWidth() / EXPAND_LEFT);
                this.imAddButton.layout(addButtonLeft, addButtonY, this.imAddButton.getMeasuredWidth() + addButtonLeft, this.imAddButton.getMeasuredHeight() + addButtonY);
                int labelsOffset = (this.imMaxButtonWidth / EXPAND_LEFT) + this.imLabelsMargin;
                int labelsXNearButton = this.imLabelsPosition == 0 ? buttonsHorizontalCenter - labelsOffset : buttonsHorizontalCenter + labelsOffset;
                if (expandUp) {
                    nextY = addButtonY - this.imButtonSpacing;
                } else {
                    nextY = (this.imAddButton.getMeasuredHeight() + addButtonY) + this.imButtonSpacing;
                }
                for (i = this.imButtonsCount - 1; i >= 0; i--) {
                    child = getChildAt(i);
                    if (!(child == this.imAddButton || child.getVisibility() == View.GONE)) {
                        childX = buttonsHorizontalCenter - (child.getMeasuredWidth() / EXPAND_LEFT);
                        if (expandUp) {
                            childY = nextY - child.getMeasuredHeight();
                        } else {
                            childY = nextY;
                        }
                        child.layout(childX, childY, child.getMeasuredWidth() + childX, child.getMeasuredHeight() + childY);
                        collapsedTranslation = (float) (addButtonY - childY);
                        child.setTranslationY(this.imExpanded ? COLLAPSED_PLUS_ROTATION : collapsedTranslation);
                        child.setAlpha(this.imExpanded ? 1.0f : COLLAPSED_PLUS_ROTATION);
                        params = (LayoutParams) child.getLayoutParams();
                        access$700 = params.mCollapseDir;
                        fArr = new float[EXPAND_LEFT];
                        fArr[LABELS_ON_LEFT_SIDE] = COLLAPSED_PLUS_ROTATION;
                        fArr[LABELS_ON_RIGHT_SIDE] = collapsedTranslation;
                        access$700.setFloatValues(fArr);
                        access$700 = params.mExpandDir;
                        fArr = new float[EXPAND_LEFT];
                        fArr[LABELS_ON_LEFT_SIDE] = collapsedTranslation;
                        fArr[LABELS_ON_RIGHT_SIDE] = COLLAPSED_PLUS_ROTATION;
                        access$700.setFloatValues(fArr);
                        params.setAnimationsTarget(child);
                       // View label = (View) child.getTag(R.id.action_a);
                      /*  if (label != null) {
                            int labelXAwayFromButton;
                            int labelLeft;
                            int labelRight;
                            if (this.imLabelsPosition == 0) {
                                labelXAwayFromButton = labelsXNearButton - label.getMeasuredWidth();
                            } else {
                                labelXAwayFromButton = labelsXNearButton + label.getMeasuredWidth();
                            }
                            if (this.imLabelsPosition == 0) {
                                labelLeft = labelXAwayFromButton;
                            } else {
                                labelLeft = labelsXNearButton;
                            }
                            if (this.imLabelsPosition == 0) {
                                labelRight = labelsXNearButton;
                            } else {
                                labelRight = labelXAwayFromButton;
                            }
                            int labelTop = (childY - this.imLabelsVerticalOffset) + ((child.getMeasuredHeight() - label.getMeasuredHeight()) / EXPAND_LEFT);
                            label.layout(labelLeft, labelTop, labelRight, label.getMeasuredHeight() + labelTop);
                            this.imITouchDelegateGroup.addTouchDelegate(new TouchDelegate(new Rect(Math.min(childX, labelLeft), childY - (this.imButtonSpacing / EXPAND_LEFT), Math.max(child.getMeasuredWidth() + childX, labelRight), (child.getMeasuredHeight() + childY) + (this.imButtonSpacing / EXPAND_LEFT)), child));
                            label.setTranslationY(this.imExpanded ? COLLAPSED_PLUS_ROTATION : collapsedTranslation);
                            label.setAlpha(this.imExpanded ? 1.0f : COLLAPSED_PLUS_ROTATION);
                            LayoutParams labelParams = (LayoutParams) label.getLayoutParams();*/
                          /*  access$700 = labelParams.mCollapseDir;
                            fArr = new float[EXPAND_LEFT];
                            fArr[LABELS_ON_LEFT_SIDE] = COLLAPSED_PLUS_ROTATION;
                            fArr[LABELS_ON_RIGHT_SIDE] = collapsedTranslation;
                            access$700.setFloatValues(fArr);
                            access$700 = labelParams.mExpandDir;
                            fArr = new float[EXPAND_LEFT];
                            fArr[LABELS_ON_LEFT_SIDE] = collapsedTranslation;
                            fArr[LABELS_ON_RIGHT_SIDE] = COLLAPSED_PLUS_ROTATION;
                            access$700.setFloatValues(fArr);
                            labelParams.setAnimationsTarget(label);*/
                      //  }
                        if (expandUp) {
                          //  nextY = childY - this.imButtonSpacing;
                        } else {
                            nextY = (child.getMeasuredHeight() + childY) + this.imButtonSpacing;
                        }
                    }
                }
                return;
            case 2 /*2*/:
            case 3 /*3*/:
                int nextX;
                boolean expandLeft = this.imExpandDirection == EXPAND_LEFT;
                int addButtonX = expandLeft ? (r - l) - this.imAddButton.getMeasuredWidth() : LABELS_ON_LEFT_SIDE;
                int addButtonTop = ((b - t) - this.imMaxButtonHeight) + ((this.imMaxButtonHeight - this.imAddButton.getMeasuredHeight()) / EXPAND_LEFT);
                this.imAddButton.layout(addButtonX, addButtonTop, this.imAddButton.getMeasuredWidth() + addButtonX, this.imAddButton.getMeasuredHeight() + addButtonTop);
                if (expandLeft) {
                    nextX = addButtonX - this.imButtonSpacing;
                } else {
                    nextX = (this.imAddButton.getMeasuredWidth() + addButtonX) + this.imButtonSpacing;
                }
                for (i = this.imButtonsCount - 1; i >= 0; i--) {
                    child = getChildAt(i);
                    if (!(child == this.imAddButton || child.getVisibility() == View.GONE)) {
                        if (expandLeft) {
                            childX = nextX - child.getMeasuredWidth();
                        } else {
                            childX = nextX;
                        }
                        childY = addButtonTop + ((this.imAddButton.getMeasuredHeight() - child.getMeasuredHeight()) / EXPAND_LEFT);
                        child.layout(childX, childY, child.getMeasuredWidth() + childX, child.getMeasuredHeight() + childY);
                        collapsedTranslation = (float) (addButtonX - childX);
                        child.setTranslationX(this.imExpanded ? COLLAPSED_PLUS_ROTATION : collapsedTranslation);
                        child.setAlpha(this.imExpanded ? 1.0f : COLLAPSED_PLUS_ROTATION);
                        params = (LayoutParams) child.getLayoutParams();
                        access$700 = params.mCollapseDir;
                        fArr = new float[EXPAND_LEFT];
                        fArr[LABELS_ON_LEFT_SIDE] = COLLAPSED_PLUS_ROTATION;
                        fArr[LABELS_ON_RIGHT_SIDE] = collapsedTranslation;
                        access$700.setFloatValues(fArr);
                        access$700 = params.mExpandDir;
                        fArr = new float[EXPAND_LEFT];
                        fArr[LABELS_ON_LEFT_SIDE] = collapsedTranslation;
                        fArr[LABELS_ON_RIGHT_SIDE] = COLLAPSED_PLUS_ROTATION;
                        access$700.setFloatValues(fArr);
                        params.setAnimationsTarget(child);
                        if (expandLeft) {
                            nextX = childX - this.imButtonSpacing;
                        } else {
                            nextX = (child.getMeasuredWidth() + childX) + this.imButtonSpacing;
                        }
                    }
                }
                return;
            default:
                return;
        }
    }

    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(super.generateDefaultLayoutParams());
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(super.generateLayoutParams(attrs));
    }

    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams(super.generateLayoutParams(p));
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return super.checkLayoutParams(p);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        bringChildToFront(this.imAddButton);
        this.imButtonsCount = getChildCount();
        if (this.imLabelsStyle != 0) {
            createLabels();
        }
    }

    private void createLabels() {
        Context context = new ContextThemeWrapper(getContext(), this.imLabelsStyle);
        for (int i = LABELS_ON_LEFT_SIDE; i < this.imButtonsCount; i += LABELS_ON_RIGHT_SIDE) {
            iFloatingActionButton button = (iFloatingActionButton) getChildAt(i);
            String title = button.getTitle();
            if (!(button == this.imAddButton || title == null)) {
                TextView label = new TextView(context);
                label.setTextAppearance(getContext(), this.imLabelsStyle);
                label.setText(button.getTitle());
                addView(label);
              //  button.setTag(R.id.action_a, label);
            }
        }
    }

    public void collapse() {
        collapse(false);
    }

    public void collapseImmediately() {
        collapse(true);
    }

    private void collapse(boolean immediately) {
        if (this.imExpanded) {
            this.imExpanded = false;
            this.imITouchDelegateGroup.setEnabled(false);
            this.imCollapseAnimation.setDuration(immediately ? 0 : 300);
            this.imCollapseAnimation.start();
            this.imExpandAnimation.cancel();
            if (this.mListener != null) {
                this.mListener.onMenuCollapsed();
            }
        }
    }

    public void toggle() {
        if (this.imExpanded) {
            collapse();
        } else {
            expand();
        }
    }

    public void expand() {
        if (!this.imExpanded) {
            this.imExpanded = true;
            this.imITouchDelegateGroup.setEnabled(true);
            this.imCollapseAnimation.cancel();
            this.imExpandAnimation.start();
            if (this.mListener != null) {
                this.mListener.onMenuExpanded();
            }
        }
    }

    public boolean isExpanded() {
        return this.imExpanded;
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.imAddButton.setEnabled(enabled);
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.imExpanded = this.imExpanded;
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof SavedState) {
            SavedState savedState = (SavedState) state;
            this.imExpanded = savedState.imExpanded;
            this.imITouchDelegateGroup.setEnabled(this.imExpanded);
            if (this.imRotatingDrawable != null) {
                this.imRotatingDrawable.setRotation(this.imExpanded ? EXPANDED_PLUS_ROTATION : COLLAPSED_PLUS_ROTATION);
            }
            super.onRestoreInstanceState(savedState.getSuperState());
            return;
        }
        super.onRestoreInstanceState(state);
    }
}
