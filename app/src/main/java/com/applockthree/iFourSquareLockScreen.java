package com.applockthree;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class iFourSquareLockScreen extends AppCompatActivity implements OnClickListener {
    private  RelativeLayout Pinlockll;
    private  ImageView appicon;
    private  TextView appname,bf;
    private  Button[] button;
    private   int cnt = 0;
    private  Date date;
    private  DataBase db;
    private SimpleDateFormat dm;
    private ImageView[] dot;
    private Drawable[] dotid;
    private  SimpleDateFormat dy;
    private Drawable emergency;
    private String password = BuildConfig.FLAVOR;
    private  PackageManager pm;
    private  SimpleDateFormat th,tm,ts,dd;
    private  int v = 0;
    private  Vibrator vb;
    private  LayoutParams wmlp;

    protected void onCreate(Bundle savedInstanceState) {
        Resources resources1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_i_four_square_lock_screen);
        getWindow().setFlags(1024, 1024);
        th = new SimpleDateFormat("hh");
        tm = new SimpleDateFormat("mm");
        ts = new SimpleDateFormat("ss a");
        dd = new SimpleDateFormat("dd");
        dm = new SimpleDateFormat("MM");
        dy = new SimpleDateFormat("yy");
        db = new DataBase(this);
        dot = new ImageView[5];
        dotid = new Drawable[5];
        Addmob();
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        emergency = getResources().getDrawable(R.drawable.fpassword);
        button = new Button[5];
        pm = getBaseContext().getPackageManager();
        try {
            Pinlockll = (RelativeLayout) findViewById(R.id.Pinlockll);
            appicon = (ImageView) findViewById(R.id.appicon);
            appname = (TextView) findViewById(R.id.appname);
            bf = (TextView) findViewById(R.id.bf);
            button[1] = (Button) findViewById(R.id.xy1);
            button[2] = (Button) findViewById(R.id.xy2);
            button[3] = (Button) findViewById(R.id.xy3);
            button[4] = (Button) findViewById(R.id.xy4);
            bf.setOnClickListener(this);
            button[1].setOnClickListener(this);
            button[2].setOnClickListener(this);
            button[3].setOnClickListener(this);
            button[4].setOnClickListener(this);
            dot[1] = (ImageView) findViewById(R.id.idot1);
            dot[2] = (ImageView) findViewById(R.id.idot2);
            dot[3] = (ImageView) findViewById(R.id.idot3);
            dot[4] = (ImageView) findViewById(R.id.idot4);
            Cursor cursor = db.getitheme();
            cursor.moveToNext();
            String itheme = cursor.getString(0);
            String ithemepkg = cursor.getString(1);
            String ithemebg = cursor.getString(2);
            cursor.close();
            Resources resources = pm.getResourcesForApplication(ithemepkg);
            int idm0 = resources.getIdentifier(itheme + "fsbox1", "drawable", ithemepkg);
            int idm1 = resources.getIdentifier(itheme + "fsbox2", "drawable", ithemepkg);
            int idm2 = resources.getIdentifier(itheme + "fsbox3", "drawable", ithemepkg);
            int idm3 = resources.getIdentifier(itheme + "fsbox4", "drawable", ithemepkg);
            emergency = resources.getDrawable(resources.getIdentifier("fpassword", "drawable", ithemepkg));
            button[1].setBackground(resources.getDrawable(idm0));
            button[2].setBackground(resources.getDrawable(idm1));
            button[3].setBackground(resources.getDrawable(idm2));
            button[4].setBackground(resources.getDrawable(idm3));
            bf.setBackground(emergency);
            dotid[0] = resources.getDrawable(resources.getIdentifier(itheme + "dot", "drawable", ithemepkg));
            dotid[1] = resources.getDrawable(resources.getIdentifier(itheme + "doti", "drawable", ithemepkg));
            dot[1].setBackground(dotid[0]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
            Cursor cursor1 = db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 = db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 = pm.getResourcesForApplication(cursor1.getString(1));
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                    Pinlockll.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e2) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
        } catch (NameNotFoundException e3) {
            e3.printStackTrace();
        }
    }

    private void vcheck() {
        try {
            Cursor cursor1 = db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
                db.deletebackground(cursor1.getString(1));
                Resources resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
        }
    }

    protected void onResume() {
        vcheck();
        super.onResume();
    }

    public void onClick(View v) {
        if (v.getId() == R.id.xy1) {
            check("1");
        }
        if (v.getId() == R.id.xy2) {
            check("2");
        }
        if (v.getId() == R.id.xy3) {
            check("3");
        }
        if (v.getId() == R.id.xy4) {
            check("4");
        }
        if (v.getId() == R.id.bf) {
            PopupMenu popup = new PopupMenu(this, v);
            popup.getMenuInflater().inflate(R.menu.actions, popup.getMenu());
            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                   cnt = 0;
                   password = BuildConfig.FLAVOR;
                   setdot();
                   db.setfp("true");
                    Intent startHome = new Intent(getBaseContext(), Forgotpassword.class);
                    startHome.addCategory("android.intent.category.HOME");
                    startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   startActivity(startHome);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                        }
                    }, 150);
                    return false;
                }
            });
            popup.show();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() != 4 || event.getAction() != 0) {
            if (db.getfastunlockm1().contains("true")) {
                if (event.getKeyCode() == 24 && event.getAction() == 0) {
                    v = 1;
                    return true;
                } else if (event.getKeyCode() == 24 && event.getAction() == 1) {
                    v = 0;
                    return true;
                }
            }
            if (db.getfastunlockm2().contains("true")) {
                if (event.getKeyCode() == 25 && event.getAction() == 0) {
                    v = 1;
                    return true;
                } else if (event.getKeyCode() == 25 && event.getAction() == 1) {
                    v = 0;
                    return true;
                }
            }
            return super.dispatchKeyEvent(event);
        } else if (v == 1) {
            v = 0;
            finish();
            return true;
        } else {
            Intent startHome = new Intent("android.intent.action.MAIN");
            startHome.addCategory("android.intent.category.HOME");
            startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startHome);
            return true;
        }
    }

    public void check(String key) {
        if (db.getsound().contains("true")) {
            MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.click);
            mp.start();
            mp.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        if (db.getisettingvibrate().contains("true")) {
            vb.vibrate(50);
        }
        password += key;
        cnt++;
        setdot();
        if (cnt < 4) {
            return;
        }
        if (db.matchpassword(password)) {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                   finish();
                   cnt = 0;
                   password = BuildConfig.FLAVOR;
                   setdot();
                }
            }, 100);
            return;
        }
        if (db.getisettingvibrate().contains("true")) {
            vb.vibrate(200);
        }
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            public void run() {
               date = new Date();
                String d ;
                String t;
                String an = BuildConfig.FLAVOR;
                d =dd.format(date) + "/" +dm.format(date) + "/" +dy.format(date);
                t =th.format(date) + ":" +tm.format(date) + ":" +ts.format(date);
                try {
                    an =pm.getApplicationLabel(pm.getApplicationInfo(db.getcurrentapp(), PackageManager.GET_META_DATA)).toString();
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
               db.insertapphacklog(iFourSquareLockScreen.this.db.getcurrentapp(), an,password, d, t);
               cnt = 0;
               password = BuildConfig.FLAVOR;
               setdot();
            }
        }, 100);
    }
    public void Addmob() {
        Log.e("FourSquareLockScreen","Activity");
        AdView adView = (AdView) findViewById(R.id.adView_ifoursqurare);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }
    public void setdot() {
        if (cnt == 0) {
            dot[1].setBackground(dotid[0]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 1) {
            dot[1].setBackground(dotid[1]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 2) {
            dot[2].setBackground(dotid[1]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 3) {
            dot[3].setBackground(dotid[1]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 4) {
            dot[4].setBackground(dotid[1]);
        }
    }

    @Override
    public void onBackPressed() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
        super.onBackPressed();
    }
}
