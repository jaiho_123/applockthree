package com.applockthree;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Forgotpassword extends AppCompatActivity {
    private TextView answer;
    private DataBase db;
    private String[] items;
    private TextView question;
    private Button save;
    private Typeface tf;

    class C04711 implements OnClickListener {

        class C04701 implements Runnable {
            C04701() {
            }

            public void run() {
                db.setfp("false");
            }
        }

        C04711() {
        }

        public void onClick(View v) {
            if (answer.getText().toString().trim().contains(db.getisettinganswer())) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                db.setfp("true");
                new Handler().postDelayed(new C04701(), 500);
                startActivity(intent);
                return;
            }
            Toast.makeText(Forgotpassword.this, Forgotpassword.this.getResources().getString(R.string.Wrong_answer), 0).show();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        db = new DataBase(this);
        question = (TextView) findViewById(R.id.spinnerquestion);
        String[] s = new String[16];
        s[1] = getResources().getString(R.string.q1);
        s[2] = getResources().getString(R.string.q2);
        s[3] = getResources().getString(R.string.q3);
        s[4] = getResources().getString(R.string.q4);
        s[5] = getResources().getString(R.string.q5);
        s[6] = getResources().getString(R.string.q6);
        s[7] = getResources().getString(R.string.q7);
        s[8] = getResources().getString(R.string.q8);
        s[9] = getResources().getString(R.string.q9);
        s[10] = getResources().getString(R.string.q10);
        s[11] = getResources().getString(R.string.q11);
        s[12] = getResources().getString(R.string.q12);
        s[13] = getResources().getString(R.string.q13);
        s[14] = getResources().getString(R.string.q14);
        s[15] = getResources().getString(R.string.q15);
        items = new String[]{s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11], s[12], s[13], s[14], s[15]};
        answer = (TextView) findViewById(R.id.txtanswerset);
        save = (Button) findViewById(R.id.btnstart);
        set();
        save.setOnClickListener(new C04711());
    }

    private void set() {
        if (!db.getisettingquestion().trim().isEmpty()) {
            question.setText(items[Integer.parseInt(db.getisettingquestion())]);
        }
    }


    public void onBackPressed() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);

    }

    protected void onStop() {
        finish();
        db.setfp("false");
        super.onStop();
    }
}
