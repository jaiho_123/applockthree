package com.applockthree;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class themeFragment extends Fragment {
    private String citheme, cithemepkg;
    private DataBase db;
    private GridView gridView;
    private String[] itheme, ithemebg, ithemepkg;
    private int tot = 0;
    private View view;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.theme_layout, null);
        db = new DataBase(getContext());
        return this.view;
    }

    public void set() {
        gridView = (GridView) view.findViewById(R.id.gridViewitheme);
        gridView.setAdapter(new CustomGridAdapteritheme(getContext(), itheme, ithemepkg, ithemebg, citheme, cithemepkg, view));
        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            }
        });
    }

    public void onResume() {
        Cursor cursor = db.getalltheme();
        tot = cursor.getCount();
        itheme = new String[tot];
        ithemebg = new String[tot];
        ithemepkg = new String[tot];
        for (int i = 0; i < tot; i++) {
            cursor.moveToNext();
            itheme[i] = cursor.getString(0);
            ithemebg[i] = cursor.getString(2);
            ithemepkg[i] = cursor.getString(1);
        }
        cursor = db.getitheme();
        cursor.moveToNext();
        citheme = cursor.getString(0);
        cithemepkg = cursor.getString(1);
        set();
        super.onResume();
    }
}
