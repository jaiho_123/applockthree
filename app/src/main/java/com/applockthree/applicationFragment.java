package com.applockthree;

import android.app.AppOpsManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class applicationFragment extends Fragment {
    private customlistapps adapter = null;
    private String[] appdetail;
    private DataBase db;
    private Handler handler;
    private Drawable[] icon;
    private String[] lock, name, packag;
    private ProgressDialog progressDialog;
    private View rootview;

    private class LoadApplicationTask extends AsyncTask<Integer, Integer, Integer> {
        final PackageManager pm;

        private LoadApplicationTask() {
            pm = getActivity().getPackageManager();
        }

        protected void onPreExecute() {
            if (db.gettotalapp() <= 50) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setCancelable(false);
                progressDialog.show();
                progressDialog.setMessage(applicationFragment.this.getResources().getString(R.string.Please_wait));
            }
            super.onPreExecute();
        }

        protected Integer doInBackground(Integer... params) {
            Intent mainIntent = new Intent("android.intent.action.MAIN", null);
            mainIntent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> mApps = getActivity().getPackageManager().queryIntentActivities(mainIntent, 0);
            int length = mApps.size();
            for (int i = 0; i < length; i++) {
                ResolveInfo info = mApps.get(i);
                if (!db.getpackageavilable(info.activityInfo.packageName)) {
                    if (info.activityInfo.packageName.contains("com.applockthree.theme")) {
                        db.inserttheme("itheme1", info.activityInfo.packageName, "bg1");
                        db.insertbackground("background", info.activityInfo.packageName);
                    } else if (!info.activityInfo.packageName.contentEquals(BuildConfig.APPLICATION_ID)) {
                        try {
                            appdetail[1] = pm.getApplicationLabel(pm.getApplicationInfo(info.activityInfo.packageName, PackageManager.GET_META_DATA)).toString();
                            appdetail[1] = appdetail[1].substring(0, 1).toUpperCase() + appdetail[1].substring(1);
                            appdetail[2] = info.activityInfo.packageName;
                            appdetail[3] = info.icon + BuildConfig.FLAVOR;
                            appdetail[4] = "false";
                            appdetail[5] = "true";
                            db.insertapp(appdetail[1], appdetail[2], appdetail[3], appdetail[4], appdetail[5]);
                        } catch (NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (isPackageInstalled("com.android.packageinstaller") && !db.getpackageavilable("com.android.packageinstaller")) {
                try {
                    appdetail[1] = "App Install/Uninstall ";
                    appdetail[2] = "com.android.packageinstaller";
                    appdetail[3] = BuildConfig.FLAVOR;
                    appdetail[4] = "true";
                    appdetail[5] = "true";
                    db.insertapp(appdetail[1], appdetail[2], appdetail[3], appdetail[4], appdetail[5]);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else if (isPackageInstalled("com.google.android.packageinstaller") && !db.getpackageavilable("com.google.android.packageinstaller")) {
                try {
                    appdetail[1] = "App Install/Uninstall ";
                    appdetail[2] = "com.google.android.packageinstaller";
                    appdetail[3] = BuildConfig.FLAVOR;
                    appdetail[4] = "true";
                    appdetail[5] = "true";
                    db.insertapp(appdetail[1], appdetail[2], appdetail[3], appdetail[4], appdetail[5]);
                } catch (Exception e22) {
                    e22.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            try {
                applicationFragment.this.progressDialog.dismiss();
            } catch (Exception e) {
            }
            Cursor cursor = applicationFragment.this.db.getallapp();
            int temp = cursor.getCount();
            try {
                applicationFragment.this.getContext().getPackageManager();
            } catch (Exception e2) {
            }
            name = new String[(temp + 1)];
            icon = new Drawable[(temp + 1)];
            packag = new String[(temp + 1)];
            lock = new String[(temp + 1)];
            for (int i = 0; i < temp; i++) {
                cursor.moveToNext();
                name[i] = cursor.getString(0);
                packag[i] = cursor.getString(1);
                try {
                    icon[i] = pm.getApplicationIcon(packag[i]);
                } catch (NameNotFoundException e3) {
                    e3.printStackTrace();
                }
                lock[i] = cursor.getString(3);
            }
            name[temp] = "none";
            cursor.close();
            try {
                adapter = new customlistapps(getActivity(), name, packag, icon, lock);
                ((ListView) rootview.findViewById(R.id.listviewapp)).setAdapter(adapter);
            } catch (Exception e4) {
            }
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.application_layout, container, false);
        handler = new Handler();
        appdetail = new String[6];
        db = new DataBase(getActivity());
        (rootview.findViewById(R.id.fab1)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(1);
                dialog.setContentView(R.layout.allapp);
                Button btncancle = (Button) dialog.findViewById(R.id.btnallunlock);
                (dialog.findViewById(R.id.btnalllock)).setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (testPermission()) {
                            db.setallapplockbymasterpassword1();
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            } else {
                                new LoadApplicationTask().execute(new Integer[0]);
                            }
                        } else {
                            final Dialog dialog = new Dialog(getContext());
                            dialog.requestWindowFeature(1);
                            dialog.setContentView(R.layout.permission);
                            Button btncancle = (Button) dialog.findViewById(R.id.btncancel);
                            (dialog.findViewById(R.id.btnpermit)).setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.Enable_usage_state_of_iApplock), Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent();
                                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$UsageAccessSettingsActivity"));
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    applicationFragment.this.getContext().startActivity(intent);
                                    dialog.dismiss();
                                }
                            });
                            btncancle.setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.setOnCancelListener(new OnCancelListener() {
                                public void onCancel(DialogInterface dialog) {
                                }
                            });
                            dialog.show();
                        }
                        dialog.dismiss();
                    }
                });
                btncancle.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        db.setallappunlock();
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        } else {
                            new LoadApplicationTask().execute(new Integer[0]);
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        this.handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    new LoadApplicationTask().execute(new Integer[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 125);
        return rootview;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onResume() {
        if (db.getinstaller().contains("true")) {
            db.setinstaller("false");
            handler.postDelayed(new Runnable() {
                public void run() {
                    new LoadApplicationTask().execute(new Integer[0]);
                }
            }, 125);
        } else if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        super.onResume();
    }

    private boolean testPermission() {
        if (VERSION.SDK_INT < 21) {
            return true;
        }
        try {
            ApplicationInfo applicationInfo = getContext().getPackageManager().getApplicationInfo(getContext().getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getContext().getSystemService(Context.APP_OPS_SERVICE);
            int mode = 0;
            if (VERSION.SDK_INT >= 21) {
                mode = appOpsManager.checkOpNoThrow("android:get_usage_stats", applicationInfo.uid, applicationInfo.packageName);
            }
            if (mode != 0) {
                return false;
            }
            getContext().startService(new Intent(getContext(), MyService.class));
            return true;
        } catch (NameNotFoundException e) {
            return true;
        } catch (Exception e2) {
            return true;
        }
    }

    private boolean isPackageInstalled(String packagename) {
        try {
            getActivity().getPackageManager().getPackageInfo(packagename, 1);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }
}
