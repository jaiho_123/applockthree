package com.applockthree;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class Password extends Fragment {
    RelativeLayout[] rl = new RelativeLayout[10];
    View view;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.password_layout, null);
        rl[1] = (RelativeLayout) view.findViewById(R.id.txtp111);
        rl[2] = (RelativeLayout) view.findViewById(R.id.txtp222);
        rl[4] = (RelativeLayout) view.findViewById(R.id.txtp444);
        rl[5] = (RelativeLayout) view.findViewById(R.id.txtp555);
        rl[6] = (RelativeLayout) view.findViewById(R.id.txtp666);
        rl[7] = (RelativeLayout) view.findViewById(R.id.txtp777);
        rl[1].setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
               menu1();
            }
        });
        rl[2].setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                menu2();
            }
        });
        rl[4].setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                menu4();
            }
        });
        rl[5].setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                menu5();
            }
        });
        rl[6].setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                menu6();
            }
        });
        rl[7].setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                menu7();
            }
        });
        return this.view;
    }

    public void menu1() {
        getContext().startService(new Intent(getContext(), PINLockScreenchangepassword.class));
    }

    public void menu2() {
        getContext().startService(new Intent(getContext(), PatternLockScreenchangepassword.class));
    }

    public void menu4() {
    }

    public void menu5() {
        getContext().startService(new Intent(getContext(), ImageTouchLockScreenchangepassword.class));
    }

    public void menu6() {
        getContext().startService(new Intent(getContext(), FourSquareLockScreenchangepassword.class));
    }

    public void menu7() {
    }
}
