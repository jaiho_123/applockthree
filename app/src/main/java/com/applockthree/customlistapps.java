package com.applockthree;

import android.app.Activity;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build.VERSION;
import android.os.Vibrator;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class customlistapps extends ArrayAdapter<String> {
    private final Activity context;
    private DataBase db;
    private final Drawable[] icon;
    private CharSequence[] items;
    private final String[] lock;
    private final String[] name;
    private final String[] packag;
    private Vibrator vb;

    public customlistapps(Context context, String[] name, String[] packag, Drawable[] icon, String[] lock) {
        super(context, R.layout.list_single_apps, name);
        db = new DataBase(context);
        this.context = (Activity) context;
        this.name = name;
        this.packag = packag;
        this.lock = lock;
        items = new CharSequence[]{context.getString(R.string.msterkey), context.getString(R.string.otherkkey), context.getString(R.string.cancels)};
        this.icon = icon;
        this.vb = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = this.context.getLayoutInflater().inflate(R.layout.list_single_apps, null, true);
        ImageView appicon = (ImageView) view.findViewById(R.id.appicon);
        TextView appname = (TextView) view.findViewById(R.id.appname);
        final SwitchCompat applock = (SwitchCompat) view.findViewById(R.id.applock);
        RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.apprl);
        if (name[position].contains("none")) {
            appicon.setVisibility(View.INVISIBLE);
            appname.setVisibility(View.INVISIBLE);
            applock.setVisibility(View.INVISIBLE);
            rl.setBackgroundResource(R.drawable.blank);
        } else {
            appicon.setImageDrawable(icon[position]);
            appname.setText(name[position]);
            if (db.getapplockbypackage(packag[position]).contains("true")) {
                applock.setChecked(true);
            } else {
                applock.setChecked(false);
            }
            applock.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (applock.isChecked()) {
                        if (db.getsound().contains("true")) {
                            MediaPlayer mp = MediaPlayer.create(context, R.raw.on);
                            mp.start();
                            mp.setOnCompletionListener(new OnCompletionListener() {
                                public void onCompletion(MediaPlayer mp) {
                                    mp.release();
                                }
                            });
                        }
                        if (db.getisettingvibrate().contains("true")) {
                            vb.vibrate(50);
                        }
                        final Dialog dialog;
                        Button btncancle;
                        if (testPermission()) {
                            dialog = new Dialog(customlistapps.this.getContext());
                            dialog.requestWindowFeature(1);
                            dialog.setContentView(R.layout.customlock);
                            Button btnlo = (Button) dialog.findViewById(R.id.btnlo);
                            btncancle = (Button) dialog.findViewById(R.id.btnc);
                            (dialog.findViewById(R.id.btnlm)).setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    Log.e("Insert into db--", packag[position]);
                                    db.insertlockbypackage(packag[position], "true");
                                    dialog.dismiss();
                                }
                            });
                            btnlo.setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    Intent intent = new Intent("android.intent.action.MAIN");
                                    intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, "com.applockthree." + db.getlocktype() + "custom"));
                                    Toast.makeText(getContext(), BuildConfig.FLAVOR + packag[position], Toast.LENGTH_LONG).show();
                                    intent.putExtra("pkg", packag[position]);
                                    context.startService(intent);
                                    dialog.dismiss();
                                }
                            });
                            btncancle.setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    applock.setChecked(false);
                                    dialog.dismiss();
                                }
                            });
                            dialog.setOnCancelListener(new OnCancelListener() {
                                public void onCancel(DialogInterface dialog) {
                                    applock.setChecked(false);
                                }
                            });
                            dialog.show();
                            return;
                        }
                        dialog = new Dialog(customlistapps.this.getContext());
                        dialog.requestWindowFeature(1);
                        dialog.setContentView(R.layout.permission);
                        btncancle = (Button) dialog.findViewById(R.id.btncancel);
                        dialog.findViewById(R.id.btnpermit).setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                Toast.makeText(context, context.getResources().getString(R.string.Enable_usage_state_of_iApplock), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$UsageAccessSettingsActivity"));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                                dialog.dismiss();
                                applock.setChecked(false);
                            }
                        });
                        btncancle.setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                applock.setChecked(false);
                                dialog.dismiss();
                            }
                        });
                        dialog.setOnCancelListener(new OnCancelListener() {
                            public void onCancel(DialogInterface dialog) {
                                applock.setChecked(false);
                            }
                        });
                        dialog.show();
                        return;
                    }
                    if (db.getsound().contains("true")) {
                        MediaPlayer.create(context, R.raw.off).start();
                    }
                    if (db.getisettingvibrate().contains("true")) {
                        vb.vibrate(50);
                    }
                    db.insertlockbypackage(packag[position], "false");
                }
            });
        }
        return view;
    }

    private boolean testPermission() {
        if (VERSION.SDK_INT < 21) {
            return true;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            int mode = 0;
            if (VERSION.SDK_INT >= 21) {
                mode = appOpsManager.checkOpNoThrow("android:get_usage_stats", applicationInfo.uid, applicationInfo.packageName);
            }
            if (mode != 0) {
                return false;
            }
            context.startService(new Intent(context, MyService.class));
            return true;
        } catch (NameNotFoundException e) {
            return true;
        } catch (Exception e2) {
            return true;
        }
    }
}
