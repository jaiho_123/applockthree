package com.applockthree;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.applockthree.DataBase;
import com.applockthree.setting;
import com.applockthree.R;

public class questionansweremail extends Fragment {
    private TextView answer;
    private DataBase db;
    private String[] items;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Spinner question;
    private Button save;
    private Typeface tf;
    private View view;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.question_answer_email_layout, container, false);
        db = new DataBase(getContext());
        question = (Spinner) view.findViewById(R.id.spinnerquestion);
        String[] s = new String[16];
        s[1] = getResources().getString(R.string.q1);
        s[2] = getResources().getString(R.string.q2);
        s[3] = getResources().getString(R.string.q3);
        s[4] = getResources().getString(R.string.q4);
        s[5] = getResources().getString(R.string.q5);
        s[6] = getResources().getString(R.string.q6);
        s[7] = getResources().getString(R.string.q7);
        s[8] = getResources().getString(R.string.q8);
        s[9] = getResources().getString(R.string.q9);
        s[10] = getResources().getString(R.string.q10);
        s[11] = getResources().getString(R.string.q11);
        s[12] = getResources().getString(R.string.q12);
        s[13] = getResources().getString(R.string.q13);
        s[14] = getResources().getString(R.string.q14);
        s[15] = getResources().getString(R.string.q15);
       items = new String[]{s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11], s[12], s[13], s[14], s[15]};
       answer = (TextView)view.findViewById(R.id.txtanswerset);
       mFragmentManager = getActivity().getSupportFragmentManager();
       save = (Button)view.findViewById(R.id.btnsave);
        set();
       save.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String val = "^[a-zA-Z ]{1,1}[a-zA-Z0-9 _@*.,]{2,}";
                if (answer.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getContext(), R.string.ans, Toast.LENGTH_LONG).show();
                    return;
                }
                db.setisettingquestion(String.valueOf(question.getSelectedItemPosition()));
                db.setisettinganswer(answer.getText().toString().trim());
                mFragmentManager.beginTransaction().replace(R.id.containerView, new setting()).commit();
                db.setposition("setting");
            }
        });
        return view;
    }

    private void set() {
        setQuestion();
        if (!db.getisettingquestion().trim().isEmpty()) {
           question.setSelection(Integer.parseInt(db.getisettingquestion()));
        }
        if (!db.getisettinganswer().trim().isEmpty()) {
           answer.setText(db.getisettinganswer());
        }
    }

    public <ViewGroup> void setQuestion() {
        Spinner mySpinner = (Spinner)view.findViewById(R.id.spinnerquestion);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) getView(position, convertView, parent);
                v.setTextSize(13.0f);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) getView(position, convertView, parent);
                v.setTextSize(13.0f);
                return v;
            }


        };
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(adapter1);
    }
}
