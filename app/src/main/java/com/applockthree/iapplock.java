package com.applockthree;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class iapplock extends Fragment {
    public static int int_items = 2;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;

    class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int position) {
            switch (position) {
                case 0 /*0*/:
                    return new applicationFragment();
                case 1 /*1*/:
                    return new themeFragment();
                default:
                    return null;
            }
        }

        public int getCount() {
            return iapplock.int_items;
        }

        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0/*0*/:
                    return iapplock.this.getResources().getString(R.string.Apps);
                case 1 /*1*/:
                    return iapplock.this.getResources().getString(R.string.Theme);
                default:
                    return null;
            }
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.tab_layout, null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tabLayout.post(new Runnable() {
            public void run() {
                iapplock.tabLayout.setupWithViewPager(iapplock.viewPager);
            }
        });
        return x;
    }
}
