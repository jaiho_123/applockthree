package com.applockthree;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

public class SettingActivity extends PreferenceActivity {
    private Cursor cursor;
    private DataBase db;
    private CheckBoxPreference notification, sound, taskprevent, vibrate, applock, autostartafterrestart;
    private Preference txtquestionanswer;
    private Vibrator vb;
    private FragmentManager mFragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        db = new DataBase(this);
        vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        cursor = db.getisetting();
        cursor.moveToNext();
        applock = (CheckBoxPreference) findPreference("isettingiapplock");
        taskprevent = (CheckBoxPreference) findPreference("isettingtaskprevent");
        sound = (CheckBoxPreference) findPreference("isettingsound");
        vibrate = (CheckBoxPreference) findPreference("isettingvibrate");
        notification = (CheckBoxPreference) findPreference("isettingnotification");
        autostartafterrestart = (CheckBoxPreference) findPreference("isettingautostartafterrestart");
        set();
        taskprevent.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                if (isAccessibilitySettingsOn(SettingActivity.this)) {
                    taskprevent.setChecked(true);
                    //return;
                }
                Toast.makeText(SettingActivity.this, R.string.enableaccessiblity, Toast.LENGTH_LONG).show();
                startActivity(new Intent("android.settings.ACCESSIBILITY_SETTINGS"));
                taskprevent.setChecked(false);
                return true;
            }
        });

        sound.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MediaPlayer mp;
                if (sound.isChecked()) {
                    db.setisettingsound("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingsound("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
                return true;
            }
        });
        vibrate.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MediaPlayer mp;
                if (vibrate.isChecked()) {
                    db.setisettingvibrate("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingvibrate("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
                return true;
            }
        });
        notification.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MediaPlayer mp;
                if (notification.isChecked()) {
                    db.setisettingnotification("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingnotification("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
                return true;
            }
        });
        autostartafterrestart.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MediaPlayer mp;
                if (autostartafterrestart.isChecked()) {
                    db.setisettingasar("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingasar("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }
                return true;
            }
        });
     /*   txtquestionanswer=getPreferenceManager().findPreference("txtquestionanswer");
        txtquestionanswer.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                questionanswer();
                return true;
            }
        });*/


        applock.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MediaPlayer mp;
                if (applock.isChecked()) {
                    db.setisettingiapplock("true");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    db.setisettingiapplock("false");
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(SettingActivity.this, R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                }
                if (db.getisettingvibrate().contains("true")) {
                    vb.vibrate(50);
                }

                return true;
            }
        });
    }

    public void questionanswer() {
        // mFragmentManager.beginTransaction().replace(R.id.containerView, new questionansweremail()).commit();
        //  db.setposition(getString(R.string.quest));
    }

    public void onResume() {
        if (taskprevent != null) {
            if (isAccessibilitySettingsOn(SettingActivity.this)) {
                taskprevent.setChecked(true);
                Log.e("resume m acessiblity-", "true");
            } else {
                taskprevent.setChecked(false);
                Log.e("resume m acessiblity-", "false");
            }
        }
        super.onResume();
    }

    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        String service = getPackageName() + "/" + MyService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(mContext.getApplicationContext().getContentResolver(), "accessibility_enabled");
        } catch (Settings.SettingNotFoundException e) {
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');
        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(mContext.getApplicationContext().getContentResolver(), "enabled_accessibility_services");
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    if (mStringColonSplitter.next().equalsIgnoreCase(service)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void set() {
        if (cursor.getString(0).contains("true")) {
            applock.setChecked(true);
        } else {
            applock.setChecked(false);
        }
        if (cursor.getString(1).contains("true")) {
            sound.setChecked(true);
        } else {
            sound.setChecked(false);
        }
        if (cursor.getString(2).contains("true")) {
            vibrate.setChecked(true);
        } else {
            vibrate.setChecked(false);
        }
        if (cursor.getString(3).contains("true")) {
            notification.setChecked(true);
        } else {
            notification.setChecked(false);
        }
        if (cursor.getString(4).contains("true")) {
            autostartafterrestart.setChecked(true);
        } else {
            autostartafterrestart.setChecked(false);
        }
        cursor.close();
    }

 /*   @Override
    public void onBackPressed() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
        super.onBackPressed();
    }*/
}
