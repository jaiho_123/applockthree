package com.applockthree;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applockthree.utils.Lock9View;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class iPatternLockScreen extends AppCompatActivity implements OnClickListener {
    RelativeLayout Pinlockll;
    ImageView appicon;
    TextView appname;
    TextView bf;
    Drawable cancel;
    int cnt = 0;
    Date date;
    DataBase db;
    SimpleDateFormat dd;
    Drawable delete;
    SimpleDateFormat dm;
    SimpleDateFormat dy;
    Drawable emergency;
    private Lock9View lock9View;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String password = BuildConfig.FLAVOR;
    PackageManager pm;
    SimpleDateFormat th;
    SimpleDateFormat tm;
    SimpleDateFormat ts;
    int v = 0;
    Vibrator vb;
    LayoutParams wmlp;

    protected void onCreate(Bundle savedInstanceState) {
        Resources resources1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_i_pattern_lock_screen);
        getWindow().setFlags(1024, 1024);
        th = new SimpleDateFormat("hh");
        tm = new SimpleDateFormat("mm");
        ts = new SimpleDateFormat("ss a");
        dd = new SimpleDateFormat("dd");
        dm = new SimpleDateFormat("MM");
        dy = new SimpleDateFormat("yy");
        db = new DataBase(this);
        Addmob();
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        pm = getBaseContext().getPackageManager();
        emergency = getResources().getDrawable(R.drawable.fpassword);
        try {
            Pinlockll = (RelativeLayout) findViewById(R.id.Pinlockll);
            lock9View = (Lock9View) findViewById(R.id.lock_9_view);
            lock9View.setCallBack(new Lock9View.CallBack() {
                public void onFinish(String password) {
                    if (db.matchpassword(password)) {
                        finish();
                        return;
                    }
                    if (db.getisettingvibrate().contains("true")) {
                        vb.vibrate(200);
                    }
                    final String str = password;
                    new Handler(iPatternLockScreen.this.getMainLooper()).postDelayed(new Runnable() {
                        public void run() {
                            iPatternLockScreen.this.date = new Date();
                            String d = BuildConfig.FLAVOR;
                            String t = BuildConfig.FLAVOR;
                            String an = BuildConfig.FLAVOR;
                            d = dd.format(iPatternLockScreen.this.date) + "/" + iPatternLockScreen.this.dm.format(iPatternLockScreen.this.date) + "/" + iPatternLockScreen.this.dy.format(iPatternLockScreen.this.date);
                            t = iPatternLockScreen.this.th.format(iPatternLockScreen.this.date) + ":" + iPatternLockScreen.this.tm.format(iPatternLockScreen.this.date) + ":" + iPatternLockScreen.this.ts.format(iPatternLockScreen.this.date);
                            try {
                                an = iPatternLockScreen.this.pm.getApplicationLabel(iPatternLockScreen.this.pm.getApplicationInfo(iPatternLockScreen.this.db.getcurrentapp(), PackageManager.GET_META_DATA)).toString();
                            } catch (NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            iPatternLockScreen.this.db.insertapphacklog(iPatternLockScreen.this.db.getcurrentapp(), an, str, d, t);
                        }
                    }, 100);
                }
            });
            appicon = (ImageView) findViewById(R.id.appicon);
            appname = (TextView) findViewById(R.id.appname);
            bf = (TextView) findViewById(R.id.bf);
            bf.setOnClickListener(this);
            Cursor cursor = db.getitheme();
            cursor.moveToNext();
            String itheme = cursor.getString(0);
            String ithemepkg = cursor.getString(1);
            String ithemebg = cursor.getString(2);
            cursor.close();
            Resources resources = this.pm.getResourcesForApplication(ithemepkg);
            int pon = resources.getIdentifier(itheme + "pon", "drawable", ithemepkg);
            int poff = resources.getIdentifier(itheme + "poff", "drawable", ithemepkg);
            int c1 = resources.getIdentifier(ithemepkg + ":string/itheme1color1", null, null);
            int c2 = resources.getIdentifier(ithemepkg + ":string/itheme1color2", null, null);
            int c3 = resources.getIdentifier(ithemepkg + ":string/itheme1color3", null, null);
            int c4 = resources.getIdentifier(ithemepkg + ":string/itheme1color4", null, null);
            lock9View.set(resources.getDrawable(pon), resources.getDrawable(poff), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c1, null))), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c2, null))), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c3, null))), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c4, null))));
            Cursor cursor1 = this.db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 = this.db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 = this.pm.getResourcesForApplication(cursor1.getString(1));
                    Resources resources2 = resources1;
                    this.Pinlockll.setBackgroundDrawable(resources1.getDrawable(resources2.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e) {
                    this.db.deletebackground(cursor1.getString(1));
                    resources1 = this.pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    this.Pinlockll.setBackgroundDrawable(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                    this.Pinlockll.setBackgroundDrawable(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e2) {
                    this.db.deletebackground(cursor1.getString(1));
                    resources1 = this.pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    this.Pinlockll.setBackgroundDrawable(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
            this.emergency = resources.getDrawable(resources.getIdentifier("fpassword", "drawable", ithemepkg));
            this.bf.setBackgroundDrawable(this.emergency);
        } catch (NameNotFoundException e3) {
            e3.printStackTrace();
        }
    }

    public void Addmob() {
        Log.e("FourSquareLockScreen", "Activity");
        AdView adView = (AdView) findViewById(R.id.adView_ipaternlockscren);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }


    private void vcheck() {
        try {
            Cursor cursor1 = this.db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
                this.db.deletebackground(cursor1.getString(1));
                Resources resources1 = this.pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                this.Pinlockll.setBackgroundDrawable(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
        }
    }

    protected void onResume() {
        vcheck();
        super.onResume();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() != 4 || event.getAction() != 0) {
            if (this.db.getfastunlockm1().contains("true")) {
                if (event.getKeyCode() == 24 && event.getAction() == 0) {
                    this.v = 1;
                    return true;
                } else if (event.getKeyCode() == 24 && event.getAction() == 1) {
                    this.v = 0;
                    return true;
                }
            }
            if (this.db.getfastunlockm2().contains("true")) {
                if (event.getKeyCode() == 25 && event.getAction() == 0) {
                    this.v = 1;
                    return true;
                } else if (event.getKeyCode() == 25 && event.getAction() == 1) {
                    this.v = 0;
                    return true;
                }
            }
            return super.dispatchKeyEvent(event);
        } else if (this.v == 1) {
            this.v = 0;
            finish();
            return true;
        } else {
            Intent startHome = new Intent("android.intent.action.MAIN");
            startHome.addCategory("android.intent.category.HOME");
            startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startHome);
            return true;
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.bf) {
            PopupMenu popup = new PopupMenu(this, v);
            popup.getMenuInflater().inflate(R.menu.actions, popup.getMenu());
            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    iPatternLockScreen.this.cnt = 0;
                    iPatternLockScreen.this.password = BuildConfig.FLAVOR;
                    iPatternLockScreen.this.db.setfp("true");
                    Intent startHome = new Intent(iPatternLockScreen.this.getBaseContext(), Forgotpassword.class);
                    startHome.addCategory("android.intent.category.HOME");
                    startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    iPatternLockScreen.this.startActivity(startHome);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                        }
                    }, 150);
                    return false;
                }
            });
            popup.show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
        super.onBackPressed();
    }
}
