package com.applockthree;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applockthree.utils.Lock9View;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PatternLockScreen extends Service implements OnClickListener {
    private  RelativeLayout Pinlockll;
    private ImageView appicon;
    private TextView appname,bf;
    private int cnt = 0;
    private Date date;
    private DataBase db;
    private  SimpleDateFormat dm,dy,dd,tm,ts,th;
    private  Drawable emergency;
    private Lock9View lock9View;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String password ;
    private  PackageManager pm;
    int v = 0;
    private  Vibrator vb;
    private  LayoutParams wmlp;

    public void onCreate() {
        super.onCreate();
        th = new SimpleDateFormat("hh");
        tm = new SimpleDateFormat("mm");
        ts = new SimpleDateFormat("ss a");
        dd = new SimpleDateFormat("dd");
        dm = new SimpleDateFormat("MM");
        dy = new SimpleDateFormat("yy");
        db = new DataBase(this);
        vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        pm = getBaseContext().getPackageManager();
        emergency = getResources().getDrawable(R.drawable.fpassword);
    }

    private void initLayouts() {
        Resources resources1;
        try {
            mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            wmlp = new LayoutParams(-1, -1, 2003, 32, -3);
            wmlp.gravity = 17;
            wmlp.screenOrientation = 1;
            wmlp.alpha = 1.0f;
            wmlp.flags = 262144;
            LayoutParams layoutParams = wmlp;
            layoutParams.flags &= -2097297;
            wmlp.flags = 256;
            wmlp.format = -1;
            wmlp.token = null;
            mWindowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            mLayout = new LinearLayout(this) {
                public boolean dispatchKeyEvent(KeyEvent event) {
                    if (event.getKeyCode() != 4 || event.getAction() != 0) {
                        if (db.getfastunlockm1().contains("true")) {
                            if (event.getKeyCode() == 24 && event.getAction() == 0) {
                                v = 1;
                                return true;
                            } else if (event.getKeyCode() == 24 && event.getAction() == 1) {
                                v = 0;
                                return true;
                            }
                        }
                        if (db.getfastunlockm2().contains("true")) {
                            if (event.getKeyCode() == 25 && event.getAction() == 0) {
                                v = 1;
                                return true;
                            } else if (event.getKeyCode() == 25 && event.getAction() == 1) {
                                v = 0;
                                return true;
                            }
                        }
                        return super.dispatchKeyEvent(event);
                    } else if (v == 1) {
                        v = 0;
                        removeLockScreen();
                        return true;
                    } else {
                        cancelAndGoHome();
                        return true;
                    }
                }
            };
            mLayout.setLayoutParams(new LayoutParams(-1, -1));
            View view = LayoutInflater.from(this).inflate(R.layout.pattern_lock_screen, mLayout, true);
            Pinlockll = (RelativeLayout) view.findViewById(R.id.Pinlockll);
            lock9View = (Lock9View) view.findViewById(R.id.lock_9_view);
            Addmob(view);
            lock9View.setCallBack(new Lock9View.CallBack() {
                public void onFinish(String password) {
                    if (db.matchpassword(password)) {
                        new Handler(getMainLooper()).postDelayed(new Runnable() {
                            public void run() {
                                removeLockScreen();
                            }
                        }, 100);
                        return;
                    }
                    if (db.getisettingvibrate().contains("true")) {
                        vb.vibrate(200);
                    }
                    final String str = password;
                    new Handler(getMainLooper()).postDelayed(new Runnable() {
                        public void run() {
                            date = new Date();
                            String d ;
                            String t ;
                            String an = BuildConfig.FLAVOR;
                            d = dd.format(date) + "/" + dm.format(date) + "/" + dy.format(date);
                            t = th.format(date) + ":" + tm.format(date) + ":" + ts.format(date);
                            try {
                                an = pm.getApplicationLabel(pm.getApplicationInfo(db.getcurrentapp(), PackageManager.GET_META_DATA)).toString();
                            } catch (NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            db.insertapphacklog(db.getcurrentapp(), an, str, d, t);
                        }
                    }, 100);
                }
            });
            appicon = (ImageView) view.findViewById(R.id.appicon);
            appname = (TextView) view.findViewById(R.id.appname);
            bf = (TextView) view.findViewById(R.id.bf);
            bf.setOnClickListener(this);
            Cursor cursor = db.getitheme();
            cursor.moveToNext();
            String itheme = cursor.getString(0);
            String ithemepkg = cursor.getString(1);
            String ithemebg = cursor.getString(2);
            cursor.close();
            Resources resources = pm.getResourcesForApplication(ithemepkg);
            int pon = resources.getIdentifier(itheme + "pon", "drawable", ithemepkg);
            int poff = resources.getIdentifier(itheme + "poff", "drawable", ithemepkg);
            int c1 = resources.getIdentifier(ithemepkg + ":string/itheme1color1", null, null);
            int c2 = resources.getIdentifier(ithemepkg + ":string/itheme1color2", null, null);
            int c3 = resources.getIdentifier(ithemepkg + ":string/itheme1color3", null, null);
            int c4 = resources.getIdentifier(ithemepkg + ":string/itheme1color4", null, null);
            emergency = resources.getDrawable(resources.getIdentifier("fpassword", "drawable", ithemepkg));
            lock9View.set(resources.getDrawable(pon), resources.getDrawable(poff), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c1, null))), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c2, null))), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c3, null))), Integer.valueOf(String.valueOf(getPackageManager().getText(ithemepkg, c4, null))));
            Cursor cursor1 = db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 = db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 = pm.getResourcesForApplication(cursor1.getString(1));
                    Resources resources2 = resources1;
                    Pinlockll.setBackground(resources1.getDrawable(resources2.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                    Pinlockll.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e2) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
            bf.setBackground(emergency);
        } catch (NameNotFoundException e3) {
            e3.printStackTrace();
        }
    }
    public void Addmob(View view) {
        Log.e("FourSquareLockScreen","Activity");
        AdView adView = (AdView) view.findViewById(R.id.adView_patenlock);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }


    private void vcheck() {
        try {
            Cursor cursor1 = db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
                db.deletebackground(cursor1.getString(1));
                Resources resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        vcheck();
        String str = BuildConfig.FLAVOR;
        try {
            str = intent.getStringExtra("type");
        } catch (Exception e) {
        }
        if ((str == BuildConfig.FLAVOR || str == null) && mLayout == null) {
            initLayouts();
        }
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (str.contains("1") && mLayout == null) {
            initLayouts();
        }
        if (str.contains("3")) {
            try {
                initLayouts();
            } catch (Exception e2) {
            }
        }
        if (str.contains("2")) {
            if (mLayout == null) {
                initLayouts();
            }
            try {
                cnt = 0;
                password = BuildConfig.FLAVOR;
                try {
                    appname.setText(pm.getApplicationLabel(pm.getApplicationInfo(db.getcurrentapp(), PackageManager.GET_META_DATA)).toString());
                    appicon.setImageDrawable(pm.getApplicationIcon(db.getcurrentapp()));
                } catch (NameNotFoundException e3) {
                    e3.printStackTrace();
                }
                mWindowManager.addView(mLayout, wmlp);
            } catch (RuntimeException e4) {
            }
        }
        if (str.contains("0")) {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    try {
                        removeLockScreen();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 100);
        }
        return START_STICKY;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.bf) {
            PopupMenu popup = new PopupMenu(this, v);
            popup.getMenuInflater().inflate(R.menu.actions, popup.getMenu());
            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    cnt = 0;
                    password = BuildConfig.FLAVOR;
                    db.setfp("true");
                    Intent startHome = new Intent(getBaseContext(), Forgotpassword.class);
                    startHome.addCategory("android.intent.category.HOME");
                    startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startHome);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            removeLockScreen();
                        }
                    }, 150);
                    return false;
                }
            });
            popup.show();
        }
    }

    public void onDestroy() {
        removeLockScreen();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void cancelAndGoHome() {
        Intent startHome = new Intent("android.intent.action.MAIN");
        startHome.addCategory("android.intent.category.HOME");
        startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startHome);
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            public void run() {
                removeLockScreen();
            }
        }, 100);
    }

    private void removeLockScreen() {
        if (mLayout != null) {
            try {
                mWindowManager.removeView(mLayout);
            } catch (IllegalStateException e) {
            } catch (Exception e2) {
            }
            try {
                appicon.setImageResource(R.drawable.blank);
            } catch (Exception e3) {
            }
        }
    }
}
