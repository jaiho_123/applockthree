package com.applockthree;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;

public class ImageTouchLockScreencustom extends Service implements OnClickListener {
    private RelativeLayout Pinlockll;
    private Button[] button;
    private int cnt = 0;
    private int cnt1 = 0;
    private DataBase db;
    private ImageView[] dot;
    private Drawable[] dotid;
    private LinearLayout imagetouchrl;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String password;
    private String password1;
    private String pkg = BuildConfig.FLAVOR;
    private PackageManager pm;
    private String string1;
    private String string2;
    private TextView txtname;
    private Vibrator vb;

    public void onCreate() {
        super.onCreate();
        db = new DataBase(this);
        button = new Button[50];
        dotid = new Drawable[5];
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        dot = new ImageView[5];
        password = BuildConfig.FLAVOR;
        password1 = BuildConfig.FLAVOR;
        pkg = BuildConfig.FLAVOR;
        string1 = getResources().getString(R.string.Enter_new_custom_password);
        string2 = getResources().getString(R.string.Comfirm_custom_password);
        pm = getBaseContext().getPackageManager();
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        cnt1 = 1;
    }

    private void initLayouts() {
        Resources resources1;
        LayoutParams wmlp = new LayoutParams(-1, -1, 2003, 256, -3);
        wmlp.gravity = 17;
        wmlp.screenOrientation = 1;
        wmlp.alpha = 1.0f;
        wmlp.flags = 262144;
        wmlp.flags &= -2097297;
        wmlp.flags = 256;
        wmlp.format = -1;
        wmlp.token = null;
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        mLayout = new LinearLayout(this) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() != 4 || event.getAction() != 0) {
                    return super.dispatchKeyEvent(event);
                }
                Intent startHome = new Intent(getBaseContext(), MainActivity.class);
                startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startHome);
                stopSelf();
                return true;
            }
        };
       mLayout.setLayoutParams(new LayoutParams(-1, -1));
        View view = LayoutInflater.from(this).inflate(R.layout.image_touch_lock_screen_change_password,mLayout, true);
       Pinlockll = (RelativeLayout) view.findViewById(R.id.Pinlockll);
       imagetouchrl = (LinearLayout) view.findViewById(R.id.imagetouchrl);
       txtname = (TextView) view.findViewById(R.id.appname);
       dot[1] = (ImageView) view.findViewById(R.id.idot1);
       dot[2] = (ImageView) view.findViewById(R.id.idot2);
       dot[3] = (ImageView) view.findViewById(R.id.idot3);
       dot[4] = (ImageView) view.findViewById(R.id.idot4);
        Addmob(view);
       txtname.setText(string1);
        Cursor cursor =db.getitheme();
        cursor.moveToNext();
        String itheme = cursor.getString(0);
        String ithemepkg = cursor.getString(1);
        String ithemebg = cursor.getString(2);
        cursor.close();
        try {
            Resources resources =pm.getResourcesForApplication(ithemepkg);
            int idm0 = resources.getIdentifier(itheme + "imagetouch", "drawable", ithemepkg);
           button[1] = (Button) view.findViewById(R.id.xy1);
           button[2] = (Button) view.findViewById(R.id.xy2);
           button[3] = (Button) view.findViewById(R.id.xy3);
           button[4] = (Button) view.findViewById(R.id.xy4);
           button[5] = (Button) view.findViewById(R.id.xy5);
           button[6] = (Button) view.findViewById(R.id.xy6);
           button[7] = (Button) view.findViewById(R.id.xy7);
           button[8] = (Button) view.findViewById(R.id.xy8);
           button[9] = (Button) view.findViewById(R.id.xy9);
           button[10] = (Button) view.findViewById(R.id.xy10);
           button[11] = (Button) view.findViewById(R.id.xy11);
           button[12] = (Button) view.findViewById(R.id.xy12);
           button[13] = (Button) view.findViewById(R.id.xy13);
           button[14] = (Button) view.findViewById(R.id.xy14);
           button[15] = (Button) view.findViewById(R.id.xy15);
           button[16] = (Button) view.findViewById(R.id.xy16);
           button[17] = (Button) view.findViewById(R.id.xy17);
           button[18] = (Button) view.findViewById(R.id.xy18);
           button[19] = (Button) view.findViewById(R.id.xy19);
           button[20] = (Button) view.findViewById(R.id.xy20);
           button[21] = (Button) view.findViewById(R.id.xy21);
           button[22] = (Button) view.findViewById(R.id.xy22);
           button[23] = (Button) view.findViewById(R.id.xy23);
           button[24] = (Button) view.findViewById(R.id.xy24);
           button[25] = (Button) view.findViewById(R.id.xy25);
           button[26] = (Button) view.findViewById(R.id.xy26);
           button[27] = (Button) view.findViewById(R.id.xy27);
           button[28] = (Button) view.findViewById(R.id.xy28);
           button[29] = (Button) view.findViewById(R.id.xy29);
           button[30] = (Button) view.findViewById(R.id.xy30);
           button[31] = (Button) view.findViewById(R.id.xy31);
           button[32] = (Button) view.findViewById(R.id.xy32);
           button[33] = (Button) view.findViewById(R.id.xy33);
           button[34] = (Button) view.findViewById(R.id.xy34);
           button[35] = (Button) view.findViewById(R.id.xy35);
           button[36] = (Button) view.findViewById(R.id.xy36);
           button[37] = (Button) view.findViewById(R.id.xy37);
           button[38] = (Button) view.findViewById(R.id.xy38);
           button[39] = (Button) view.findViewById(R.id.xy39);
           button[40] = (Button) view.findViewById(R.id.xy40);
           button[41] = (Button) view.findViewById(R.id.xy41);
           button[42] = (Button) view.findViewById(R.id.xy42);
           button[43] = (Button) view.findViewById(R.id.xy43);
           button[44] = (Button) view.findViewById(R.id.xy44);
           button[45] = (Button) view.findViewById(R.id.xy45);
           button[46] = (Button) view.findViewById(R.id.xy46);
           button[47] = (Button) view.findViewById(R.id.xy47);
           button[48] = (Button) view.findViewById(R.id.xy48);
           button[49] = (Button) view.findViewById(R.id.xy49);
           button[1].setOnClickListener(this);
           button[2].setOnClickListener(this);
           button[3].setOnClickListener(this);
           button[4].setOnClickListener(this);
           button[5].setOnClickListener(this);
           button[6].setOnClickListener(this);
           button[7].setOnClickListener(this);
           button[8].setOnClickListener(this);
           button[9].setOnClickListener(this);
           button[10].setOnClickListener(this);
           button[11].setOnClickListener(this);
           button[12].setOnClickListener(this);
           button[13].setOnClickListener(this);
           button[14].setOnClickListener(this);
           button[15].setOnClickListener(this);
           button[16].setOnClickListener(this);
           button[17].setOnClickListener(this);
           button[18].setOnClickListener(this);
           button[19].setOnClickListener(this);
           button[20].setOnClickListener(this);
           button[21].setOnClickListener(this);
           button[22].setOnClickListener(this);
           button[23].setOnClickListener(this);
           button[24].setOnClickListener(this);
           button[25].setOnClickListener(this);
           button[26].setOnClickListener(this);
           button[27].setOnClickListener(this);
           button[28].setOnClickListener(this);
           button[29].setOnClickListener(this);
           button[30].setOnClickListener(this);
           button[31].setOnClickListener(this);
           button[32].setOnClickListener(this);
           button[33].setOnClickListener(this);
           button[34].setOnClickListener(this);
           button[35].setOnClickListener(this);
           button[36].setOnClickListener(this);
           button[37].setOnClickListener(this);
           button[38].setOnClickListener(this);
           button[39].setOnClickListener(this);
           button[40].setOnClickListener(this);
           button[41].setOnClickListener(this);
           button[42].setOnClickListener(this);
           button[43].setOnClickListener(this);
           button[44].setOnClickListener(this);
           button[45].setOnClickListener(this);
           button[46].setOnClickListener(this);
           button[47].setOnClickListener(this);
           button[48].setOnClickListener(this);
           dotid[0] = resources.getDrawable(resources.getIdentifier(itheme + "dot", "drawable", ithemepkg));
           dotid[1] = resources.getDrawable(resources.getIdentifier(itheme + "doti", "drawable", ithemepkg));
           imagetouchrl.setBackground(resources.getDrawable(idm0));
            Cursor cursor1 =db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 =db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 =pm.getResourcesForApplication(cursor1.getString(1));
                   Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e) {
                   db.deletebackground(cursor1.getString(1));
                    resources1 =pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                   Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                   Pinlockll.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e2) {
                   db.deletebackground(cursor1.getString(1));
                    resources1 =pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                   Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
           dot[1].setBackground(dotid[0]);
           dot[2].setBackground(dotid[0]);
           dot[3].setBackground(dotid[0]);
           dot[4].setBackground(dotid[0]);
        } catch (NameNotFoundException e3) {
            e3.printStackTrace();
        }
       txtname.setText(string1);
        try {
           mWindowManager.addView(mLayout, wmlp);
        } catch (RuntimeException e4) {
            stopSelf();
        }
    }
    public void Addmob(View view) {
        Log.e("FourSquareLockScreen","Activity");
        AdView adView = (AdView) view.findViewById(R.id.adView_tochcghangepsswd);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }

    private void vcheck() {
        try {
            Cursor cursor1 =db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
               db.deletebackground(cursor1.getString(1));
                Resources resources1 =pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
               Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        vcheck();
        try {
           pkg = intent.getStringExtra("pkg");
        } catch (Exception e) {
        }
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (mLayout == null) {
            initLayouts();
        }
        return START_STICKY;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.xy1) {
            check("01");
        }
        if (v.getId() == R.id.xy2) {
            check("02");
        }
        if (v.getId() == R.id.xy3) {
            check("03");
        }
        if (v.getId() == R.id.xy4) {
            check("04");
        }
        if (v.getId() == R.id.xy5) {
            check("05");
        }
        if (v.getId() == R.id.xy6) {
            check("06");
        }
        if (v.getId() == R.id.xy7) {
            check("07");
        }
        if (v.getId() == R.id.xy8) {
            check("08");
        }
        if (v.getId() == R.id.xy9) {
            check("09");
        }
        if (v.getId() == R.id.xy10) {
            check("10");
        }
        if (v.getId() == R.id.xy11) {
            check("11");
        }
        if (v.getId() == R.id.xy12) {
            check("12");
        }
        if (v.getId() == R.id.xy13) {
            check("13");
        }
        if (v.getId() == R.id.xy14) {
            check("14");
        }
        if (v.getId() == R.id.xy15) {
            check("15");
        }
        if (v.getId() == R.id.xy16) {
            check("16");
        }
        if (v.getId() == R.id.xy17) {
            check("17");
        }
        if (v.getId() == R.id.xy18) {
            check("18");
        }
        if (v.getId() == R.id.xy19) {
            check("19");
        }
        if (v.getId() == R.id.xy20) {
            check("20");
        }
        if (v.getId() == R.id.xy21) {
            check("21");
        }
        if (v.getId() == R.id.xy22) {
            check("22");
        }
        if (v.getId() == R.id.xy23) {
            check("23");
        }
        if (v.getId() == R.id.xy24) {
            check("24");
        }
        if (v.getId() == R.id.xy25) {
            check("25");
        }
        if (v.getId() == R.id.xy26) {
            check("26");
        }
        if (v.getId() == R.id.xy27) {
            check("27");
        }
        if (v.getId() == R.id.xy28) {
            check("28");
        }
        if (v.getId() == R.id.xy29) {
            check("29");
        }
        if (v.getId() == R.id.xy30) {
            check("30");
        }
        if (v.getId() == R.id.xy31) {
            check("31");
        }
        if (v.getId() == R.id.xy32) {
            check("32");
        }
        if (v.getId() == R.id.xy33) {
            check("33");
        }
        if (v.getId() == R.id.xy34) {
            check("34");
        }
        if (v.getId() == R.id.xy35) {
            check("35");
        }
        if (v.getId() == R.id.xy36) {
            check("36");
        }
        if (v.getId() == R.id.xy37) {
            check("37");
        }
        if (v.getId() == R.id.xy38) {
            check("38");
        }
        if (v.getId() == R.id.xy39) {
            check("39");
        }
        if (v.getId() == R.id.xy40) {
            check("40");
        }
        if (v.getId() == R.id.xy41) {
            check("41");
        }
        if (v.getId() == R.id.xy42) {
            check("42");
        }
        if (v.getId() == R.id.xy43) {
            check("43");
        }
        if (v.getId() == R.id.xy44) {
            check("44");
        }
        if (v.getId() == R.id.xy45) {
            check("45");
        }
        if (v.getId() == R.id.xy46) {
            check("46");
        }
        if (v.getId() == R.id.xy47) {
            check("47");
        }
        if (v.getId() == R.id.xy48) {
            check("48");
        }
        if (v.getId() == R.id.xy49) {
            check("49");
        }
    }

    public void check(String key) {
        if (db.getsound().contains("true")) {
            MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.click);
            mp.start();
            mp.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        if (db.getisettingvibrate().contains("true")) {
           vb.vibrate(50);
        }
       password += key;
       cnt++;
        setdot();
        if (cnt >= 4) {
            if (cnt1 < 2) {
               password1 =password;
               password = BuildConfig.FLAVOR;
               cnt1++;
               cnt = 0;
                setdot();
            } else if (password.contains(password1)) {
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        db.insertlockbypackage(pkg, "true");
                        db.setapppassword(pkg, password);
                        stopSelf();
                    }
                }, 100);
            } else {
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        if (db.getisettingvibrate().contains("true")) {
                            vb.vibrate(200);
                        }
                        cnt = 0;
                        password = BuildConfig.FLAVOR;
                        setdot();
                    }
                }, 100);
            }
        }
        if (cnt1 == 2) {
           button[11].setVisibility(View.VISIBLE);
        } else {
           button[11].setVisibility(View.GONE);
        }
        setname();
    }

    public void setdot() {
        if (cnt == 0) {
           dot[1].setBackground(dotid[0]);
           dot[2].setBackground(dotid[0]);
           dot[3].setBackground(dotid[0]);
           dot[4].setBackground(dotid[0]);
        }
        if (cnt == 1) {
           dot[1].setBackground(dotid[1]);
           dot[2].setBackground(dotid[0]);
           dot[3].setBackground(dotid[0]);
           dot[4].setBackground(dotid[0]);
        }
        if (cnt == 2) {
           dot[2].setBackground(dotid[1]);
           dot[3].setBackground(dotid[0]);
           dot[4].setBackground(dotid[0]);
        }
        if (cnt == 3) {
           dot[3].setBackground(dotid[1]);
           dot[4].setBackground(dotid[0]);
        }
        if (cnt == 4) {
           dot[4].setBackground(dotid[1]);
        }
    }

    public void setname() {
        if (cnt1 == 1) {
           txtname.setText(string1);
        } else {
           txtname.setText(string2);
        }
    }

    public void onDestroy() {
        removeLockScreen();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }


    private void removeLockScreen() {
       pkg = BuildConfig.FLAVOR;
        if (mLayout != null) {
            try {
               mWindowManager.removeView(mLayout);
            } catch (IllegalStateException e) {
            }
           mLayout = null;
        }
    }
}
