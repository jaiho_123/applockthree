package com.applockthree;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;

public class fastunlock extends Fragment {
    private int cnt = 0;
    private DataBase db;
    private SwitchCompat fu;
    private CheckBox fucb1;
    private CheckBox fucb2;
    private RelativeLayout method1;
    private RelativeLayout method2;
    private String ostr, ostr1, ostr2, str, str1, str2;
    private Vibrator vb;
    private View view;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = new DataBase(getContext());
        view = inflater.inflate(R.layout.fast_unlock_layout, null);
        vb = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        return view;
    }

    public void onResume() {
        fu = (SwitchCompat) view.findViewById(R.id.fastunlockswitch);
        fucb1 = (CheckBox) view.findViewById(R.id.fucb1);
        fucb2 = (CheckBox) view.findViewById(R.id.fucb2);
        method1 = (RelativeLayout) view.findViewById(R.id.method1rl);
        method2 = (RelativeLayout) view.findViewById(R.id.method2rl);
        fill();
        this.fu.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MediaPlayer mp;
                if (fu.isChecked()) {
                    db.setfastunlock("true", str1, str2);
                    if (db.getisettingvibrate().contains("true")) {
                        vb.vibrate(50);
                    }
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.on);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                } else {
                    if (db.getisettingvibrate().contains("true")) {
                        vb.vibrate(50);
                    }
                    if (db.getsound().contains("true")) {
                        mp = MediaPlayer.create(getContext(), R.raw.off);
                        mp.start();
                        mp.setOnCompletionListener(new OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    }
                    db.setfastunlock("false", str1, str2);
                }
                fill();
            }
        });
        fucb1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (fucb1.isChecked()) {
                    db.setfastunlock(str, "true", str2);
                } else {
                    db.setfastunlock(str, "false", str2);
                }
                fill();
            }
        });
        this.fucb2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (fucb2.isChecked()) {
                    db.setfastunlock(str, str1, "true");
                } else {
                    db.setfastunlock(str, str1, "false");
                }
                fill();
            }
        });
        super.onResume();
    }

    private void fill() {
        Cursor cursor = db.getfastunlock();
        cursor.moveToNext();
        str = cursor.getString(0);
        str1 = cursor.getString(1);
        str2 = cursor.getString(2);
        if (cnt == 0) {
            cnt++;
            ostr = str;
            ostr1 = str1;
            ostr2 = str2;
        }
        set();
    }

    private void set() {
        Cursor cursor = db.getfastunlock();
        cursor.moveToNext();
        if (cursor.getString(0).contains("true")) {
            fu.setChecked(true);
            method1.setVisibility(View.VISIBLE);
            method2.setVisibility(View.VISIBLE);
            if (cursor.getString(1).contains("true")) {
                fucb1.setChecked(true);
            } else {
                fucb1.setChecked(false);
            }
            if (cursor.getString(2).contains("true")) {
                fucb2.setChecked(true);
                return;
            } else {
                this.fucb2.setChecked(false);
                return;
            }
        }
        fu.setChecked(false);
        method1.setVisibility(View.GONE);
        method2.setVisibility(View.GONE);
    }

    public void onStop() {
        fill();
        if (ostr.contains(str) && ostr1.contains(str1) && !ostr2.contains(str2)) {
            getContext().startService(new Intent(getContext(), MyService.class));
            super.onStop();
        } else {
            getContext().startService(new Intent(getContext(), MyService.class));
            super.onStop();
        }
    }
}
