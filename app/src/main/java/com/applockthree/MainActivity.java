package com.applockthree;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.applockthree.utils.PermissionResult;
import com.applockthree.utils.PermissionUtils;
import com.giftlibrray.LoadGiftApp;

import java.util.Locale;

public class MainActivity extends ActivityManagePermission {
    public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 8347;
    private short REQUEST_CODE_ASK_PERMISSIONS = (short) 7;
    private DataBase db;
    private boolean flag = false;
    private boolean lock = false;
    private DrawerLayout mDrawerLayout;
    private FragmentManager mFragmentManager;
    private NavigationView mNavigationView;
    private Toolbar toolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(this, MyService.class));
        db = new DataBase(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().replace(R.id.containerView, new iapplock()).commit();
        db.setposition("iapplock");

        askCompactPermissions(new String[]{PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_CALL_PHONE}, new PermissionResult() {
            @Override
            public void permissionGranted() {

            }
            @Override
            public void permissionDenied() {
                //permission denied
                //replace with your action
            }
        });
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();
                if (menuItem.getItemId() == R.id.nav_item_password) {
                    if (!db.getposition().contains("Password")) {
                        mFragmentManager.beginTransaction().replace(R.id.containerView, new Password()).commit();
                        db.setposition("Password");
                    } else if (mDrawerLayout.isDrawerOpen(8388611)) {
                        mDrawerLayout.closeDrawer(8388611);
                    }
                }
                if (menuItem.getItemId() == R.id.nav_item_fastunlock) {
                    if (!db.getposition().contains("fastunlock")) {
                        mFragmentManager.beginTransaction().replace(R.id.containerView, new fastunlock()).commit();
                        db.setposition("fastunlock");
                    } else if (mDrawerLayout.isDrawerOpen(8388611)) {
                        mDrawerLayout.closeDrawer(8388611);
                    }
                }
                if (menuItem.getItemId() == R.id.nav_item_setting) {
                    if (!db.getposition().contains("setting")) {
                        // mFragmentManager.beginTransaction().replace(R.id.containerView, new setting()).commit();
                        Intent i = new Intent(MainActivity.this, SettingActivity.class);
                        startActivity(i);
                        setTitle(getString(R.string.setings));
                        db.setposition("setting");
                    } else if (mDrawerLayout.isDrawerOpen(8388611)) {
                        mDrawerLayout.closeDrawer(8388611);
                    }
                }
                if (menuItem.getItemId() == R.id.nav_item_iapplock) {
                    if (!db.getposition().contains("iapplock")) {
                        mFragmentManager.beginTransaction().replace(R.id.containerView, new iapplock()).commit();
                        db.setposition("iapplock");
                    } else if (mDrawerLayout.isDrawerOpen(8388611)) {
                        mDrawerLayout.closeDrawer(8388611);
                    }
                }
                return false;
            }
        });
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(new DrawerListener() {
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (db.getlchange().contains("true")) {
                    db.setlchange("false");
                    abc();
                }
            }

            public void onDrawerOpened(View drawerView) {
            }

            public void onDrawerClosed(View drawerView) {
            }

            public void onDrawerStateChanged(int newState) {
            }
        });
    }

    boolean doubleBackToExitPressedOnce = false;


    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.exits, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                jaiho();
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void jaiho() {
        if (mDrawerLayout.isDrawerOpen(8388611)) {
            mDrawerLayout.closeDrawer(8388611);
        } else if (db.getposition().contains("Password")) {
            mFragmentManager.beginTransaction().replace(R.id.containerView, new iapplock()).commit();
            db.setposition("iapplock");
        } else if (db.getposition().contains("fastunlock")) {
            mFragmentManager.beginTransaction().replace(R.id.containerView, new iapplock()).commit();
            db.setposition("iapplock");
        } else if (db.getposition().contains("setting")) {
            mFragmentManager.beginTransaction().replace(R.id.containerView, new iapplock()).commit();
            db.setposition("iapplock");
        } else if (db.getposition().contains("help")) {
            mFragmentManager.beginTransaction().replace(R.id.containerView, new iapplock()).commit();
            db.setposition("iapplock");
        } else if (db.getposition().contains("questionanswer")) {
            mFragmentManager.beginTransaction().replace(R.id.containerView, new setting()).commit();
            db.setposition("setting");
        } else {
            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(1);
            dialog.setContentView(R.layout.exitdialog);
            Button btncancle = (Button) dialog.findViewById(R.id.btnno);
            dialog.findViewById(R.id.btnyes).setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent startHome = new Intent("android.intent.action.MAIN");
                    startHome.addCategory("android.intent.category.HOME");
                    startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startHome);
                    lock = false;
                    mFragmentManager.beginTransaction().replace(R.id.containerView, new iapplock()).commit();
                    db.setposition("iapplock");
                    dialog.dismiss();
                }
            });
            btncancle.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.gift:
                try {
                    startActivity(new Intent(MainActivity.this, LoadGiftApp.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        db.insertcurrentapp(BuildConfig.FLAVOR);
        if (mDrawerLayout.isDrawerOpen(8388611)) {
            mDrawerLayout.closeDrawer(8388611);
        }
        if (db.getisettinghideiapplock().contains("true")) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(new ComponentName(getBaseContext(), StartActivity.class), 2, 1);
        } else {
            getBaseContext().getPackageManager().setComponentEnabledSetting(new ComponentName(getBaseContext(), StartActivity.class), 1, 1);
        }
        super.onStop();
    }

    protected void onResume() {

        if (db.getmasterpassword().trim().toString().length() >= 1 && db.getisettinganswer().trim().length() >= 1) {
            if (db.getfp().contains("true")) {
                if (db.getfp().contains("true")) {
                    db.setfp("false");
                }
            } else if (lock) {
                lock = false;
            } else {
                lock = true;
                if (db.getlocktype().contains("PatternLockScreen")) {
                    startActivity(new Intent(this, iPatternLockScreen.class));
                } else if (db.getlocktype().contains("ImageTouchLockScreen")) {
                    startActivity(new Intent(this, iImageTouchLockScreen.class));
                } else if (db.getlocktype().contains("FourSquareLockScreen")) {
                    startActivity(new Intent(this, iFourSquareLockScreen.class));
                } else {
                    startActivity(new Intent(this, iPINLockScreen.class));
                }
            }
        }
        String s = db.getisettinglanguage();
        if (s.contains("English")) {
            lchange("en");
        } else if (s.contains("Arabic")) {
            lchange("ar");
        } else if (s.contains("Japanese")) {
            lchange("ja");
        } else if (s.contains("Franch")) {
            lchange("fr");
        } else if (s.contains("Urdu")) {
            lchange("ur");
        } else if (s.contains("Hindi")) {
            lchange("hi");
        } else if (s.contains("Gujarati")) {
            lchange("gu");
        }
        pc();
        super.onResume();
    }

    public void lchange(String code) {
        Locale locale = new Locale(code);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, null);
    }

    public void abc() {
        Menu menu = mNavigationView.getMenu();
        menu.findItem(R.id.nav_item_iapplock).setTitle(getResources().getString(R.string.app_name));
        menu.findItem(R.id.nav_item_password).setTitle(getResources().getString(R.string.password));
        menu.findItem(R.id.nav_item_fastunlock).setTitle(getResources().getString(R.string.Fast_Unlock));
        menu.findItem(R.id.nav_item_setting).setTitle(getResources().getString(R.string.Settings));
    }

    private void pc() {
        if (VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse("package:" + getPackageName()));
                if (!flag) {
                    flag = true;
                    startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
                } else if (flag) {
                    finish();
                }
                Toast.makeText(this, getResources().getString(R.string.Enable_this_permission_for_use_iApplock), Toast.LENGTH_LONG).show();
            } else if (db.getmasterpassword().trim().length() < 1 || db.getisettinganswer().trim().length() < 1) {
                startActivity(new Intent(getBaseContext(), emailquestion.class));
            }
        } else if (db.getmasterpassword().trim().length() < 1 || db.getisettinganswer().trim().length() < 1) {
            startActivity(new Intent(getBaseContext(), emailquestion.class));
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS && VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(this)) {
                flag = false;
                if (db.getmasterpassword().trim().length() < 1 || db.getisettinganswer().trim().length() < 1) {
                    startActivity(new Intent(getBaseContext(), emailquestion.class));
                    return;
                }
                return;
            }
            Toast.makeText(this, getResources().getString(R.string.Sorry_without_this_permission_you_can_not_use_iApplock), Toast.LENGTH_LONG).show();
        }
    }
}
