package com.applockthree;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;

public class FourSquareLockScreencustom extends Service implements OnClickListener {
    private static final String TAG = "Application";
    private RelativeLayout Pinlockll;
    private Button[] button;
    private  int cnt = 0;
    private  int cnt1 = 0;
    private   DataBase db;
    private   ImageView[] dot;
    private  Drawable[] dotid;
    private LinearLayout mLayout;
    private WindowManager mWindowManager;
    private String password;
    private  String password1;
    private  String pkg = BuildConfig.FLAVOR;
    private PackageManager pm;
    private String string1;
    private String string2;
    private TextView txtname;
    private Vibrator vb;

    public void onCreate() {
        super.onCreate();
        db = new DataBase(this);
        button = new Button[5];
        dot = new ImageView[5];
        password = BuildConfig.FLAVOR;
        password1 = BuildConfig.FLAVOR;
        vb = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        dotid = new Drawable[5];
        pkg = BuildConfig.FLAVOR;
        string1 = getResources().getString(R.string.Enter_new_custom_password);
        string2 = getResources().getString(R.string.Comfirm_custom_password);
        pm = getBaseContext().getPackageManager();
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        cnt1 = 1;
    }

    private void initLayouts() {
        Resources resources1;
        LayoutParams wmlp = new LayoutParams(-1, -1, 2003, 256, -3);
        wmlp.gravity = 17;
        wmlp.screenOrientation = 1;
        wmlp.alpha = 1.0f;
        wmlp.flags = 262144;
        wmlp.flags &= -2097297;
        wmlp.flags = 256;
        wmlp.format = -1;
        wmlp.token = null;
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        mLayout = new LinearLayout(this) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() != 4 || event.getAction() != 0) {
                    return super.dispatchKeyEvent(event);
                }
                Intent startHome = new Intent(getBaseContext(), MainActivity.class);
                startHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startHome);
                stopSelf();
                return true;
            }
        };
        mLayout.setLayoutParams(new LayoutParams(-1, -1));
        View view = LayoutInflater.from(this).inflate(R.layout.four_square_lock_screen_change_password, mLayout, true);
        Pinlockll = (RelativeLayout) view.findViewById(R.id.Pinlockll);
        txtname = (TextView) view.findViewById(R.id.appname);
        dot[1] = (ImageView) view.findViewById(R.id.idot1);
        dot[2] = (ImageView) view.findViewById(R.id.idot2);
        dot[3] = (ImageView) view.findViewById(R.id.idot3);
        dot[4] = (ImageView) view.findViewById(R.id.idot4);
        button[1] = (Button) view.findViewById(R.id.xy1);
        button[2] = (Button) view.findViewById(R.id.xy2);
        button[3] = (Button) view.findViewById(R.id.xy3);
        button[4] = (Button) view.findViewById(R.id.xy4);
        button[1].setOnClickListener(this);
        button[2].setOnClickListener(this);
        button[3].setOnClickListener(this);
        button[4].setOnClickListener(this);
        txtname.setText(string1);
        Cursor cursor = db.getitheme();
        cursor.moveToNext();
        String itheme = cursor.getString(0);
        String ithemepkg = cursor.getString(1);
        String ithemebg = cursor.getString(2);
        cursor.close();
        try {
            Resources resources = pm.getResourcesForApplication(ithemepkg);
            int idm0 = resources.getIdentifier(itheme + "fsbox1", "drawable", ithemepkg);
            int idm1 = resources.getIdentifier(itheme + "fsbox2", "drawable", ithemepkg);
            int idm2 = resources.getIdentifier(itheme + "fsbox3", "drawable", ithemepkg);
            int idm3 = resources.getIdentifier(itheme + "fsbox4", "drawable", ithemepkg);
            button[1].setBackground(resources.getDrawable(idm0));
            button[2].setBackground(resources.getDrawable(idm1));
            button[3].setBackground(resources.getDrawable(idm2));
            button[4].setBackground(resources.getDrawable(idm3));
            dotid[0] = resources.getDrawable(resources.getIdentifier(itheme + "dot", "drawable", ithemepkg));
            dotid[1] = resources.getDrawable(resources.getIdentifier(itheme + "doti", "drawable", ithemepkg));
            Cursor cursor1 = db.getbackground();
            if (cursor1.getCount() <= 0) {
                cursor1 = db.getbackground1();
            }
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("background")) {
                try {
                    resources1 = pm.getResourcesForApplication(cursor1.getString(1));
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", cursor1.getString(1))));
                } catch (Exception e) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            if (cursor1.getString(0).contains("custom")) {
                try {
                    Pinlockll.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeFile(cursor1.getString(1))));
                } catch (Exception e2) {
                    db.deletebackground(cursor1.getString(1));
                    resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                    Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
                }
            }
            cursor1.close();
            dot[1].setBackground(dotid[0]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        } catch (NameNotFoundException e3) {
            e3.printStackTrace();
        }
        txtname.setText(string1);
        try {
            mWindowManager.addView(mLayout, wmlp);
        } catch (RuntimeException e4) {
            stopSelf();
        }
    }

    private void vcheck() {
        try {
            Cursor cursor1 = db.getbackground();
            cursor1.moveToNext();
            if (cursor1.getString(0).contains("custom") && !new File(cursor1.getString(1)).exists()) {
                db.deletebackground(cursor1.getString(1));
                Resources resources1 = pm.getResourcesForApplication(BuildConfig.APPLICATION_ID);
                Pinlockll.setBackground(resources1.getDrawable(resources1.getIdentifier("bg1", "drawable", BuildConfig.APPLICATION_ID)));
            }
            cursor1.close();
        } catch (Exception e) {
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        vcheck();
        try {
            pkg = intent.getStringExtra("pkg");
        } catch (Exception e) {
        }
        if (intent == null) {
            return START_NOT_STICKY;
        }
        if (mLayout == null) {
            initLayouts();
        }
        return START_STICKY;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.xy1) {
            check("1");
        }
        if (v.getId() == R.id.xy2) {
            check("2");
        }
        if (v.getId() == R.id.xy3) {
            check("3");
        }
        if (v.getId() == R.id.xy4) {
            check("4");
        }
    }

    public void check(String key) {
        if (db.getsound().contains("true")) {
            MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.click);
            mp.start();
            mp.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
        if (db.getisettingvibrate().contains("true")) {
            vb.vibrate(50);
        }
        password += key;
        cnt++;
        setdot();
        if (cnt >= 4) {
            if (cnt1 < 2) {
                password1 = password;
                password = BuildConfig.FLAVOR;
                cnt1++;
                cnt = 0;
                setdot();
            } else if (password.contains(password1)) {
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        db.insertlockbypackage(pkg, "true");
                        db.setapppassword(pkg, password);
                        stopSelf();
                    }
                }, 100);
            } else {
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        if (db.getisettingvibrate().contains("true")) {
                            vb.vibrate(200);
                        }
                        cnt = 0;
                        password = BuildConfig.FLAVOR;
                        setdot();
                    }
                }, 100);
            }
        }
        setname();
    }

    public void setdot() {
        if (cnt == 0) {
            dot[1].setBackground(dotid[0]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 1) {
            dot[1].setBackground(dotid[1]);
            dot[2].setBackground(dotid[0]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 2) {
            dot[2].setBackground(dotid[1]);
            dot[3].setBackground(dotid[0]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 3) {
            dot[3].setBackground(dotid[1]);
            dot[4].setBackground(dotid[0]);
        }
        if (cnt == 4) {
            dot[4].setBackground(dotid[1]);
        }
    }
    public void Addmob(View view) {
        Log.e("FourSquareLockScreen","Activity");
        AdView adView = (AdView) view.findViewById(R.id.adView_foursqrechangepswd);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("addmob", "loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.e("err in ShowPinLokActvty", "" + errorCode);
            }
        });
    }

    public void setname() {
        if (cnt1 == 1) {
            txtname.setText(string1);
        } else {
            txtname.setText(string2);
        }
    }

    public void onDestroy() {
        removeLockScreen();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }


    private void removeLockScreen() {
        pkg = BuildConfig.FLAVOR;
        if (mLayout != null) {
            try {
                mWindowManager.removeView(mLayout);
            } catch (IllegalStateException e) {
            }
            mLayout = null;
        }
    }
}
