package com.giftlibrray;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

public class LoadAppAdapter extends BaseAdapter {
    private List<Bean> list;
    private Context context;
    private LayoutInflater layoutInflater;
    private View myView;
    private ImageLoader imageLoader;

    public LoadAppAdapter(List<Bean> list, Context context) {
        this.list = list;
        this.context = context;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View V, ViewGroup viewGroup) {
        myView = V;
        ViewHolder viewHolder;
        if (V == null) {
            viewHolder = new ViewHolder();
            myView = layoutInflater.inflate(R.layout.list_item, null);
            viewHolder.name = (TextView) myView.findViewById(R.id.app_name);
            viewHolder.short_des = (TextView) myView.findViewById(R.id.app_desc);
            viewHolder.imageView = (ImageView) myView.findViewById(R.id.image);
            myView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) myView.getTag();
        }
        viewHolder.name.setText(list.get(i).getApp_name());
        viewHolder.short_des.setText(list.get(i).getShort_desc());
        imageLoader.displayImage(list.get(i).getImage_url(), viewHolder.imageView);
        return myView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView name;
        TextView short_des;
    }
}
