package com.giftlibrray;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by AJAY SUNDRIYAL on 7/15/2016.
 */

public class LoadGiftApp extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Bean>> {
    private ListView listView;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listView = new ListView(this);
        setContentView(listView);
        getSupportActionBar().setTitle(R.string.trymore);
        if (isNetworkAvailable(this))
            getSupportLoaderManager().initLoader(1, null, this).forceLoad();
        else
            Toast.makeText(this, R.string.connect, Toast.LENGTH_LONG).show();
    }

    @Override
    public Loader<List<Bean>> onCreateLoader(int id, Bundle args) {
        return new AsynkLoader(this, "http://pastebin.com/raw/4KhSkqm3");
    }

    @Override
    public void onLoadFinished(Loader<List<Bean>> loader, final List<Bean> list) {
        listView.setAdapter(new LoadAppAdapter(list, LoadGiftApp.this));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Uri uri = Uri.parse("market://details?id=" + list.get(i).getApp_url());
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id="
                                    + list.get(i).getApp_url())));
                }
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<List<Bean>> loader) {

    }

    private boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
