package com.giftlibrray;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AsynkLoader extends AsyncTaskLoader<List<Bean>> {
    private String url = null;
    private String response = null;
    private List<Bean> list;

    public AsynkLoader(Context context, String url) {
        super(context);
        this.url = url;
        list = new ArrayList<>();
    }

    @Override
    public List<Bean> loadInBackground() {
        HttpURLConnection urlConnection = null;
        try {
            StringBuilder builder = new StringBuilder();
            int b;
            URL url1 = new URL(url);
            urlConnection = (HttpURLConnection) url1.openConnection();
            urlConnection.setReadTimeout(60000);
            InputStream inputStream = new BufferedInputStream(
                    urlConnection.getInputStream());
            while ((b = inputStream.read()) != -1) {
                builder.append((char) b);
            }
            response = builder.toString();
            setJson(response);
        } catch (Exception e) {
            e.printStackTrace();
            list = null;
        } finally {
            urlConnection.disconnect();
        }
        return list;
    }

    private void setJson(String response) throws Exception {
        JSONArray jsonArray = new JSONArray(response);
        for (int i = 0; i < jsonArray.length(); i++) {
            Bean bean = new Bean();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Boolean status = jsonObject.getBoolean("status");
            if (status) {
                bean.setApp_name(jsonObject.getString("app_name"));
                bean.setApp_url(jsonObject.getString("app_url"));
                bean.setShort_desc(jsonObject.getString("short_desc"));
                bean.setImage_url(jsonObject.getString("image_url"));
                list.add(bean);
            }
        }
    }


}