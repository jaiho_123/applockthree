package com.giftlibrray;

import java.io.Serializable;

/**
 * Created by AJAY SUNDRIYAL on 7/15/2016.
 */

public class Bean implements Serializable {
    private String app_url = null;
    private Boolean status = false;

    public String getApp_url() {
        return app_url;
    }

    public void setApp_url(String app_url) {
        this.app_url = app_url;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getShort_desc() {
        return short_desc;
    }

    public void setShort_desc(String short_desc) {
        this.short_desc = short_desc;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    private String short_desc = null;
    private String image_url = null;
    private String app_name = null;
}
